 
  	jQuery.ajaxSetup({ cache: true });
	jQuery.getScript('https://connect.facebook.net/en_US/sdk.js', function()
	{
	    FB.init({
	      appId: '518927675378904',
	      version: 'v2.1' // or v2.1, v2.2, v2.3, ...
	    });     
	    jQuery('.fb_login').removeAttr('disabled');
	    FB.getLoginStatus(function(response) {
	     
	      if (response.status === 'connected') {
	    		getFBProfile(function(profile) {
	          
	        	});
	      	}
	      
	    });
	    jQuery('body').on('click', '.fb_login', function(e) {
	      	e.preventDefault();
	    
	      	FB.login(function(response){
	         
	        getFBProfile(function(profile) {
	          
				var profile_pic = profile.picture.data.url;
				var first_name  = profile.first_name;
				var last_name   = profile.last_name;
				var email       = profile.email;
				if(email !='')
				{
		         	$.ajax({
			            url: ajaxURLPath+'home/fb_login',
			            method: 'POST',
			            data: {first_name:first_name,last_name:last_name,email:email,profile_pic:profile_pic},
			            success: function(result) 
		            	{
			               	if(result == 'logged_in'){
			                 	window.location.href=ajaxURLPath+'profile/user_profile';
			              	}else{
			                	$('.login_msg').html(result);
			              	}
	                	}
		            });
	          	}
	          // activeLevel('level8');
	        });
	      }, {scope: 'email'});
	    });

	    jQuery('body').on('click', '.fb_register', function(e) {
	      e.preventDefault();
	    
	      FB.login(function(response){
	         
	        getFBProfile(function(profile) {
	          
				var profile_pic = profile.picture.data.url;
				var first_name = profile.first_name;
				var last_name = profile.last_name;
				var email = profile.email;
				if(email !=''){
		          	$.ajax({
			            url: ajaxURLPath+'home/fb_login',
			            method: 'POST',
			            data: {first_name:first_name,last_name:last_name,email:email,profile_pic:profile_pic},
			            success: function(result) 
			            {
		               		if(result == 'logged_in'){
			                 	window.location.href=ajaxURLPath+'profile/user_profile';
			              	}else{
			                	$('.login_msg').html(result);
			              	}
		                }
	    			});
	          	}
	          // activeLevel('level8');
	        });
	      }, {scope: 'email'});
	    });
	});

	function getFBProfile(callback) 
	{
		FB.api('/me?fields=id,first_name,last_name,picture.width(100).height(100),email', function(response) {
			var fname = response.first_name;
			var lname = response.last_name;
			var email = response.email;
			jQuery('#service_modal .fname').val(fname);
			jQuery('#service_modal .lname').val(lname);
			jQuery('#service_modal .user_email').val(email); 
			jQuery('.service_modal .dir_btn.next').show();
			callback(response);

		});
	}

	function onSignIn(googleUser) {
		var profile = googleUser.getBasicProfile(); 
		var first_name  = profile.getName();
		var profile_pic = profile.getImageUrl();
		var email       = profile.getEmail();
			window.onbeforeunload = function(e){
			gapi.auth2.getAuthInstance().signOut();
		}
		$.ajax({
			url: ajaxURLPath+'home/fb_login',
			method: 'POST',
			data: {first_name:first_name ,email:email,profile_pic:profile_pic},
			success: function(result) 
			{
			   	if(result == 'logged_in'){
			     	window.location.href=ajaxURLPath+'profile/user_profile';
			  	}else{
			    	$('.login_msg').html(result);
			  	}
			}
		});
	}

	 

	 