<?php

    /**

   * @package Contact :  CodeIgniter Multi Language Loader

   *

   * @author Zeeshan

   *

   * @email  zeeshan72awan@gmail.com

   *   

   * Description of Multi Language Loader Hook

   */





if (!defined('BASEPATH')) exit ('No direct script access allowed');

 

class MultiLanguageSwitcher extends CI_Controller

{

    public function __construct() {

        parent::__construct();     

    }





    // create language Switcher method

    function switch($language = "")  
    {        

        $language = ($language != "") ? $language : "english";

        $this->session->set_userdata('site_lang', $language);        

        redirect($_SERVER['HTTP_REFERER']);
    }



}





?>