<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Profile extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */



	public function __construct()

	{

		parent::__construct();



		$this->load->model('CommonModel'); 



		if(!$this->session->userdata('user_id'))

		{

			redirect('home');

		}

	}





	 



	public function user_profile()

	{

		$user_id   =  $this->session->userdata('user_id');





		$data['user_date'] = $this->CommonModel->getAll('user',' user_id= "'.$user_id.'" ');







		$this->load->view('user/profile/profile',$data);

	}



 



	// public function signup_save()

	// {



	// 	if($this->input->post('save_data'))

	// 	{



	// 		$username   =  $this->input->post('username');

	// 		$email      =  $this->input->post('email');

	// 		$password   =  $this->input->post('password');

	// 		$cpassword  =  $this->input->post('cpassword');





	// 		if($password != $cpassword)

	// 		{

	// 			$error = lang('password_same_msg');

	// 		}else if(empty($username))

	// 		{

	// 			$error = lang('user_req_msg');

	// 		}else if(empty($email))

	// 		{

	// 			$error = lang('email_is_msg');

	// 		}else if(empty($password))

	// 		{

	// 			$error = lang('pwd_is_msg');

	// 		} 













	// 		if(isset($error) and !empty($error))

	// 		{

	// 			$this->session->set_flashdata('error',$error);

	// 			redirect($_SERVER['HTTP_REFERER']);



	// 		}else{



	// 			$data_to_insert = array(

	// 				'first_name' => $username,

	// 				'email'      => $email,

	// 				'password'   => $password,  

	// 			);





	// 			$id_user = $this->CommonModel->add('user',$data_to_insert);



	// 			if($id_user)

	// 			{	

	// 				$success =  lang('confirm_account_msg');



	// 				$this->session->set_flashdata('success',$success);

	// 				redirect($_SERVER['HTTP_REFERER']);		

	// 			}

	// 		}



	// 	}else{

	// 		redirect('home/signupn');

	// 	}

		 	

	// }







	public function save_general()

	{



		if($this->input->post('save_general'))

		{

			 

			$bio        =  $this->input->post('bio');

			$about      =  $this->input->post('about');

			$location   =  $this->input->post('location'); 



			$user_id   =  $this->session->userdata('user_id');



			$data_to_update = array(

				'bio'        => $bio,

				'location'   => $location,

				'about'      => $about,  

			);





			$result = $this->CommonModel->update('user',' user_id= "'.$user_id.'" ',$data_to_update);



			if($result)

			{	

				$success =  lang('update_success');



				$this->session->set_flashdata('success',$success);

				redirect($_SERVER['HTTP_REFERER']);		

			}else{

				$error =  lang('update_error');



				$this->session->set_flashdata('error',$error);

				redirect($_SERVER['HTTP_REFERER']);

			}

			 

		}else{

			redirect('home/signupn');

		}

		 	

	}



















	public function save_work()

	{



		if($this->input->post('save_work'))

		{

			 

			$roles           =  $this->input->post('roles');

			$project_type    =  $this->input->post('project_type');

			$your_service    =  $this->input->post('your_service'); 



			$user_id   =  $this->session->userdata('user_id');



			$data_to_update = array(

				'roles'          => $roles,

				'project_type'   => $project_type,

				'your_service'   => $your_service,  

			);





			$result = $this->CommonModel->update('user',' user_id= "'.$user_id.'" ',$data_to_update);



			if($result)

			{	

				$success =  lang('update_success');



				$this->session->set_flashdata('success',$success);

				redirect($_SERVER['HTTP_REFERER']);		

			}else{

				$error =  lang('update_error');



				$this->session->set_flashdata('error',$error);

				redirect($_SERVER['HTTP_REFERER']);

			}

			 

		}else{

			redirect('home/signupn');

		}

		 	

	}





	public function change_password()

	{



		 



		if($this->input->post('password'))

		{

			 

			$password          =  $this->input->post('password');

			$newpassword       =  $this->input->post('newpassword');

			$confirmpassword   =  $this->input->post('confirmpassword'); 



			if (empty($newpassword) or empty($confirmpassword)  ) 

			{

				$error =  'passwords is required';



				$this->session->set_flashdata('error',$error);

				redirect($_SERVER['HTTP_REFERER']);

			}







			if ( $newpassword != $confirmpassword   ) 

			{

				$error =  'Both passwords should be same';



				$this->session->set_flashdata('error',$error);

				redirect($_SERVER['HTTP_REFERER']);

				 

			}







			$user_id   =  $this->session->userdata('user_id');



			$data_to_update = array( 

				'password'   => md5($newpassword), 

			);





			$result = $this->CommonModel->update('user',' user_id= "'.$user_id.'" ',$data_to_update);



			if($result)

			{	

				$success =  lang('update_success');



				$this->session->set_flashdata('success',$success);

				redirect($_SERVER['HTTP_REFERER']);		

			}else{

				$error =  lang('update_error');



				$this->session->set_flashdata('error',$error);

				redirect($_SERVER['HTTP_REFERER']);

			}

			 

		}else{

			redirect('home/signupn');

		}

		 	

	}





	public function update_profilepicture()

	{	 



		if(!empty($_FILES))

		{

			$user_id   =  $this->session->userdata('user_id');

			$config['upload_path']          = 'assets/img/user_profiles/'.$user_id.'/';;   

            $config['allowed_types']        = 'jpg|jpeg|png';
            
            if (!file_exists($config['upload_path']))  
            { 
                mkdir($config['upload_path'], 0777, true); 
            }
            

            $this->load->library('upload', $config);



            if(!$this->upload->do_upload('profile_img')) 

            {               

                $result['error'] = $this->upload->display_errors();



                $this->session->set_flashdata('error',$result['error']);

                redirect($_SERVER['HTTP_REFERER']); 

            } else {

                $result = array('upload_data' => $this->upload->data());

                

                $file   = $result['upload_data']['file_name'];

            }





            

            $data_to_update = array(

            	'profile_pic' => $file,

            );



			$result = $this->CommonModel->update('user',' user_id= "'.$user_id.'" ',$data_to_update);



			if($result)

			{	

				$success =  lang('update_success'); 

				$this->session->set_flashdata('success',$success);

				redirect($_SERVER['HTTP_REFERER']);		

			}else{

				$error =  lang('update_error');



				$this->session->set_flashdata('error',$error);

				redirect($_SERVER['HTTP_REFERER']);

			}

			 

		}else{

			redirect($_SERVER['HTTP_REFERER']);

		}

		 	

	}





}

