<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		$this->load->model('CommonModel'); 
		$this->load->model('CommonModelAdmin');
	}


	public function index()
	{
		// extension_loaded('ffmpeg') or die("ffmpeg extension not loaded");
		// die();
		$this->load->view('user/home');
	}





	public function about()
	{
		$this->load->view('user/about_us');	
	}

	public function cookies()
	{
		$this->load->view('user/cookies');	
	}

	

	public function quality_video()
	{

		$page_content = $this->CommonModelAdmin->getPagesTemplates();
		 
		$data['page_template'] = $this->CommonModelAdmin->getPagesTemplateSetting();


		$data['body'] = $page_content->quality_video;

		$this->load->view('user/public_template',$data);	
	}


	public function privacy_control()
	{

		$data['page_template'] = $this->CommonModelAdmin->getPagesTemplateSetting();
		
		$page_content = $this->CommonModelAdmin->getPagesTemplates();
		 

		$data['body'] = $page_content->privacy_control;

		$this->load->view('user/public_template',$data);
	}

	public function team()
	{
		$page_content = $this->CommonModelAdmin->getPagesTemplates();
		 
		$data['page_template'] = $this->CommonModelAdmin->getPagesTemplateSetting();

		$data['body'] = $page_content->team;

		$this->load->view('user/public_template',$data);
	}

	public function public_grow()
	{
		$page_content = $this->CommonModelAdmin->getPagesTemplates();
		 
		$data['page_template'] = $this->CommonModelAdmin->getPagesTemplateSetting();

		$data['body'] = $page_content->public_pg;

		$this->load->view('user/public_template',$data);
	}


	public function streaming()
	{
		$page_content = $this->CommonModelAdmin->getPagesTemplates();
		 
		$data['page_template'] = $this->CommonModelAdmin->getPagesTemplateSetting();

		$data['body'] = $page_content->streaming;

		$this->load->view('user/public_template',$data);
	}

	public function contact()
	{
		$this->load->view('user/contact');	
	}


	public function pricing()
	{
		$this->load->view('user/pricing');	
	}



	public function terms()
	{
		$this->load->view('user/terms');	
	}


	public function copyright()
	{
		$this->load->view('user/copyright');	
	}



	public function privacy()
	{
		$this->load->view('user/privacy');	
	}



	public function login()
	{
		if($this->session->userdata('user_id'))
		{
			redirect('profile/user_profile');
		}
		$this->load->view('user/login');	
	}

	public function forget_password()
	{
		if($this->input->post('email'))
		{
			$email        =  $this->input->post('email'); 
			$where = " ( email = '$email'  ) AND is_admin = 0 ";

			$user_data = $this->CommonModel->user_login('user',$where);

			 

			if(!empty($user_data))
			{ 

				$setting = $this->CommonModelAdmin->getsetting();	

				$from_email   = $setting->company_email;
		    	$company_name = $setting->company_name; 
		    	// $email        = 'zeeshan72awan@gmail.com'; 
		    	//send email 
		    	$this->load->library('email');
				$this->load->library('parser');
				$config = array();
				$config['smtp_auth'] = true;
				$config['protocol']  = 'smtp';
				$config['smtp_host'] = 'vinheo.com';
				$config['smtp_user'] = $setting->host_user; 
				$config['smtp_pass'] = $setting->host_password; 
				$config['smtp_port'] = $setting->host_port; 

				$reset_code = substr(uniqid('', true), -5);
				$user_id    = $user_data[0]['user_id'];

				$link = base_url()."Home/reset_password?reset_code=".$reset_code."&email=".$email;

				$this->email->initialize($config);
				ob_start(); 
    			include_once 'assets/reset_password.html';
				$mail = ob_get_contents();

		        $this->email->set_header('Content-Type', 'text/html');
				$this->email->from($from_email,$company_name);
				$this->email->to($email);
				$this->email->subject($company_name.' - Reset Password');
				$this->email->message($mail); 
				$email_ye = $this->email->send(); 
				 
				ob_end_clean();

				if($email_ye)
				{
					$data_to_update = array(
						'reset_code'  => $reset_code,  
					);


					$this->CommonModel->update('user',' user_id= "'.$user_id.'" ',$data_to_update);

					$success =  lang('reset_msg_success'); 
					$this->session->set_flashdata('success',$success);
					redirect($_SERVER['HTTP_REFERER']);
				}

			}else {
				$error =  lang('email_not_correct');

				$this->session->set_flashdata('error',$error);
				redirect($_SERVER['HTTP_REFERER']);
			}

		}
		
		$this->load->view('user/forget_password');	
	}


	public function validate_login()
	{

		if($this->input->post('valide_login'))
		{ 
			 
			$email      =  $this->input->post('email');
			$password   =  $this->input->post('password');
			 


			if(empty($email))
			{
				$error = lang('email_is_msg');
			}else if(empty($password))
			{
				$error = lang('pwd_is_msg');
			} 






			if(isset($error) and !empty($error))
			{
				$this->session->set_flashdata('error',$error);
				redirect($_SERVER['HTTP_REFERER']);

			}else{

				$password = md5($password);

				$where = " email = '$email'  AND password = '$password' AND is_admin = 0 ";

				$user_data = $this->CommonModel->user_login('user',$where);

				if(empty($user_data))
				{
					$error = 'Invalid Email/Password.';

					$this->session->set_flashdata('error',$error);
					redirect($_SERVER['HTTP_REFERER']);

				} else if($user_data[0]['email_confirm'] == 'not')
				{
					$error = 'Your email is not verified';

					$this->session->set_flashdata('error',$error);
					redirect($_SERVER['HTTP_REFERER']);

				}else if ($user_data[0]['status'] == 'deactive')
				{
					$error = 'Your account is not active. Please contact support center.';
					$this->session->set_flashdata('error',$error);
					redirect($_SERVER['HTTP_REFERER']);
				}else{
					$this->session->set_userdata('user_id',$user_data[0]['user_id']);

					$this->session->set_userdata('user_email',$user_data[0]['email']);

					if (isset($_REQUEST['redirect_true'])) 
					{
						redirect($_REQUEST['redirect_true']);
					}else{
						redirect('profile/user_profile');
					}

 					
				}

				
			}

		}else{
			redirect('home/signupn');
		}
		 	
	}


	public function reset_password()
	{


		if($this->input->get('reset_code') and !empty($this->input->get('reset_code')) and $this->input->get('email') and !empty($this->input->get('email') ))
		{

			$reset_code   =  $this->input->get('reset_code'); 
			$email        =  $this->input->get('email'); 


			$where = "   email = '$email' and  reset_code = '$reset_code'  AND is_admin = 0 ";

			$user_data = $this->CommonModel->user_login('user',$where); 

			if(!empty($user_data))
			{ 
				$data = array();
				$data['email']      = $email;
				$data['reset_code'] = $reset_code;
				$this->load->view('user/change_password',$data);
			}else{
				redirect('home/forget_password');
			}

		}else{
			redirect('home/forget_password');
		}
		
	}

	public function fb_login()
	{
		$first_name   =  $this->input->post('first_name');
		$last_name    =  $this->input->post('last_name'); 
		$email        =  $this->input->post('email'); 
		$profile_pic  =  $this->input->post('profile_pic'); 

		if(empty($email))
		{
			echo "<div class='alert alert-warning alert-dismissible '>
					<strong>Warning! </strong>Your account does not have email address. 
					<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
						<span aria-hidden='true'>&times;</span> 
					</button>
				</div>";
			exit();
		}

		$where = " (email = '$email' and fb_user='yes'   ) AND is_admin = 0 ";

		$user_data = $this->CommonModel->user_login('user',$where);

		if(empty($user_data))
		{
			$where2 = " (email = '$email'   ) AND is_admin = 0 ";

			$user_data2 = $this->CommonModel->user_login('user',$where2);

			if(!empty($user_data2))
			{
				echo 	"<div class='alert alert-warning alert-dismissible '>
							<strong>Warning! </strong>".lang('email_registerd')." 
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span> 
							</button>
						</div>"; 
				exit(); 
			}


			$data_to_insert = array(
				'first_name'  => $first_name,
				'last_name'   => $last_name,
				'profile_pic' => $profile_pic,
				'email'       => $email,  
				'email_confirm'  => 'yes',  
				'status'       => 'active',  
				'fb_user'      => 'yes',  
			);


			$id_user = $this->CommonModel->add('user',$data_to_insert);


			if($id_user)
			{
				$this->session->set_userdata('user_id',$id_user);

				$this->session->set_userdata('user_email',$email);

				echo "logged_in";
			}


		} else{

			$this->session->set_userdata('user_id',$user_data[0]['user_id']);

			$this->session->set_userdata('user_email',$user_data[0]['email']);

			echo "logged_in";
		}
 

	}



	public function gm_login()
	{
		$first_name   =  $this->input->post('first_name'); 
		$email        =  $this->input->post('email'); 
		$profile_pic  =  $this->input->post('profile_pic'); 

		if(empty($email))
		{
			echo "<div class='alert alert-warning alert-dismissible '>
					<strong>Warning! </strong>Your account does not have email address. 
					<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
						<span aria-hidden='true'>&times;</span> 
					</button>
				</div>";
			exit();
		}

		$where = " (email = '$email' and gm_user='yes' ) AND is_admin = 0 ";

		$user_data = $this->CommonModel->user_login('user',$where);

		if(empty($user_data))
		{

			$where2 = " (email = '$email'   ) AND is_admin = 0 ";

			$user_data2 = $this->CommonModel->user_login('user',$where2);

			if(!empty($user_data2))
			{
				echo 	"<div class='alert alert-warning alert-dismissible '>
							<strong>Warning! </strong>".lang('email_registerd')." 
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span> 
							</button>
						</div>"; 
				exit(); 
			}

			$data_to_insert = array(
				'first_name'  => $first_name, 
				'profile_pic' => $profile_pic,
				'email'       => $email,
				'email_confirm'  => 'yes',  
				'status'       => 'active', 
				'gm_user'      => 'yes', 
			);


			$id_user = $this->CommonModel->add('user',$data_to_insert);


			if($id_user)
			{
				$this->session->set_userdata('user_id',$id_user);

				$this->session->set_userdata('user_email',$email);

				echo "logged_in";
			}


		} else{

			$this->session->set_userdata('user_id',$user_data[0]['user_id']);

			$this->session->set_userdata('user_email',$user_data[0]['email']);

			echo "logged_in";
		}
 

	}


	public function signup()
	{
		if($this->session->userdata('user_id'))
		{
			redirect('profile/user_profile');
		}
		$this->load->view('user/signup');	
	}



	public function logout()
	{
		$user_data = $this->session->all_userdata();
		 
        foreach ($user_data as $key => $value) {
            
            $this->session->unset_userdata($key);
            
        }
    	$this->session->sess_destroy();
		redirect('/home');
	}






	public function signup_save()
	{

		if($this->input->post('save_data'))
		{

			$username   =  $this->input->post('username');
			$email      =  $this->input->post('email');
			$password   =  $this->input->post('password');
			$cpassword  =  $this->input->post('cpassword');


			if($password != $cpassword)
			{
				$error = lang('password_same_msg');
			}else if(empty($username))
			{
				$error = lang('user_req_msg');
			}else if(empty($email))
			{
				$error = lang('email_is_msg');
			}else if(empty($password))
			{
				$error = lang('pwd_is_msg');
			} 






			if(isset($error) and !empty($error))
			{
				$this->session->set_flashdata('error',$error);
				redirect($_SERVER['HTTP_REFERER']);

			}else{

				$where = " (email = '$email' ) AND is_admin = 0 ";
				$user_data = $this->CommonModel->user_login('user',$where);

				if(!empty($user_data))
				{
					$error =  lang('email_registerd');

					$this->session->set_flashdata('error',$error);
					redirect($_SERVER['HTTP_REFERER']);
					exit();

				}


				$data_to_insert = array(
					'first_name' => $username,
					'email'      => $email,
					'password'   => md5($password),  
				);
 

				$id_user = $this->CommonModel->add('user',$data_to_insert);

				if($id_user)
				{	
 
    				$setting = $this->CommonModelAdmin->getsetting();	

					$from_email   = $setting->company_email;
			    	$company_name = $setting->company_name;  
			    	
			    	//send email 
			    	$this->load->library('email');
					$this->load->library('parser');
					$config = array();
					$config['smtp_auth'] = true;
					$config['protocol']  = 'smtp';
					$config['smtp_host'] = 'vinheo.com';
					$config['smtp_user'] = $setting->host_user; 
					$config['smtp_pass'] = $setting->host_password; 
					$config['smtp_port'] = $setting->host_port; 
					$link = base_url()."Home/verify/".urlencode($id_user);
					$this->email->initialize($config);
					ob_start(); 
	    			include_once 'assets/register.html';
					$mail = ob_get_contents();
			        $this->email->set_header('Content-Type', 'text/html');
					$this->email->from($from_email,$company_name);
					$this->email->to($email);
					$this->email->subject($company_name.' - Account Registered');
					$this->email->message($mail); 
					$email_ye = $this->email->send();
					
					 
					ob_end_clean();
					if($email_ye)
					{
						$success =  lang('confirm_account_msg');

						$this->session->set_flashdata('success',$success);
						redirect($_SERVER['HTTP_REFERER']);
					}

							
				}
			}

		}else{
			redirect('home/signupn');
		}
		 	
	}


	public function update_password()
	{
		if($this->input->post('save_data'))
		{

			$reset_code =  $this->input->post('reset_code');
			$email      =  $this->input->post('email');
			$password   =  $this->input->post('password');
			$cpassword  =  $this->input->post('cpassword');


			if($password != $cpassword)
			{
				$error = lang('password_same_msg');
			}else if(empty($password))
			{
				$error = lang('pwd_is_msg');
			} 






			if(isset($error) and !empty($error))
			{
				$this->session->set_flashdata('error',$error);
				redirect($_SERVER['HTTP_REFERER']);

			}else{

				$where = "   email = '".$email."' and  reset_code =  '".$reset_code."'   AND is_admin = 0 ";

				$user_data = $this->CommonModel->user_login('user',$where);  
			  

				if(empty($user_data))
				{
					$error =  lang('no_record_found');

					$this->session->set_flashdata('error',$error);
					redirect($_SERVER['HTTP_REFERER']);
					exit();

				}
 				
 				 
				$update_data = array(
					'reset_code'  => '',  
					'password'    => md5($password),  
		    	);

	    		$user_update = $this->CommonModel->update('user',' user_id="'.$user_data[0]['user_id'].'" ',$update_data); 
				
				if ($user_update) 
				{
					$success = lang('pwd_change_success');
					$this->session->set_flashdata('success',$success);  	
					redirect('home/login');
				}
				
			}

		}else{
			redirect('home/signupn');
		}
	}


	public function verify()
	{    
    	$id = $this->uri->segment(3);;
    	$id = urldecode($id);
    	 
    	$update_data = array(
			'email_confirm'  => 'yes',  
			'status'         => 'active',  
    	);
    	$user_update = $this->CommonModel->update('user',' user_id='.$id.' ',$update_data); 
		
		$success = 'Your account has been activated successfully.';
		$this->session->set_flashdata('success',$success);  	
		redirect('home/signup');
		exit();
	}


}
