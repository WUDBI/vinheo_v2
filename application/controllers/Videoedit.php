<?php



defined('BASEPATH') OR exit('No direct script access allowed');







class Videoedit extends CI_Controller {



 



	public function __construct() 

	{



		parent::__construct(); 

		$this->load->model('CommonModel');  

		if(!$this->session->userdata('user_id'))  
		{  
			redirect('home/signup');  
		}



	}











	public function index() 

	{



		$this->load->view('user/home');



	}











	public function upload_video() 

	{

    

		$this->load->view('user/profile/upload');



	}











	public function replace_video()  
	{ 
 
		$config['upload_path']     = 'assets/work_files/'.$this->session->userdata('user_id').'/';
		$video_id                  = $this->input->post('video_id');


		if (!file_exists($config['upload_path']))  
		{  
		    mkdir($config['upload_path'], 0777, true); 
		} 
 

        $config['allowed_types']   = 'mp4|mov|avi|mpeg4|flv|3gpp';  


        $this->load->library('upload', $config); 

        if(!$this->upload->do_upload('video_file'))   
        {                 
            $result['error'] = $this->upload->display_errors();   

        } else  

        { 

            $result = array('upload_data' => $this->upload->data()); 



            $file        = $result['upload_data']['file_name'];



            $file_size   = $result['upload_data']['file_size'];



            $file_ext    = $result['upload_data']['file_ext'];

 



            $update_data = array(  

            	'video_name' => $file, 

            	'video_size' => $file_size, 

            	'file_ext'   => $file_ext, 

            	'user_id'    => $this->session->userdata('user_id'), 

            );

            $result =  $this->CommonModel->update('video',' video_id ="'.$video_id.'"   ',$update_data);
 

        }   



	}





    public function uploadvideo_thumbnail()  
    {  

        $video_id  = $this->input->post('video_id'); 

        $base64String = $this->input->post('imgBase64');   

        define('UPLOAD_DIR', 'assets/video_thumnails/'.$this->session->userdata('user_id').'/'); 



        $img = str_replace('data:image/png;base64,', '', $base64String);

        $img = str_replace(' ', '+', $img);

        $data = base64_decode($img);

        $file = UPLOAD_DIR . uniqid() . '.png';

        $success = file_put_contents($file, $data);

         



        $insertd= array( 



            'user_id'    => $this->session->userdata('user_id'),



            'video_id'   => $video_id,



            'active'     => 'yes',



            'base_img'   => 'yes',



            'thumbnail'  => $file,



        );



        $video_thumbnail  =  $this->CommonModel->add('video_thumbnails',$insertd); 

        



        

        



        if($video_thumbnail) 

        { 

            $update_data = array(   

                'active'       => 'no', 

            ); 



            $result =  $this->CommonModel->update('video_thumbnails',' video_id ="'.$video_id.'" and id != "'.$video_thumbnail.'" ',$update_data);  



            $data_all['success'] = 'Image uploaded successfully';



        }else{  
            $error = 'There was problem while uploading.'; 
            $data_all['error'] = $error; 

        }  

        echo json_encode($data_all);                             
    }




    public function uploadvideo_randomthumbnail()  
    {  

        if ($this->input->post('dataURL')) 
        {
                    
            $video_id  = $this->input->post('video_id'); 

            $dataURL   = $this->input->post('dataURL');    


            $user_id   = $this->session->userdata('user_id');

            $img_name = 'random'.$video_id.'.jpg';
            $newName =  'assets/video_thumnails/'.$user_id.'/'.$img_name;

            $copied  = copy($dataURL , $newName);



            if ($copied) 
            {
                $insertd= array(  

                    'user_id'    => $this->session->userdata('user_id'), 

                    'video_id'   => $video_id, 

                    'active'     => 'yes', 

                    'base_img'   => 'no',  

                    'thumbnail'  => $img_name, 

                );



                $video_thumbnail  =  $this->CommonModel->add('video_thumbnails',$insertd); 
         



                if($video_thumbnail) 

                { 

                    $update_data = array(   

                        'active'       => 'no', 

                    ); 



                    $result =  $this->CommonModel->update('video_thumbnails',' video_id ="'.$video_id.'" and id != "'.$video_thumbnail.'" ',$update_data);  



                    $data_all['success'] = 'Image uploaded successfully'; 
                }else{  
                    $error = 'There was problem while uploading.'; 
                    $data_all['error'] = $error; 

                }  

            }else{  
                $error = 'There was problem while uploading.'; 
                $data_all['error'] = $error; 

            } 

            
            echo json_encode($data_all);    
        }                         
    }	



    public function update_thumbnail()
    {
        $video_id  = $this->input->post('video_id');
        $box       = $this->input->post('box');


        $update_data = array(    
            'active'       => 'no',  
        );  
        $result =  $this->CommonModel->update('video_thumbnails',' video_id ="'.$video_id.'"  ',$update_data); 



        $update_data = array(    
            'active'       => 'yes',  
        );  

        $result =  $this->CommonModel->update('video_thumbnails',' video_id ="'.$video_id.'" and id  = "'.$box.'" ',$update_data); 


       

        if($result)  
        {   
            $data_all['success'] = 'Changes uploaded successfully';
 
        }else{  
           $error = 'There was problem while uploading.'; 
            $data_all['error'] = $error;  
        } 
        echo json_encode($data_all);

    }













	public function update_date() 

	{ 



        if($this->input->post('video_id') and !empty($this->input->post('video_id')))



        {



            $video_id    =  $this->input->post('video_id');



            $title       =  $this->input->post('title');



            $description =  $this->input->post('description');



            $privacy     =  $this->input->post('privacy');



            $tags        =  $this->input->post('tags');



            $language    =  $this->input->post('language');

            

            $download    =  $this->input->post('download');







            $update_data = array(  



            	'title'       => $title,



            	'description' => $description,



            	'privacy'     => $privacy,



            	'tags'        => $tags, 



                'download'    => $download, 



            	'status '     => 'uploaded', 



            ); 



           	$result =  $this->CommonModel->update('video',' video_id ="'.$video_id.'" ',$update_data);



           	if($result) 

           	{ 

           		$success = 'The changes were successfully saved';



           		$this->session->set_flashdata('success',$success);



				redirect($_SERVER['HTTP_REFERER']);	



           	}else{ 



           		$error = 'There was problem while uploading your video please try again latter';



           		$this->session->set_flashdata('error',$error);



			    redirect($_SERVER['HTTP_REFERER']); 

           	}



            	                           



        }else{ 

            $error = 'Error! Please upload video.';



            $this->session->set_flashdata('error','Video is required');



            redirect('upload/all_videos');	 

       	}   



	}





 



	public function edit_video($id)  
	{



		$data = array();



		$user_id   =  $this->session->userdata('user_id'); 


        


		$data['video'] = $this->CommonModel->getAllVideos( ' video.user_id= "'.$user_id.'" and video_id= "'.$id.'" '); 

        
        $path_temp1 = 'assets/temp_thumbnails/'.$id;

        //get random images
        $data['random_images'] = glob($path_temp1 . "/*.jpg");
 
		$this->load->view('user/video/video_edit',$data); 

	}





    public function upload_thumbnail() 
    { 

        if ($this->input->post('video_id')) 

        { 

            $config['upload_path']     = 'assets/video_thumnails/'.$this->session->userdata('user_id').'/';



            if (!file_exists($config['upload_path']))  

            { 

                mkdir($config['upload_path'], 0777, true);

            } 



            $config['allowed_types']   = 'jpg|jpeg|png'; 

            



            $this->load->library('upload', $config); 

            if(!$this->upload->do_upload('file_thumbnail'))  

            {                

                $error = $this->upload->display_errors(); 

                $this->session->set_flashdata('error',$error);



                redirect($_SERVER['HTTP_REFERER']); 



            } else  

            { 

                $result = array('upload_data' => $this->upload->data()); 



                $file        = $result['upload_data']['file_name'];



                $file_size   = $result['upload_data']['file_size'];



                $file_ext    = $result['upload_data']['file_ext'];

     

                $video_id    = $this->input->post('video_id');



                $insertd= array( 



                    'user_id'    => $this->session->userdata('user_id'),



                    'video_id'   => $video_id,



                    'active'     => 'yes',



                    'base_img'   => 'no',



                    'thumbnail'  => $file,



                );



                $video_thumbnail  =  $this->CommonModel->add('video_thumbnails',$insertd);   


                if($video_thumbnail) 

                { 

                    $update_data = array(   

                        'active'       => 'no', 

                    ); 



                    $result =  $this->CommonModel->update('video_thumbnails',' video_id ="'.$video_id.'" and id != "'.$video_thumbnail.'" ',$update_data);  



                    redirect($_SERVER['HTTP_REFERER']);  
 
                }else{ 
 
                    $error = 'There was problem while uploading.'; 
                    $this->session->set_flashdata('error',$error); 
                    redirect($_SERVER['HTTP_REFERER']);   

                }                         
            }   
        }  
    }



	public function  thumbnail() 
	{  
        $file          = $this->input->post('imgBase64');

        $video_id      = $this->input->post('video_id'); 

        $update_data = array(  
        	'thumbnail' => $file,   
        ); 
       	$result =  $this->CommonModel->update('video',' video_id ="'.$video_id.'" ',$update_data);
 
       echo json_encode($result);  
	}


    public function delete_video($id)
    {
        
        if ($id) 
        {
            $user_id = $this->session->userdata('user_id');

            $video = $this->CommonModel->getAllVideos( ' video.user_id= "'.$user_id.'" and video_id= "'.$id.'" '); 


            if (isset($video[0])) 
            {
                $result = $this->CommonModel->delete('video',' video_id='.$id.' and user_id='.$user_id);

                if($result)  
                {   

                    $this->CommonModel->delete('video_thumbnails',' video_id='.$id.' '); 


                    $file_name =  'assets/work_files/'.$user_id.'/'.$video[0]->video_name;
                    if(is_file($file_name))
                    { 
                        unlink($file_name); 
                    } 
                    
                    $error = 'Your video has been deleted successfully.'; 
                    $this->session->set_flashdata('success',$error);
                    redirect('upload/all_videos');   
                }else{  
                    $error = 'There was problem while uploading.'; 
                    $this->session->set_flashdata('error',$error); 
                    redirect('upload/all_videos');    
                }  
            }

            
        }
        
        
    }   


    public function delete_thumbnail($id,$v_id)
    {
        
        if ($id) 
        {
            $user_id = $this->session->userdata('user_id');

            $video = $this->CommonModel->getAll( 'video_thumbnails',' video_id ="'.$v_id.'" and user_id ="'.$user_id.'" order by id desc limit 2'); 

             
            
            $result = $this->CommonModel->delete('video_thumbnails',' video_id ="'.$v_id.'" and user_id ="'.$user_id.'" and id= "'.$id.'"  ');

            if($result)  
            {   

                $update_data = array(    
                    'active'       => 'yes',  
                ); 

                if (isset($video[1]->id)) 
                { 
                    $this->CommonModel->update('video_thumbnails',' video_id ="'.$v_id.'" and id = "'.$video[1]->id.'" ',$update_data);  
                }

                // $file_name =  'assets/work_files/'.$user_id.'/'.$video[0]->video_name;
                // if(is_file($file_name))
                // { 
                //     unlink($file_name); 
                // } 
                
                $error = 'Your video thumbnail has been deleted successfully.'; 
                $this->session->set_flashdata('success',$error);
                redirect('videoedit/edit_video/'.$v_id);   
            }else{  
                $error = 'There was problem while deleting thumbnail.'; 
                $this->session->set_flashdata('error',$error); 
                redirect('upload/all_videos');    
            }  
            
            
        }
        
        
    }




}



