<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Upload extends CI_Controller {


 



	public function __construct()

	{

		parent::__construct();



		$this->load->model('CommonModel'); 

		

		if(!$this->session->userdata('user_id'))

		{

			redirect('home/signup');

		}

	}





	public function index()

	{

		$this->load->view('user/home');

	}





	public function upload_video() 
	{
    	
    	$user_id = $this->session->userdata('user_id');

    	$user    = $this->CommonModel->getByIdCustom('user','user_id= "'.$user_id.'"');




    	$weekly_limit = 0;
    	$weekly_limit_name = '';
    	$total_limit  = 0;
    	$total_limit_name  = '';
    	if ($user->user_package == 'plus') 
    	{
    		$weekly_limit = 5;
    		$total_limit  = 250;

    		$weekly_limit_name  = 'GB';
    		$total_limit_name   = 'GB';
    	}elseif ($user->user_package == 'pro') 
    	{
    		$weekly_limit = 20;
    		$total_limit  = 1;

    		$weekly_limit_name  = 'GB';
    		$total_limit_name   = 'TB';
    	}elseif ($user->user_package == 'business') 
    	{
    		$weekly_limit = 0;
    		$total_limit  = 5;
    		$weekly_limit_name  = '';
    		$total_limit_name   = 'TB';
    	}elseif ($user->user_package == 'premium') 
    	{
    		$weekly_limit = 0;
    		$total_limit  = 7;
    		$weekly_limit_name  = '';
    		$total_limit_name   = 'TB';
    	}else{
            $weekly_limit = 500;
    		$total_limit  = 5;

    		$weekly_limit_name  = 'MB';
    		$total_limit_name   = 'GB';
        }


    	$data['weekly_limit']        = $weekly_limit;
    	$data['total_limit']         = $total_limit;
    	$data['weekly_limit_name']   = $weekly_limit_name;
    	$data['total_limit_name']    = $total_limit_name; 
    	///////////end of total storage 




        if (!isset($no_calculation )) 
        {
        	//get week data storage 
        	$monday = strtotime('monday this week');
    		$sunday = strtotime('sunday this week');
        	

        	$monday =  date('Y-m-d', $monday);
        	$sunday =  date('Y-m-d', $sunday);
     


        	$weekly_size = $this->CommonModel->getAllVideoSize( ' video.user_id= "'.$user_id.'"  and   DATE_FORMAT(video.created_on, "%Y-%m-%d") >= "'.$monday.'" AND  DATE_FORMAT(video.created_on, "%Y-%m-%d") <= "'.$sunday.'"   ');
        	

        	$weekly_size = $this->formatSizeUnits($weekly_size*1024);

        	$data['weekly_size'] = $weekly_size;

        	//end of weekly



        
            
        
        	//total size and percentage
        	$total_size = $this->CommonModel->getAllVideoSize( ' video.user_id= "'.$user_id.'"     ');  


        	$total_limit_available = $this->convertIntoBytes($total_limit,$total_limit_name); 
        	$total_size2           = $this->convertIntoBytes($total_size,'KB');

        	 
        	 
        	 

        	$data['total_size'] = $this->formatSizeUnits($total_size*1024);
        	 
        	if ($total_size2 > $total_limit_available) 
        	{
        		 
        	}else{ 

        		$percentage =  $total_size2 /  $total_limit_available *100;
        		 
        		$data['percentage'] = sprintf('%f', $percentage);
        	}
     
        }



		$this->load->view('user/video/upload',$data);

	}



	public function formatSizeUnits($size, $precision = 1, $show = "")
	{
	    $b  = $size;
	    $kb = round($size / 1024, $precision);
	    $mb = round($kb / 1024, $precision);
	    $gb = round($mb / 1024, $precision);

	    if($kb == 0 || $show == "B") {
	        return $b . " bytes";
	    } else if($mb == 0 || $show == "KB") {
	        return $kb . "KB";
	    } else if($gb == 0 || $show == "MB") {
	        return $mb . "MB";
	    } else {
	        return $gb . "GB";
	    }
	}
	

 
 
 
	public function save_video() 
	{



		$config['upload_path']     = 'assets/work_files/'.$this->session->userdata('user_id').'/';

		if (!file_exists($config['upload_path']))  
		{ 
		    mkdir($config['upload_path'], 0777, true); 
		} 

        $config['allowed_types']   = 'mp4|mov|avi|mpeg4|flv|3gpp';

        

        $this->load->library('upload', $config); 

        if(!$this->upload->do_upload('file'))  
        {               

            $result['error'] = $this->upload->display_errors();   

        } else  
        {

            $result = array('upload_data' => $this->upload->data());

             

            $file        = $result['upload_data']['file_name'];

            $file_size   = $result['upload_data']['file_size'];

            $file_ext    = $result['upload_data']['file_ext'];


            // $video = $_FILES["video"]["name"];

			 


            $insertd= array( 

            	'video_name' => $file,

            	'video_size' => $file_size,

            	'file_ext'   => $file_ext,

            	'user_id'    => $this->session->userdata('user_id'),

            );


            

           	$data_all['video_id']  =  $this->CommonModel->add('video',$insertd);
            $data_all['file_name'] =  $file;




            //thumbnail active

            $video_id   = $data_all['video_id'];
            $user_id    = $this->session->userdata('user_id');

            $video_f    = 'assets/work_files/'.$user_id.'/'.$file;
            $interval   = 5;

            $thumbnail_name = 'shot_'.$video_id;

            $command = "usr/bin/ffmpeg -i $video_f -ss $interval -vframes 1   assets/video_thumnails/$user_id/$thumbnail_name.jpg";
            shell_exec($command);





            $insertthumbnail= array( 

                'thumbnail' => $thumbnail_name.'.jpg',

                'base_img'  => 'no',

                'active'    => 'yes',

                'video_id'  =>  $video_id,

                'user_id'   =>  $user_id,

            );

            $this->CommonModel->add('video_thumbnails',$insertthumbnail);

            //end 



            //generate random thumbnails 
            $video_name = $video_f; 
 
            $path_temp1 = 'assets/temp_thumbnails/'.$video_id;
            

            if (!file_exists($path_temp1))  
            { 
                mkdir($path_temp1, 0777, true); 
            } 

            $path_temp1  = $path_temp1.'/ythumb%3d.jpg'; 

            $command2 = "usr/bin/ffmpeg  -i $video_name -f image2 -vf fps=fps=1/10 $path_temp1";
            shell_exec($command2);

            //random thumbs end

 


           echo json_encode($data_all);	                           

        }  
  
	}	







	public function save_video_data() 
	{
 

        if($this->input->post('video_id') and !empty($this->input->post('video_id'))) 
        {
 


            foreach ($this->input->post('title') as $key => $value) 
            {
                if (!empty($this->input->post('title')[$key])) 
                {
                       
                    $video_id    =  $this->input->post('video_id')[$key];

                    $title       =  $this->input->post('title')[$key];

                    $description =  $this->input->post('description')[$key];

                    $privacy     =  $this->input->post('privacy')[$key];

                    $tags        =  $this->input->post('tags')[$key];

                    $language    =  $this->input->post('language')[$key];
                    
                    $download    =  $this->input->post('download')[$key];



                    $update_data = array(  

                    	'title'       => $title,

                    	'description' => $description,

                    	'privacy'     => $privacy,

                    	'tags'        => $tags,

                    	'language'    => $language, 

                        'download'    => $download, 

                    	'status '     => 'uploaded', 

                    ); 

                   	$result =  $this->CommonModel->update('video',' video_id ="'.$video_id.'" ',$update_data);
                }
            }

           	if($result) 
           	{

           		$success = 'Your video has been uploaded successfully';

           		$this->session->set_flashdata('success',$success);

				redirect('upload/all_videos');	

           	}else{



           		$error = 'There was problem while uploading your video please try again latter';

           		$this->session->set_flashdata('error',$error);

				redirect('upload/all_videos');	 
           	 

           	}

            	                           

        }else{



       		 $error = 'Error! Please upload video.';

       		 $this->session->set_flashdata('error','Video is required');

			     redirect('upload/all_videos');	 

       	}    
	}



 

 

	public function all_videos() 
	{

		$user_id   =  $this->session->userdata('user_id');
		
// 		$file   = 'Vue_js_2_Tutorial_-_2_-_Getting_Started(720p).mp4';
// 		$video_id   = 1;
		
// 		$video_f    = 'assets/work_files/'.$user_id.'/'.$file;
//         $interval   = 5;

//         $thumbnail_name = 'shot_'.$video_id;
        
//         function isEnabled($func) {
// return is_callable($func) && false === stripos(ini_get('disable_functions'), $func);
// }
// $enabled = true;
// if (!isEnabled('shell_exec')) {
//  echo 'true';
// }else{
//      echo 'error';
// }

  //       $command = "public_html/usr/bin/ffmpeg.exe -i $video_f -ss $interval -vframes 1   assets/video_thumnails/$user_id/$thumbnail_name.jpg";
  //       if(shell_exec($command ))
  //       {
  //           echo 'success';
  //       }
        
  //       else{
  //             echo 'error';
  //       }
        
		
		// die();


		$total = $this->CommonModel->getAllVideosCount( ' video.user_id= "'.$user_id.'" ');

        $this->load->library('pagination');
        $config['base_url']   = site_url('upload/all_videos/');
        $config['total_rows'] = $total;
        $config['per_page']   = 6;
        $config['reuse_query_string'] = TRUE;

        $offset = 0;
        if ($this->uri->segment(3))
        {
            $offset = $this->uri->segment(3);
        } 

        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();

        

        $this->db->limit($config['per_page'], $offset);

        $sort_by= '';
        if (isset($_GET['sort_by']) and $_GET['sort_by'] == 'oldest') 
        {
            $sort_by = ' order by created_on ASC ';
        }

        $search= '';
        if (isset($_GET['search']) and !empty($_GET['search'])) 
        {
            $search = '  and   video.title LIKE "'.$_GET['search'].'%"    ';
        }


        if (isset($_GET['sort_by']) and $_GET['sort_by'] == 'newest') 
        {
            $sort_by = ' order by created_on DESC ';
        }

	    $data['all_videos'] = $this->CommonModel->getAllVideos( ' video.user_id= "'.$user_id.'"  '. $search.'   '. $sort_by.'  ');



	  	$this->load->view('user/video/my_videos',$data);

        

	}




	public function detail($id) 
	{

		$data = array();

		$user_id   =  $this->session->userdata('user_id');



		$data['video'] = $this->CommonModel->getAllVideos( ' video.user_id= "'.$user_id.'" and video_id= "'.$id.'" ');

		// $data['all_videos'] = $this->CommonModel->getAll('video',' user_id= "'.$user_id.'" ');



		$this->load->view('user/video/video_detail',$data); 
	}



	public function  thumbnail()
	{ 
 
        $file          = $this->input->post('imgBase64');
        $video_id      = $this->input->post('video_id');


        $update_data = array( 

        	'thumbnail' => $file, 

        );

       	$result =  $this->CommonModel->update('video',' video_id ="'.$video_id.'" ',$update_data);



       echo json_encode($result);	                           

        
	}









	function convertIntoBytes($number,$types)
	{
		$size_is = 0;
		if ($types == 'GB') 
		{
			$size_is =  1073741824*$number;
		}elseif ($types == 'KB') 
		{
			$size_is =  1024*$number;
		}elseif ($types == 'TB') 
		{
			$size_is =  1099511627776*$number;
		}

		return $size_is;
	}



}

