<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends CI_Controller{



	public function __construct(){

		parent::__construct();

		$this->load->model('CommonModelAdmin');
		$this->load->model('CommonModel');

		if(!$this->session->userdata('adminid')) 
		redirect('admin');

	}



	public function active_package()
	{

		if(isset($_SESSION['adminid']))
		{
			$result = array();
 

			$result['active_package'] = $this->CommonModel->getAllActivePackages('payments','   1 GROUP BY payments.user_id order by payments.id desc');
			 

			$result['active_setting_users_package'] = true;

			$this->load->view('admin/reports/setting_users_package',$result);

		}
		else{
	   		redirect('admin');
	   	}

	}	








	public function edit_active_package($id)
	{

		if(isset($_SESSION['adminid']))
		{
			$result = array();
 

			$active_package = $this->CommonModel->getAllActivePackages('payments',' id="'.$id.'"    GROUP BY payments.user_id order by payments.id desc');


			$result['active_package'] =  $active_package[0];

			 
			 

			$result['active_setting_users_package'] = true;

			$this->load->view('admin/payments/edit_active_package',$result);

		}
		else{
	   		redirect('admin');
	   	}

	}


	public function update_package()
	{

		if(isset($_SESSION['adminid']))
		{ 

			$id = $_POST['uid']; 

			$data_update = array(	
				'start_date' => $_POST['start_date'],
				'end_date'   => $_POST['end_date'],
			);

	        $this->CommonModel->update('payments',' id ="'.$id.'" ', $data_update); 

	        $this->session->set_flashdata('success', 'Data updated successfully');  

	        redirect('admin/setting_users_package');

		}
		else{
	   		redirect('admin');
	   	}

	}


	public function subscription_report()
	{

		$result = array(); 

		$result['active_package'] = $this->CommonModel->getAllActivePackages('payments','   1  order by payments.id desc');
		 

		$result['active_users_package'] = true;

		$this->load->view('admin/reports/users_package',$result);


	}










} 

?>