<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('CommonModelAdmin'); 
		$this->load->model('CommonModel'); 
	}




	
	public function index(){
		 
		if(isset($_SESSION['adminid']))
		{
			redirect('admin/dashboard');
		}

		$result = array();
		$this->load->view('admin/login',$result);
	}



	public function about_us(){
		 
		if(isset($_SESSION['adminid']))
		{
			redirect('admin/dashboard');
		}

		$result = array();
		$this->load->view('admin/login',$result);
	}
	
	public function userlogin(){

		$uname = $this->input->post('user_name');
		$pass  = $this->input->post('password');


		$result = $this->CommonModelAdmin->adminlogin($uname,$pass);
		
		if($result == true){
			redirect('admin/dashboard');
		}else{
			redirect('Admin');
		}
	}

	public function dashboard(){

		if(isset($_SESSION['adminid'])){
			$result = array();

			$conditions = array();

			$result['total_customers'] = $this->CommonModelAdmin->getTotalRecords('user', $conditions);
			// $result['total_invoices']  = $this->CommonModelAdmin->getTotalRecords('invoices', $conditions);

			$result['active_dash'] = true;

			$this->load->view('admin/home',$result);

		}
		else{
	   		redirect('Admin');
	   }
	}

	
	public function setting()
	{

	  
		if(isset($_SESSION['adminid']))
		{

			$result = array();
			if(isset($_POST['submit']))
			{


				$data = $this->input->post();

				$data['stripe_publish_key'] = base64_encode(utf8_encode($data['stripe_publish_key']));
				$data['stripe_secret_key']  = base64_encode(utf8_encode($data['stripe_secret_key'])); 
				
				unset($data['submit']);
				$this->CommonModelAdmin->updateData('setting',1,$data);

				$this->session->set_flashdata('success', 'Settings updated successfully');

 				redirect($_SERVER['HTTP_REFERER']); 
			}

			$result['setting'] = $this->CommonModelAdmin->getsetting();
			
			$result['setting']->stripe_publish_key =  utf8_decode(base64_decode($result['setting']->stripe_publish_key));

			$result['setting']->stripe_secret_key =  utf8_decode(base64_decode($result['setting']->stripe_secret_key));
			
			$result['setting_active'] = true;
			// $result['timezones'] = $this->generate_timezone_list();

			$this->load->view('admin/setting',$result);
		}
		else{
			redirect('Admin');
		}
			
		
	}
	
 

	public function package_setting()
	{

	  
		if(isset($_SESSION['adminid']))
		{

			$result = array();
			if(isset($_POST['submit']))
			{


				$data = $this->input->post();

				// $data['stripe_publish_key'] = base64_encode(utf8_encode($data['stripe_publish_key']));
				// $data['stripe_secret_key']  = base64_encode(utf8_encode($data['stripe_secret_key'])); 
				
				unset($data['submit']);
				$this->CommonModelAdmin->updateData('setting',1,$data);

				$this->session->set_flashdata('success', 'Settings updated successfully');

 				redirect($_SERVER['HTTP_REFERER']); 
			}

			$result['setting'] = $this->CommonModelAdmin->getsetting();
			
			 
			
			$result['package_setting_active'] = true;
			// $result['timezones'] = $this->generate_timezone_list();

			$this->load->view('admin/package_setting',$result);
		}
		else{
			redirect('Admin');
		}
			
		
	}
	
	
	public function profile(){
		 
		$result = array();
		$adminid = $_SESSION['adminid'];

		if(isset($_POST['submit']))
		{
			$data = $this->input->post();
			unset($data['submit']);

			 
	        if(empty($data['password'])){
	        	unset($data['password']);
	        }
	        
	        $this->session->set_flashdata('success', 'Profile updated successfully');
	        $this->CommonModelAdmin->updateadmin($data,$adminid);
	        redirect($_SERVER['HTTP_REFERER']);
		}
	
		$result['admin_profile'] = $this->CommonModelAdmin->getByid('user',$adminid);
		$this->load->view('admin/user/profile',$result);
		 
	}

	public function logout(){

		$this->session->unset_userdata('adminid');
		$this->session->unset_userdata('adminname');
		
		redirect('admin');
	}

	public function email_templates()
	{
		if(isset($_SESSION['adminid'])){

			$result = array();
			
			if(isset($_POST['submit'])){
				$data = $_POST;
				unset($data['submit']);
				unset($data['files']);
				$this->CommonModelAdmin->update_template($data);
				$this->session->set_flashdata('success', 'Email templates updated successfully');
			}

			$result['page_content'] = $this->CommonModelAdmin->getemailtemplate();
			$result['template_active'] = true;
			$this->load->view('admin/email_templates',$result);

		}
		else{
	   		redirect('Admin');
	   	}
	}




	public function pages_templates()
	{
		if(isset($_SESSION['adminid'])){

			$result = array();
			
			if(isset($_POST['submit']))
			{
				$data = $_POST;

				unset($data['submit']);
				unset($data['files']);
				$this->CommonModelAdmin->updatePages($data);
				 
				$this->session->set_flashdata('success', 'Pages templates updated successfully');
				redirect($_SERVER['HTTP_REFERER']);
			}

			$result['page_content'] = $this->CommonModelAdmin->getPagesTemplates();
			$result['template_pages_active'] = true;
			$this->load->view('admin/pages_templates',$result);

		}
		else{
	   		redirect('Admin');
	   	}
	}



	public function pages_template_setting()
	{
		if(isset($_SESSION['adminid']))
		{

			$result = array();
			 
			if(isset($_POST['update']))
			{
				 

				if(isset($_FILES['fileToUpload']['name']) and !empty($_FILES['fileToUpload']['name']))
		    	{
 
			        $config['upload_path']          = 'assets/template_setting/';   
			        $config['allowed_types']        = 'jpg|jpeg|png';
	                
			        $this->load->library('upload', $config);

			        if(!$this->upload->do_upload('fileToUpload')) 
			        {               
			            $result['error'] = $this->upload->display_errors(); 
			            $this->session->set_flashdata('error', $result['error']);
			            redirect($_SERVER['HTTP_REFERER']);

			        } else {
			            $result = array('upload_data' => $this->upload->data());
			            
			            $file   = $result['upload_data']['file_name'];

			            $update_data= array( 
			            	'page_template_image' => $file,
			            );
			            $this->CommonModelAdmin->updateData('page_template_setting',1,$update_data);

			                                           
			        } 
			    }

			    $update_data= array( 
	            	'page_template_content' => $this->input->post('page_template_content'),
	            	'page_template_heading' => $this->input->post('page_template_heading'), 
	            );
				

				$this->CommonModelAdmin->updateData('page_template_setting',1,$update_data);
				 
				$this->session->set_flashdata('success', 'Pages template Setting updated successfully');
				redirect($_SERVER['HTTP_REFERER']);
			}

			$result['page_template'] = $this->CommonModelAdmin->getPagesTemplateSetting();


			$result['pages_template_setting'] = true;
			$this->load->view('admin/page_template_setting',$result);

		}
		else{
	   		redirect('Admin');
	   	}

	}


	public function setting_users_package()
	{

		if(isset($_SESSION['adminid']))
		{
			$result = array();
 

			$result['active_package'] = $this->CommonModel->getAllActivePackages('payments','   1 GROUP BY payments.user_id order by payments.id desc');
			 

			$result['active_setting_users_package'] = true;

			$this->load->view('admin/payments/setting_users_package',$result);

		}
		else{
	   		redirect('admin');
	   	}

	}	








	public function edit_active_package($id)
	{

		if(isset($_SESSION['adminid']))
		{
			$result = array();
 

			$active_package = $this->CommonModel->getAllActivePackages('payments',' id="'.$id.'"    GROUP BY payments.user_id order by payments.id desc');


			$result['active_package'] =  $active_package[0];

			 
			 

			$result['active_setting_users_package'] = true;

			$this->load->view('admin/payments/edit_active_package',$result);

		}
		else{
	   		redirect('admin');
	   	}

	}


	public function update_package()
	{

		if(isset($_SESSION['adminid']))
		{ 

			$id = $_POST['uid']; 

			$data_update = array(	
				'start_date' => $_POST['start_date'],
				'end_date'   => $_POST['end_date'],
			);

	        $this->CommonModel->update('payments',' id ="'.$id.'" ', $data_update); 

	        $this->session->set_flashdata('success', 'Data updated successfully');  

	        redirect('admin/setting_users_package');

		}
		else{
	   		redirect('admin');
	   	}

	}


	public function users_package()
	{

		if(isset($_SESSION['adminid']))
		{
			$result = array();
 

			$result['active_package'] = $this->CommonModel->getAllActivePackages('payments','   1  order by payments.id desc');
			 

			$result['active_users_package'] = true;

			$this->load->view('admin/payments/users_package',$result);

		}
		else{
	   		redirect('admin');
	   	}

	}
	
} 
?>