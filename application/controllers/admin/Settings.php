<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller{



	public function __construct()

	{

		parent::__construct();



		$this->load->model('CommonModelAdmin'); 

		$this->load->model('CommonModel'); 

	}









	

	public function index()

	{ 

		$result = array();

		if(isset($_POST['submit']))

		{





			$data = $this->input->post();



			$data['stripe_publish_key'] = base64_encode(utf8_encode($data['stripe_publish_key']));

			$data['stripe_secret_key']  = base64_encode(utf8_encode($data['stripe_secret_key'])); 

			

			unset($data['submit']);

			$this->CommonModelAdmin->updateData('setting',1,$data);



			$this->session->set_flashdata('success', 'Settings updated successfully');



				redirect($_SERVER['HTTP_REFERER']); 

		}



		$result['setting'] = $this->CommonModelAdmin->getsetting();

		

		$result['setting']->stripe_publish_key =  utf8_decode(base64_decode($result['setting']->stripe_publish_key));



		$result['setting']->stripe_secret_key =  utf8_decode(base64_decode($result['setting']->stripe_secret_key));

		

		$result['setting_active'] = true;





		$this->load->view('admin/settings/site_settings',$result);

	}







	public function prices_setting()

	{

 

		$result = array();

		if(isset($_POST['submit']))

		{





			$data = $this->input->post();



			// $data['stripe_publish_key'] = base64_encode(utf8_encode($data['stripe_publish_key']));

			// $data['stripe_secret_key']  = base64_encode(utf8_encode($data['stripe_secret_key'])); 

			

			unset($data['submit']);

			$this->CommonModelAdmin->updateData('setting',1,$data);



			$this->session->set_flashdata('success', 'Settings updated successfully');



				redirect($_SERVER['HTTP_REFERER']); 

		}



		$result['setting'] = $this->CommonModelAdmin->getsetting();

		 

		$result['package_setting_active'] = true; 



		$this->load->view('admin/settings/prices_setting',$result); 

		

	}

	





	public function pages_template_setting()

	{

 

		$result = array();

			 

		if(isset($_POST['update']))

		{

			 



			if(isset($_FILES['fileToUpload']['name']) and !empty($_FILES['fileToUpload']['name']))

	    	{



		        $config['upload_path']          = 'assets/template_setting/';   

		        $config['allowed_types']        = 'jpg|jpeg|png';

                

		        $this->load->library('upload', $config);



		        if(!$this->upload->do_upload('fileToUpload')) 

		        {               

		            $result['error'] = $this->upload->display_errors(); 

		            $this->session->set_flashdata('error', $result['error']);

		            redirect($_SERVER['HTTP_REFERER']);



		        } else {

		            $result = array('upload_data' => $this->upload->data());

		            

		            $file   = $result['upload_data']['file_name'];



		            $update_data= array( 

		            	'page_template_image' => $file,

		            );

		            $this->CommonModelAdmin->updateData('page_template_setting',1,$update_data);



		                                           

		        } 

		    }



		    $update_data= array( 

            	'page_template_content' => $this->input->post('page_template_content'),

            	'page_template_heading' => $this->input->post('page_template_heading'), 

            );

			



			$this->CommonModelAdmin->updateData('page_template_setting',1,$update_data);

			 

			$this->session->set_flashdata('success', 'Pages template Setting updated successfully');

			redirect($_SERVER['HTTP_REFERER']);

		}



		$result['page_template'] = $this->CommonModelAdmin->getPagesTemplateSetting();





		$result['pages_template_setting'] = true;

		$this->load->view('admin/settings/page_template_setting',$result);

		

	}

















	public function about_us()

	{

 

		$result = array();

			 

		if(isset($_POST['update']))

		{ 



		    $update_data= array( 

            	'about_us_heading' => $this->input->post('about_us_heading'),

            	'about_us_detail'  => $this->input->post('about_us_detail'), 
            	'banner_heading'   => $this->input->post('banner_heading'), 
            	'banner_detail'    => $this->input->post('banner_detail'), 

            );

			



			$this->CommonModelAdmin->updateData('pages_templates',1,$update_data);

			 

			$this->session->set_flashdata('success', 'Data has been updated successfully');

			redirect($_SERVER['HTTP_REFERER']);

		}



		$result['about_us_data'] = $this->CommonModelAdmin->getPagesTemplates();





		$result['pages_template_setting'] = true;

		$this->load->view('admin/settings/about_us',$result);

		

	}

	

	

} 

?>