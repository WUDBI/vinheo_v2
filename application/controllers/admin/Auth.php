<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Auth extends CI_Controller {



	function __construct()
	{

		parent::__construct();

		$this->load->model('CommonModelAdmin'); 
		$this->load->model('CommonModel'); 


 		if(isset($_SESSION['adminid']))
		{
			redirect('admin/dashboard');
		}
	 
	}


	public function index()
	{
		  
		$result = array();
		$this->load->view('admin/login',$result);
	}



	public function userlogin()
	{

		$uname = $this->input->post('user_name');
		$pass  = $this->input->post('password');


		$result = $this->CommonModelAdmin->adminlogin($uname,$pass);
		 
		if($result == true)
		{
			redirect('admin/dashboard');
		}else {
			$error =  lang('email_not_correct'); 
			$this->session->set_flashdata('error',$error);
			redirect($_SERVER['HTTP_REFERER']);
		} 
	}



	 
}

