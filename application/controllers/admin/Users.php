<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller{



	public function __construct(){

		parent::__construct();

		$this->load->model('CommonModelAdmin');

		if(!$this->session->userdata('adminid')) 
		redirect('Admin');

	}



	public function all() 
	{

		$conditions = array();


		$data['Customers'] = true;
		$data['customers'] = $this->CommonModelAdmin->getList('user', '  `is_admin`="0" order by user_id desc');
 
		$this->load->view('admin/user/users_list', $data); 
	}



	public function edit($id=null){

		$result = array();



		$result['uid'] =$id;

		$result['customer'] = $this->CommonModelAdmin->getById('user', $id);



	    $this->load->view('admin/user/edit_user',$result);

	}



	public function update()

	{

		if(isset($_POST['submit']))
		{

			$data = $this->input->post();

			$id = $_POST['uid'];

			unset($data['submit']);
			unset($data['uid']);

	        $this->CommonModelAdmin->updateDataCustomer('user', $id, $data); 

	        $this->session->set_flashdata('success', 'Customer updated successfully'); 


	        redirect($_SERVER['HTTP_REFERER']);

		}

	}



	public function delete($id=null){

		$this->CommonModelAdmin->deleteById('user', $id);

		redirect($_SERVER['HTTP_REFERER']);

	}

	

} 

?>