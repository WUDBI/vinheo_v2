<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Slider extends CI_Controller{



	public function __construct(){

		parent::__construct();

		$this->load->model('CommonModelAdmin');

		if(!$this->session->userdata('adminid')) 
		redirect('admin');

	}



	public function index()
	{ 

		$result = array();
		if(isset($_POST['update']) )
		{
			$image1 = $_POST['image1'];
	    	$image2 = $_POST['image2'];
	    	$image3 = $_POST['image3'];


	    	if($image1 == $image2)
	    	{
	    		$this->session->set_flashdata('error', 'Please select different numbers for each selection.');
	    		redirect($_SERVER['HTTP_REFERER']); 
	    	}
	    	if($image2 == $image3)
	    	{
	    		$this->session->set_flashdata('error', 'Please select different numbers for each selection.');
	    		redirect($_SERVER['HTTP_REFERER']); 
	    	}

	    	if($image1 == $image3)
	    	{
	    		$this->session->set_flashdata('error', 'Please select different numbers for each selection.');
	    		redirect($_SERVER['HTTP_REFERER']); 
	    	}


	    	$data_to_update = array(
	    		'image1'  => $image1,
	    		'image2'  => $image2,
	    		'image3'  => $image3,
	    	);

	    	$this->CommonModelAdmin->updateData('slider_number',1,$data_to_update);
	    	if(isset($_FILES['fileToUpload']['name']) and !empty($_FILES['fileToUpload']['name']))
	    	{

		        $config['upload_path']          = 'assets/slider/';   
		        $config['allowed_types']        = 'jpg|jpeg|png';
                
		        $this->load->library('upload', $config);

		        if(!$this->upload->do_upload('fileToUpload')) {               
		            $result['error'] = $this->upload->display_errors(); 
		            $this->session->set_flashdata('error', $result['error']);
		        } else {
		            $result = array('upload_data' => $this->upload->data());
		            
		            $file   = $result['upload_data']['file_name'];

		            $insertd= array( 
		            	'file_name' => $file,
		            );
		            $this->CommonModelAdmin->insertData('slider_images',$insertd);

		            $this->session->set_flashdata('success', 'Slider added successfully.');

		           	redirect($_SERVER['HTTP_REFERER']);                               
		        } 
		    }    
	    }else if (isset($_POST['del']))
	    {
	    	$id_is = $_POST['img_id'];
	    	$this->CommonModelAdmin->deleteById('slider_images',$id_is);
	    	$this->session->set_flashdata('success', 'Slider deleted successfully.');
	    	redirect($_SERVER['HTTP_REFERER']); 
	    }else if(isset($_POST['image1'])){

	    	$image1 = $_POST['image1'];
	    	$image2 = $_POST['image2'];
	    	$image3 = $_POST['image3'];


	    	if($image1 == $image2)
	    	{
	    		$this->session->set_flashdata('error', 'Please select different numbers for each selection.');
	    		redirect($_SERVER['HTTP_REFERER']); 
	    	}
	    	if($image2 == $image3)
	    	{
	    		$this->session->set_flashdata('error', 'Please select different numbers for each selection.');
	    		redirect($_SERVER['HTTP_REFERER']); 
	    	}

	    	if($image1 == $image3)
	    	{
	    		$this->session->set_flashdata('error', 'Please select different numbers for each selection.');
	    		redirect($_SERVER['HTTP_REFERER']); 
	    	}


	    	$data_to_update = array(
	    		'image1'  => $image1,
	    		'image2'  => $image2,
	    		'image3'  => $image3,
	    	);

	    	$this->CommonModelAdmin->updateData('slider_number',1,$data_to_update);
	    }

		$result['images'] = $this->CommonModelAdmin->get_slider_images();
		$result['slider_number'] = $this->CommonModelAdmin->get_slider_names();
		
		 
		 	
		
		$result['settingslider_active'] = true; 

		$this->load->view('admin/slider/slider',$result); 
		
	}

	

} 

?>