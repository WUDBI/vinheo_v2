<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Template extends CI_Controller{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('CommonModelAdmin'); 
		$this->load->model('CommonModel'); 
	}




	
	public function email_templates()
	{

		$result = array();
		
		if(isset($_POST['submit'])){
			$data = $_POST;
			unset($data['submit']);
			unset($data['files']);
			$this->CommonModelAdmin->update_template($data);
			$this->session->set_flashdata('success', 'Email templates updated successfully');
		}

		$result['page_content'] = $this->CommonModelAdmin->getemailtemplate();
		$result['template_active'] = true;
		 
		$this->load->view('admin/template/email_template',$result); 
	} 
	
	 
	 

	
	 
 
	
	 

	




	public function pages_templates()
	{
		  
		$result = array();
		
		if(isset($_POST['submit']))
		{
			$data = $_POST;

			unset($data['submit']);
			unset($data['files']);
			$this->CommonModelAdmin->updatePages($data);
			 
			$this->session->set_flashdata('success', 'Pages templates updated successfully');
			redirect($_SERVER['HTTP_REFERER']);
		}

		$result['page_content'] = $this->CommonModelAdmin->getPagesTemplates();
		$result['template_pages_active'] = true;
		$this->load->view('admin/template/pages_templates',$result);

		 
	}

 
	
} 
?>