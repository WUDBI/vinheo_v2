<?php
  /**
   * @package Contact :  CodeIgniter Multi Language Loader
   *
   * @author Zeeshan
   *
   * @email  zeeshan72awan@gmail.com
   *   
   * Description of Multi Language Loader Hook
   */
 
class MultiLanguageLoader
{
      function initialize() 
      {
          $ci =& get_instance();
          // load language helper
          
          $ci->load->helper('language');

          $siteLang = $ci->session->userdata('site_lang');
          if ($siteLang) {
              // difine all language files
              $ci->lang->load('file',$siteLang);
               
          } else {
              // default language files
              $ci->lang->load('file','english');
               
          }
      }
}


?>