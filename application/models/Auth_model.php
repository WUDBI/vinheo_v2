<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{
	public function register($data)
	{
		$query = $this->db->insert('users', $data);

		if($query)
		{
			return $this->db->insert_id();
		}
	}

	public function login($email, $password)
	{
		return $this->db->get_where('users', array('email' => $email, 'password' => $password ));
	}

	public function verification($token)
	{
		$query = $this->db->get_where('users', array('email_verification_token' => $token, 'status' => '0') )->row();

		if($query){
			$this->db->where('email_verification_token', $token);
			$this->db->update('users', array('status' => 1, 'email_verification_token' => ''));
			return true;
		}else{
			return false;
		}
		
	}

	public function get_user($email, $token)
	{
		$query =  $this->db->get_where('users', array('email' => $email))->row();

		if($query){
			$this->set_reset_token($email, $token);
			return $query;
		}else{
			return false;
		}
	}

	public function get_user_by_token($user_id, $token)
	{
		return $this->db->get_where('users', array('id' => $user_id, 'password_reset_token' => $token))->row();
	}

	public function reset_password($user_id, $password)
	{
		$this->db->where('id', $user_id);
		return $this->db->update('users', array('password' => $password, 'password_reset_token' => ''));
	}

	private function set_reset_token($email, $token)
	{
		$this->db->where('email', $email);
		return $this->db->update('users', array('password_reset_token' => $token));
	}
}