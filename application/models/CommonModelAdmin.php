<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CommonModelAdmin extends CI_Model{

	public function getList($table, $conditions)
	{
		return $this->db->get_where($table, $conditions)->result();
	}



	public function getVideosAll($table,$where=null){

		$this->db->select('video.*,user.first_name as first_name,user.last_name as last_name,user.email as email,user.location as location,user.user_package as user_package')->from('video');

		if($where !=null){

			$this->db->where($where);

		}

		$this->db->join('user',' user.user_id = video.user_id','left');

		$query=$this->db->get();

		if($query->num_rows() > 0)

			return $query->result();

		return false;

	}


	public function getTotalRecords($table, $conditions)
	{
		$this->db->where($conditions);
		return $this->db->count_all_results($table);
	}

	public function getUser($id){
		return $this->db->get_where('users', array('id' => $id))->row();
	}

	public function getById($table,$id){
		return $this->db->query("SELECT * FROM $table WHERE user_id ='$id'")->row();
	}
	
	public function insertData($table,$data){
		return $this->db->insert($table,$data);
	}



	public function getReviews($table)
	{ 
		return $this->db->query("SELECT * FROM $table order by id desc limit 3")->result();
	}
	
	public function deleteById($table,$id){
		$this->db->where('id',$id);
		return $this->db->delete($table);
	}
	public function updateData($table,$id,$data){
		$this->db->where('id',$id);
		return $this->db->update($table,$data);
	}

	public function updateDataCustomer($table,$id,$data){
		$this->db->where('user_id',$id);
		return $this->db->update($table,$data);
	}

	public function reg_user($fname,$lname,$uname,$email,$pass1,$dob_sign,$country_search,$locality_id)
	{
		$query = $this->db->query("SELECT * FROM users WHERE `uname` ='".$uname."' OR  `email` = '".$email."' ")->row();
		
		if(count($query)>= 1){
			return false;
		}else{
			$sql = "INSERT INTO users SET `fname`='".$fname."', `lname`='".$lname."', `uname` = '".$uname."',`email` = '".$email."',`dob` = '".$dob_sign."',`city`='".$locality_id."',`country`='".$country_search."',`pass`='".$pass1."', `profile_pic` ='avatar2.png',`status`=0 ";
			 
			$this->db->query($sql);
			return $this->db->insert_id();

		}
	}

	public function reg_usergmail($fname,$email)
	{ 
		$query = $this->db->query("SELECT * FROM users WHERE `uname` ='".$fname."' OR  `email` = '".$email."' ")->row();
		 

		if(count($query)>= 1){
			return $query->id;
		}else{
			$sql = "INSERT INTO users SET `fname`='".$fname."' , `uname` = '".$fname."',`email` = '".$email."', `status`=0 ";
			 
			$this->db->query($sql);
			return $this->db->insert_id();

		}
	}
	public function login_user($uname,$pass){
		$sql = "SELECT * FROM users WHERE (`uname` = '".$uname."' OR `email` ='".$uname."' ) AND `password` = '".$pass."' AND `status`=1";
		$query = $this->db->query($sql)->row();
		if(count($query) >= 1){
				$udata = array(
				'uid'       => $query->id,
		        'username'  => $query->uname,
		        'email'     => $query->email,
		        'is_host'   => $query->is_host,
		        'logged_in' => TRUE
		         );
         $this->session->set_userdata($udata);
         return true;
		}else{
			return false;
		}
	}
	public function adminlogin($uname,$pass)
	{
		$query = $this->db->query("SELECT * FROM user WHERE  `email` = '".$uname."'   AND `password` = '".$pass."' and is_admin =1 ")->row(); 
		 
		if(!empty($query) ){
				$udata = array(
					'adminid'       => $query->user_id,
			        'adminname'     => $query->first_name,
			        'email'         => $query->email,
			        'logged_in'     => TRUE
			    );
         $this->session->set_userdata($udata);
         return true;
		}else{
			return false;
		}
	}
	public function updatepass($uid,$new_pass,$current_pass)
	{
		$query = $this->db->query("SELECT * FROM user WHERE `user_id` ='".$uid."' AND password='".$current_pass."'  ")->row();
		
		if(count($query)>= 1){
			$this->db->query("UPDATE user SET `password` = '".$new_pass."' WHERE user_id = '".$uid."' ");
			return true;
		}else{
			return false;
		}
	}
	public function verify_user($id){
		$this->db->query("UPDATE user SET `status` = 1 WHERE user_id = '".$id."' ");
	}
	
	public function get_listreviews($id){
		return $query = $this->db->query("SELECT * FROM reviews WHERE list_id='".$id."' ORDER BY id DESC")->result();
	}
	

	public function total_users($user_type){
		return $this->db->query("SELECT COUNT(*) as total FROM users WHERE user_type = '".$user_type."' ")->row();
	}

	
	
	public function getsetting(){ 
		return $query = $this->db->query("SELECT * FROM `setting` WHERE 1 ORDER BY id DESC")->row();
	}

	public function get_slider_images(){ 
		return $query = $this->db->query("SELECT * FROM `slider_images` WHERE 1 ORDER BY id DESC limit 3")->result();
	}

	public function get_slider_names(){ 
		return $query = $this->db->query("SELECT * FROM `slider_number` WHERE 1 ORDER BY id ASC limit 1")->row();
	}


	public function gettemplate(){
		return $query = $this->db->query("SELECT * FROM `email_templates` WHERE 1 ORDER BY id DESC")->row();
	}
	
	
	public function deleteuser($id){
		$this->db->query("DELETE FROM users WHERE id ='".$id."' ");
		return true;
	}

  
	public function update_cms($data,$page_type){
		$this->db->where('page_type', $page_type);
		$this->db->update('cms', $data);
		return true;
	}
	public function getcms($page_type){
		return $this->db->query("SELECT * FROM cms WHERE page_type ='".$page_type."' ")->row();
	}
	
	public function updateadmin($data,$adminid){
		$this->db->where('user_id', $adminid);
        $this->db->update('user', $data);
	}
	public function updateUser($data,$id){
		$this->db->where('id', $id);
        $this->db->update('users', $data);
	}
	public function getemailtemplate(){
		return $this->db->query("SELECT * FROM email_templates WHERE 1")->row();
	}

	

	public function update_template($data){
		$this->db->where('id', 1);
        $this->db->update('email_templates', $data);
	}
	
	public function getPagesTemplates(){
		return $this->db->query("SELECT * FROM pages_templates WHERE 1")->row();
	}

	public function getPagesTemplateSetting(){
		return $this->db->query("SELECT * FROM page_template_setting WHERE 1")->row();
	}


	public function updatePages($data)
	{
		$this->db->where('id', 1);
        $this->db->update('pages_templates', $data);
	}

	public function alluseremails($uid){
		return $this->db->query("SELECT email FROM users WHERE id !=$uid AND is_admin=0 ")->result();
	}
	

	public function fb_login($first_name,$last_name,$email,$profile_pic){
		$query = $this->db->query("SELECT * FROM users WHERE `uname` ='".$last_name."' OR  `email` = '".$email."' ")->row();
		
		if(count($query)>= 1){
			$udata = array(
				'uid'       => $query->id,
		        'username'  => $query->uname,
		        'email'     => $query->email,
		        'logged_in' => TRUE
		         );
         $this->session->set_userdata($udata);
         return true;
		}else{
			$query = $this->db->query("INSERT INTO users SET `fname`='".$first_name."', `lname`='".$last_name."', `uname` = '".$first_name."',`email` = '".$email."',`pass`='".$pass1."', `profile_pic` ='avatar2.png',`status`=0 ");
			$uid = $this->db->insert_id();
			$udata = array(
				'uid'       => $uid,
		        'username'  => $first_name,
		        'email'     => $email,
		        'logged_in' => TRUE
		         );
         $this->session->set_userdata($udata);
          return true;
		}
	}
	public function updatereset($reset_email,$randomletter){
		$this->db->query("UPDATE users SET reset_num='".$randomletter."' WHERE email = '".$reset_email."' ");
	}
	public function reset_password($token,$new_pass,$reset_email){
		$query = $this->db->query("SELECT * FROM users WHERE  `email` = '".$reset_email."' AND reset_num ='".$token."' ")->row();
		
		if(count($query)>= 1){
			$this->db->query("UPDATE users SET password ='".$new_pass."' WHERE `email` ='".$reset_email."'  ");
			return true;
		}else{
			return false;
		}
	}
	
} 
?>