<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class CommonModel extends CI_Model{



	

	public function add($table, $data){

		$this->db->insert($table,$data);

		 

		if($this->db->insert_id() > 0)

			return $this->db->insert_id();

		return false;

	}

	public function getAll($table,$where=null){

		$this->db->select()->from($table);

		if($where !=null){

			$this->db->where($where);

		}

		$query=$this->db->get();

		if($query->num_rows() > 0)

			return $query->result();

		return false;

	}

	public function getById($table,$id=null){

		$query=$this->db->select()->from($table)->where('id',$id)->get();

		if($query->num_rows() > 0)

			return $query->row();

		return false;

	}


	public function getActiveUser($id=null){

		$query=$this->db->select()->from('user')->where('user_id',$id)->get();

		if($query->num_rows() > 0)

			return $query->row();

		return false;

	}


	public function getByIdCustom($table,$where=null){

		$query=$this->db->select()->from($table)->where($where)->get();

		if($query->num_rows() > 0)

			return $query->row();

		return false;

	}

	public function getOne($table,$where=null){

		$this->db->select()->from($table);

		if($where!=null){

		    $this->db->where($where);

		}

		$query=$this->db->get();

		if($query->num_rows() > 0)

			return $query->row();

		return false;

	}

	public function update($table,$where,$data){

		if($this->db->set($data)->where($where)->update($table))

			return true;

		return false;

	}

	public function delete($table,$where=null){

		if($this->db->where($where)->delete($table))

			return true;

		return false;

	}

	public function universal($query=null){

		if($query!=null)

			return $this->db->query($query);

	}

 

	 







	public function user_login($table,$where=null) {



        $this->db->select();



        $this->db->from($table);



        $this->db->where($where);



        $query = $this->db->get();



        return $query->result_array();



    }











    public function getAllVideos($where=null)

    {

		$this->db->select('video.*,user.first_name as first_name,user.last_name as last_name,user.status as user_status,user.profile_pic as profile_pic ')->from('video');



		$this->db->join('user',' video.user_id = user.user_id ','left');



		if($where !=null)

		{

			$this->db->where($where);

		}



		$query = $this->db->get()->result();
 

		return $query;

	}



	

	public function getAllVideosCount($where=null) 
    {

		$this->db->select('COUNT(*) AS count')->from('video');



		$this->db->join('user',' video.user_id = user.user_id ','left');



		if($where !=null)

		{

			$this->db->where($where);

		}



		$query = $this->db->get()->row()->count;



		 

		return $query;

	}



	public function getAllVideoSize($where=null) 
    {

		$this->db->select('sum(video_size) as total_size')->from('video'); 
		$this->db->join('user',' video.user_id = user.user_id ','left'); 

		if($where !=null) 
		{ 
			$this->db->where($where); 
		} 

		$query = $this->db->get()->row();

		if(isset($query->total_size))
		{
			return $query->total_size;
		}else{
			return '0';
		}

		

	}




	public function getAllActivePackages($table,$where=null){

		$this->db->select()->from($table);

		if($where !=null){

			$this->db->where($where);

		}
		$this->db->join('user',' payments.user_id = user.user_id ','left');

		$query=$this->db->get();

		if($query->num_rows() > 0)

			return $query->result();

		return false;

	}



    



	 

}