<?php

// title



// Main Menu

$lang['join'] = 'Unirse';

$lang['Log_in'] = 'Iniciar sesión';

$lang['About'] = 'Acerca de';

$lang['Pricing'] = 'Precios';

$lang['Contact'] = 'Contacto';

$lang['Upload'] = 'Subir';
$lang['Logout'] = 'Cerrar sesión';

$lang['Location'] = 'Ubicación';
$lang['Your_website'] = 'Su sitio web';
$lang['Your_website2'] = 'Agregue su sitio personal, enlaces sociales, etc.';
$lang['Add_link2'] = 'Agregar un nuevo enlace';



//sign up

$lang['Username'] = 'Nombre de usuario';

$lang['Email'] = 'Email';

$lang['Password'] = 'Contraseña';

$lang['Password2'] = 'Confirmar contraseña';

$lang['confirm_account_msg'] = 'Por favor revise su correo electrónico y confirme su cuenta.';

$lang['reset_msg_success'] = 'Por favor revise su correo electrónico y restablezca su contraseña.';

$lang['email_registerd'] = 'Este correo electrónico ya está registrado.';

$lang['email_not_correct'] = 'Por favor, introduzca la dirección de correo electrónico correcta.';

$lang['password_same_msg'] = 'Ambas contraseñas deben ser iguales.';

$lang['user_req_msg'] = 'Se requiere nombre de usuario.';

$lang['email_is_msg'] = 'Correo electronico es requerido.';

$lang['pwd_is_msg'] = 'Se requiere contraseña.';

$lang['forgot_password'] = 'Se te olvidó tu contraseña';

$lang['reset_password'] = 'Restablecer la contraseña';

$lang['Save'] = 'Salvar';

$lang['change_password'] = 'Cambia la contraseña';

$lang['no_record_found'] = 'Error! Ningún record fue encontrado.';

$lang['pwd_change_success'] = 'Contraseña cambiada con éxito.';





$lang['update_success'] = 'Los datos se han actualizado con éxito.';

$lang['update_error']   = 'Error! No se pueden actualizar los datos.';


$lang['vinheo_info']   = 'Vinheo es todo sobre ti y tus videos';

$lang['vinheo_info2']   = 'Creamos cosas que lo ayudan a CREAR COMPARTIR DESCUBRIR videos en línea y en todo el mundo';

$lang['Get_in_Touch']   = 'Ponerse en contacto';

$lang['Send_Message']   = 'Enviar mensaje';

$lang['Send_us_your_query_anytime']   = 'Envíenos su consulta en cualquier momento!';

$lang['Vinheo_is_what_you_need']   = 'Vinheo es lo que necesitas';

$lang['Share_your_videos_with_the_whole_world']   = 'Comparte tus videos con todo el mundo';


$lang['Enhance_your_brand_with_top_quality_videos']   = 'Mejora tu marca con videos de alta calidad';

$lang['msg2']   = 'Un reproductor de video simple pero potente, rápido, sin publicidad, personalizable y muy fácil de insertar';

$lang['Learn_more']   = 'Aprende más';

$lang['Compare_plans']   = 'Compara planes';

$lang['msg3']   = 'Control total de la privacidad.';
$lang['msg4']   = 'Controle exactamente quién ve sus videos, cuándo y dónde con controles de privacidad avanzados.';
$lang['msg5']   = 'Planes con control de privacidad avanzado de';
$lang['msg6']   = 'Trabajar y hacer en equipo.';
$lang['msg7']   = 'Invita a los miembros de tu equipo, organiza las opiniones, comparte archivos de video y hazlo todo más rápido.';
$lang['msg8']   = 'Planes con herramientas de colaboración de';
$lang['msg9']   = 'En cualquier parte pública y crece';
$lang['msg10']   = 'Haga crecer su audiencia y su negocio en todas partes del mundo, independientemente del dispositivo';
$lang['msg11']   = 'Planes profesionales y comerciales de';

$lang['msg12']   = 'Transmisión en vivo ilimitada.';
$lang['msg13']   = 'Transmisión en vivo de alta calidad y participación del público en tiempo real para su próximo gran evento';
$lang['msg14']   = 'Planes con transmisión en vivo desde';

$lang['Forget_password']   = 'Contraseña olvidada';
$lang['Login_with_facebook']   = 'Iniciar sesión con Facebook';


$lang['User_Profile']   = 'Perfil del usuario';
$lang['Settings']   = 'Configuraciones';
$lang['General']   = 'General';
$lang['Work']   = 'Trabajo';
$lang['Roles']   = 'Roles';
$lang['Contact']   = 'Contacto';
$lang['max_note']   = 'Escribe una breve biografía para presentarte. Esto aparecerá en la página principal de su perfil. (160 caracteres como máximo)';
$lang['max_note2']   = 'Cuéntale al mundo más sobre quién eres, qué te gusta hacer y en qué cosas estás interesado en trabajar a continuación. Esto aparecerá en la página Acerca de su perfil.';
$lang['Project_types']   = 'Tipos de proyectos';
$lang['Project_types2']   = 'Comparta el tipo de proyectos en los que ha trabajado.';
$lang['Project_types3']   = 'Cómo te describes';
$lang['Project_types4']   = 'Profesional independiente';
$lang['Project_types6']   = 'Agencia / estudio';
$lang['Project_types5']   = 'Marca / negocio';
$lang['Project_types8']   = 'Arrastre y suelte en cualquier lugar para cargar.';
$lang['Thumbnail']   = 'Miniatura';
$lang['Title']   = 'Título';
$lang['Description']   = 'Descripción';
$lang['Privacy']   = 'Intimidad';
$lang['Language']   = 'Idioma';
$lang['Privacy2']   = 'Etiquetas (separadas por comas, por favor)';
$lang['Privacy4']   = 'Administra tus videos';
$lang['Dashboard']   = 'Tablero';
$lang['Manage_Videos']   = 'Administrar videos';
$lang['Account']   = 'Cuenta';


?>