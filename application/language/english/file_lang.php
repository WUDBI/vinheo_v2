<?php





// Main Menu

$lang['join'] = 'Join';

$lang['Log_in'] = 'Login';

$lang['About'] = 'About';

$lang['Pricing'] = 'Pricing';

$lang['Contact'] = 'Contact';

$lang['Upload'] = 'Upload';

$lang['Logout'] = 'Logout';

$lang['Location'] = 'Location';
$lang['Your_website'] = 'Your website';
$lang['Your_website2'] = 'Add your personal site, social links, etc.';
$lang['Add_link2'] = 'Add a new link';



//sign up

$lang['Username'] = 'Username';

$lang['Email'] = 'Email';

$lang['Password'] = 'Password';

$lang['Password2'] = 'Confirm Password';

$lang['confirm_account_msg'] = 'Please check your email and confirm your account.';

$lang['reset_msg_success'] = 'Please check your email and reset your password.';

$lang['email_registerd'] = 'This email is already registered.';

$lang['email_not_correct'] = 'Please enter correct email address.';

$lang['password_same_msg'] = 'Both passwords should be same.';

$lang['user_req_msg'] = 'Username is required.';

$lang['email_is_msg'] = 'Email is required.';

$lang['pwd_is_msg'] = 'Password is required.';

$lang['forgot_password'] = 'Forgot password';

$lang['reset_password'] = 'Reset Password';

$lang['Save'] = 'Save';

$lang['change_password'] = 'Change Password';

$lang['no_record_found'] = 'Error! No record found.';

$lang['pwd_change_success'] = 'Password changed successfully.';





$lang['update_success'] = 'Data has been updated successfully.';

$lang['update_error']   = 'Error! Unable to updated data.';


$lang['vinheo_info']   = 'Vinheo  is all about you and your videos';
$lang['vinheo_info2']   = 'We build things that help you CREATESHARESELLDISCOVER videos online and worldwide';
$lang['Get_in_Touch']   = 'Get in Touch';
$lang['Send_Message']   = 'Send Message';
$lang['Send_us_your_query_anytime']   = 'Send us your query anytime!';
$lang['Vinheo_is_what_you_need']   = 'Vinheo is what you need';
$lang['Share_your_videos_with_the_whole_world']   = 'Share your videos with the whole world';


$lang['Enhance_your_brand_with_top_quality_videos']   = 'Enhance your brand with top quality videos';
$lang['msg2']   = 'A simple but powerful video player, fast, without advertising, customizable and so easy to insert';
$lang['Learn_more']   = 'Learn more';
$lang['Compare_plans']   = 'Compare plans';

$lang['msg3']   = 'Total control of privacy.';
$lang['msg4']   = 'Control exactly who sees your videos, when, and where with advanced privacy controls.';
$lang['msg5']   = 'Plans with advanced privacy control from';
$lang['msg6']   = 'Work and do as a team.';
$lang['msg7']   = 'Invite your team members, organize the opinions, share video files and do it all faster.';
$lang['msg8']   = 'Plans with collaboration tools from';
$lang['msg9']   = 'In any public part and it grows';
$lang['msg10']   = 'Grow your audience and your business in all parts of the world regardless of the device';
$lang['msg11']   = 'Pro and Business plans from';

$lang['msg12']   = 'Unlimited live streaming.';
$lang['msg13']   = 'High quality live streaming and real-time audience engagement for your next big event';
$lang['msg14']   = 'Plans with live streaming from ';
$lang['Forget_password']   = 'Forget password';
$lang['Login_with_facebook']   = 'Login with facebook';


$lang['User_Profile']   = 'User Profile';
$lang['Settings']   = 'Settings';
$lang['General']   = 'General';
$lang['Work']   = 'Work';
$lang['Roles']   = 'Roles';
$lang['Contact']   = 'Contact';
$lang['max_note']   = 'Write a short bio to introduce yourself. This will appear on the front page of your profile. (160 characters max)';
$lang['max_note2']   = 'Tell the world more about who you are, what you like to make, and what things you’re interested in working on next. This will appear on the About page of your profile.';
$lang['Project_types']   = 'Project types';
$lang['Project_types2']   = 'Share the type of projects you have worked on.';
$lang['Project_types3']   = 'How would you describe yourself';
$lang['Project_types4']   = 'Freelance professional';
$lang['Project_types6']   = 'Agency/Studio';
$lang['Project_types5']   = 'Brand/Business';
$lang['Project_types8']   = 'Drag and drop anywhere to upload.';
$lang['Thumbnail']   = 'Thumbnail';
$lang['Title']   = 'Title';
$lang['Description']   = 'Description';
$lang['Privacy']   = 'Privacy';
$lang['Language']   = 'Language';
$lang['Privacy2']   = 'Tags(Separated by commas, please!)';
$lang['Privacy4']   = 'Mange your videos';
$lang['Dashboard']   = 'Dashboard';
$lang['Manage_Videos']   = 'Manage Videos';
$lang['Account']   = 'Account';









// Main Menu

$lang['text_menu_home_header'] = 'Home';

$lang['text_menu_demo_header'] = 'Live Demo'; 

$lang['text_menu_tutorials_header'] = 'Tutorials';

$lang['text_menu_contact_header'] = 'Contact';

?>