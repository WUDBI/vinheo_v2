<?php echo $this->load->view('admin/layout/header','',true); ?>
         
        
<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>
<style>
    .form-horizontal b{
        margin-bottom: 14px;
        display: block;
        color: #f3103c;
    }
    .btn-info{
        width: 20%;
    }
    .style_mg{
        margin-top: 20px !important;
        margin-bottom: 85px !important;
    }
 

    .form-horizontal b{

        margin-bottom: 14px;

        display: block;

        color: #f3103c;

    }

    .btn-info{

        width: 20%;

    }

    .top-right 

    {

      position: absolute;

      top: 8px;

      right: 27px;

       

    }



    .btn_custom_de{

        border: none !important;

        background: none !important;

    }



    .fa-trash{

            font-size: 16px !important; 

            color: #FF5F29;

    }



    .img_stl{

        width: 100%; 

        height: 170px;

        margin-right: 3%;

    }

  .container {
    padding: 50px 10%;
  }

  .box {
    position: relative;
    background: #ffffff;
    width: 100%;
  }

  .box-header {
    color: #444;
    display: block;
    padding: 10px;
    position: relative;
    border-bottom: 1px solid #f4f4f4;
    margin-bottom: 10px;
  }

  .box-tools {
    position: absolute;
    right: 10px;
    top: 5px;
  }

  .dropzone-wrapper {
    border: 2px dashed #91b0b3;
    color: #92b0b3;
    position: relative;
    height: 150px;
  }

  .dropzone-desc {
    position: absolute;
    margin: 0 auto;
    left: 0;
    right: 0;
    text-align: center;
    width: 40%;
    top: 50px;
    font-size: 16px;
  }

  .dropzone,
  .dropzone:focus {
    position: absolute;
    outline: none !important;
    width: 100%;
    height: 150px;
    cursor: pointer;
    opacity: 0;
  }

  .dropzone-wrapper:hover,
  .dropzone-wrapper.dragover {
    background: #ecf0f5;
  }

  .preview-zone {
    text-align: center;
  }

  .preview-zone .box {
    box-shadow: none;
    border-radius: 0;
    margin-bottom: 0;
  }


</style> 
<div id="page-wrapper"  >  
    <div class="row  border-bottom white-bg dashboard-header">
        <div class="panel-body widget-shadow ">
            <div class="row"> 
            <div class="col-lg-12 style_mg"  >
                <?php if(!empty($this->session->flashdata('success'))){ ?>
        	        <div class="alert alert-success alert-dismissible">
        	            <button type="button" class="close" data-dismiss="alert">&times;</button>
        	            <span><i class="fa fa-check-circle"></i></span> <?= $this->session->flashdata('success'); ?>
        	        </div>
                <?php } ?>

                <?php if(!empty($this->session->flashdata('error'))){ ?>
        	        <div class="alert alert-warning alert-dismissible">
        	            <button type="button" class="close" data-dismiss="alert">&times;</button>
        	             <?= $this->session->flashdata('error'); ?>
        	        </div>
                <?php } ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="padding: 14px 15px 7px;">
                        <h4>Setting</h4>
                         
                    </div>

                    <hr>
                    <div class="ibox-content" style="padding: 14px 15px 7px;">
                        <div  class="row"> 

                            <?php 
                                if (isset($images) and !empty($images)) 
                                { 
                                    foreach ($images as $image): 
                                        ?>

                                        <div class="col-lg-4">

                                            <img class="img_stl" src="<?php echo base_url(); ?>assets/slider/<?php echo $image->file_name ?> ">

                                            
                                            <div class="top-right">

                                                <form action="" method="post">

                                                    <input type="hidden" name="img_id" value="<?php echo $image->id ?>">

                                                     

                                                    <button onclick="return confirm('Are you sure you want to delete this item?');" name="del" class="btn_custom_de" type="submit"><i class="fa fa-trash"  ></i></button>

                                                </form> 

                                            </div>

                                        </div> 

                                        <?php 
                                    endforeach;
                                }
                            ?> 

                        </div> 
                        <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>admin/slider" enctype="multipart/form-data"> 
                            <div  class="row"> 
                              <div class="col-lg-4 " style="margin-top: 15px;margin-left: 0px">
                                <select name="image1" class="form-control">
                                    <option <?php if ($slider_number->image1 == 1): ?> selected <?php endif ?> value="1">1</option>
                                    <option <?php if ($slider_number->image1 == 2): ?> selected <?php endif ?> value="2">2</option>
                                    <option <?php if ($slider_number->image1 == 3): ?> selected <?php endif ?> value="3">3</option>
                                </select>
                              </div>
                              <div class="col-lg-4  ">
                                <select name="image2" class="form-control"  style="margin-top: 15px; ">
                                    <option <?php if ($slider_number->image2 == 1): ?> selected <?php endif ?> value="1">1</option>
                                    <option <?php if ($slider_number->image2 == 2): ?> selected <?php endif ?> value="2">2</option>
                                    <option <?php if ($slider_number->image2 == 3): ?> selected <?php endif ?> value="3">3</option>
                                </select>
                              </div>

                              <div class="col-lg-4  "> 
                                <select name="image3" class="form-control"  style="margin-top: 15px; ">
                                    <option <?php if ($slider_number->image3 == 1): ?> selected <?php endif ?> value="1">1</option>
                                    <option <?php if ($slider_number->image3 == 2): ?> selected <?php endif ?> value="2">2</option>
                                    <option <?php if ($slider_number->image3 == 3): ?> selected <?php endif ?> value="3">3</option>
                                </select>
                              </div>
                            </div>

                        

                            <br> 

                            <!-- <input type="file" name="fileToUpload" id="fileToUpload">  -->
                            <div  style="width: 100%">
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="control-label">Upload File</label>
                                    <div class="preview-zone hidden">
                                      <div class="box box-solid">
                                        <div class="box-header with-border">
                                          <div><b>Preview</b></div>
                                          <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-danger btn-xs remove-preview">
                                              <i class="fa fa-times"></i> Reset This Form
                                            </button>
                                          </div>
                                        </div>
                                        <div class="box-body"></div>
                                      </div>
                                    </div>
                                    <div class="dropzone-wrapper">
                                      <div class="dropzone-desc">
                                        <i class="glyphicon glyphicon-download-alt"></i>
                                        <p>Choose an image file or drag it here.</p>
                                      </div>
                                      <input type="file" name="fileToUpload" class="dropzone">
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-12">
                                  <button type="submit" name="update"  class="btn btn-sm btn-info">Upload</button>
                                </div>
                              </div>
                            </div>


                            <br> 

                          <!--   <div class="form-group"> 

                                <div class=" col-lg-8">

                                    <button name="update" class="btn btn-sm btn-info" type="submit">Update</button>

                                </div>

                            </div> -->

                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</div>

<?php  echo $this->load->view('admin/layout/footer','',true); ?>
 
<script type="text/javascript">
function readFile(input) 
{
  if (input.files && input.files[0])    
  {
      if (!input.files[0].name.match(/.(jpg|jpeg|png)$/i))
      {
          alert('not an image');
          exit(); 
      }
     
      var reader = new FileReader();

      reader.onload = function(e) {
        var htmlPreview =
          '<img width="200" src="' + e.target.result + '" />' +
          '<p>' + input.files[0].name + '</p>';
        var wrapperZone = $(input).parent();
        var previewZone = $(input).parent().parent().find('.preview-zone');
        var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');

        wrapperZone.removeClass('dragover');
        previewZone.removeClass('hidden');
        boxZone.empty();
        boxZone.append(htmlPreview);
      };

      reader.readAsDataURL(input.files[0]);
  }
}

function reset(e) {
  e.wrap('<form>').closest('form').get(0).reset();
  e.unwrap();
}

$(".dropzone").change(function() {
  readFile(this);
});

$('.dropzone-wrapper').on('dragover', function(e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).addClass('dragover');
});

$('.dropzone-wrapper').on('dragleave', function(e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).removeClass('dragover');
});

$('.remove-preview').on('click', function() {
  var boxZone = $(this).parents('.preview-zone').find('.box-body');
  var previewZone = $(this).parents('.preview-zone');
  var dropzone = $(this).parents('.form-group').find('.dropzone');
  boxZone.empty();
  previewZone.addClass('hidden');
  reset(dropzone);
});

 </script>