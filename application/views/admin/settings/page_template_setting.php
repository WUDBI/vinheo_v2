<?php echo $this->load->view('admin/layout/header','',true); ?>
         
        
<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>
<style>
    .form-horizontal b{
        margin-bottom: 14px;
        display: block;
        color: #f3103c;
    }
    .btn-info{
        width: 20%;
    }
 .box {
    position: relative;
    background: #ffffff;
    width: 100%;
  }

  .box-header {
    color: #444;
    display: block;
    padding: 10px;
    position: relative;
    border-bottom: 1px solid #f4f4f4;
    margin-bottom: 10px;
  }

  .box-tools {
    position: absolute;
    right: 10px;
    top: 5px;
  }
 .dropzone-wrapper {
    border: 2px dashed #91b0b3;
    color: #92b0b3;
    position: relative;
    height: 150px;
  }

  .dropzone-desc {
    position: absolute;
    margin: 0 auto;
    left: 0;
    right: 0;
    text-align: center;
    width: 40%;
    top: 50px;
    font-size: 16px;
  }

  .dropzone,
  .dropzone:focus {
    position: absolute;
    outline: none !important;
    width: 100%;
    height: 150px;
    cursor: pointer;
    opacity: 0;
  }

  .dropzone-wrapper:hover,
  .dropzone-wrapper.dragover {
    background: #ecf0f5;
  }

  .preview-zone {
    text-align: center;
  }

  .preview-zone .box {
    box-shadow: none;
    border-radius: 0;
    margin-bottom: 0;
  }

     
</style> 
<div id="page-wrapper"  >
    <div class="row  border-bottom white-bg dashboard-header">
        <div class="panel-body widget-shadow ">
            <div class="row">  
                <div class="col-lg-12 style_mg"  >
                <?php if(!empty($this->session->flashdata('success'))){ ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <span><i class="fa fa-check-circle"></i></span> <?= $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>

                <?php if(!empty($this->session->flashdata('error'))){ ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <span><i class="fa fa-check-circle"></i></span> <?= $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title tables" style="padding: 14px 15px 7px;">
                        <h4>Package Template Setting</h4>
                        
                    </div>

                    <hr>
                    <div class="ibox-content" style="padding: 14px 15px 7px;">
                        <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>admin/settings/pages_template_setting" enctype="multipart/form-data">
                            <b>Main Heading</b>
                            <div class="form-group"> 

                                <div class="col-lg-12"><input name="page_template_heading" type="text" value="<?php echo $page_template->page_template_heading; ?>" class="form-control"> 
                                </div>
                            </div> 
                           
                             
                            <hr> 
                            <b>Heading Content</b>
                            <div class="form-group"> 

                                <div class="col-lg-12"><input name="page_template_content" type="text" value="<?php echo $page_template->page_template_content; ?>" class="form-control"> 
                                </div>
                            </div>
                            <hr> 
                             
          
                            <div class="container" style="width: 100%">
                              <div class="row">
                                <div class="col-md-4">
                                    <img src="<?php echo base_url().'assets/template_setting/'.$page_template->page_template_image; ?>" style='width:100%;height: auto;object-fit: cover;'>
                                </div>
                                <div class="col-md-12">

                                
                                  <div class="form-group">
                                    <label class="control-label">Upload Image</label>
                                    <div class="preview-zone hidden">
                                      <div class="box box-solid">
                                        <div class="box-header with-border">
                                          <div><b>Preview</b></div>
                                          <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-danger btn-xs remove-preview">
                                              <i class="fa fa-times"></i> Reset This Form
                                            </button>
                                          </div>
                                        </div>
                                        <div class="box-body"></div>
                                      </div>
                                    </div>
                                    <div class="dropzone-wrapper">
                                      <div class="dropzone-desc">
                                        <i class="glyphicon glyphicon-download-alt"></i>
                                        <p>Choose an image file or drag it here.</p>
                                      </div>
                                      <input type="file" name="fileToUpload" class="dropzone">
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-12">
                                  <button type="submit" name="update"  class="btn btn-sm btn-info">Upload</button>
                                </div>
                              </div>
                            </div>  
                        </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php  echo $this->load->view('admin/layout/footer','',true); ?>
 <script type="text/javascript">
function readFile(input) 
{
  if (input.files && input.files[0])    
  {
      if (!input.files[0].name.match(/.(jpg|jpeg|png)$/i))
      {
          alert('not an image');
          exit(); 
      }
     
      var reader = new FileReader();

      reader.onload = function(e) {
        var htmlPreview =
          '<img width="200" src="' + e.target.result + '" />' +
          '<p>' + input.files[0].name + '</p>';
        var wrapperZone = $(input).parent();
        var previewZone = $(input).parent().parent().find('.preview-zone');
        var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');

        wrapperZone.removeClass('dragover');
        previewZone.removeClass('hidden');
        boxZone.empty();
        boxZone.append(htmlPreview);
      };

      reader.readAsDataURL(input.files[0]);
  }
}

function reset(e) {
  e.wrap('<form>').closest('form').get(0).reset();
  e.unwrap();
}

$(".dropzone").change(function() {
  readFile(this);
});

$('.dropzone-wrapper').on('dragover', function(e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).addClass('dragover');
});

$('.dropzone-wrapper').on('dragleave', function(e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).removeClass('dragover');
});

$('.remove-preview').on('click', function() {
  var boxZone = $(this).parents('.preview-zone').find('.box-body');
  var previewZone = $(this).parents('.preview-zone');
  var dropzone = $(this).parents('.form-group').find('.dropzone');
  boxZone.empty();
  previewZone.addClass('hidden');
  reset(dropzone);
});

 </script>