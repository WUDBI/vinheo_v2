<?php echo $this->load->view('admin/layout/header','',true); ?>
         
        
<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>

<style type="text/css">
    form .form-group{
        padding: 23px 0px;
    } 

    form b{
        color: #f3103c;
    }
</style>
<div id="page-wrapper"  >
    <div class="row  border-bottom white-bg dashboard-header">
        <div class="panel-body widget-shadow ">
            <div class="row">  
                <div class="col-lg-12 style_mg"  >
                    <?php if(!empty($this->session->flashdata('success'))){ ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <span><i class="fa fa-check-circle"></i></span> <?= $this->session->flashdata('success'); ?>
                    </div>
                    <?php } ?>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title tables" style="padding: 14px 15px 7px;">
                            <h4>Prices Setting</h4> 
                        </div>
                        <hr>
                        <div class="ibox-content" style="padding: 14px 15px 7px;">
                            <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>admin/settings/prices_setting" enctype="multipart/form-data">
                                <b>Enhance your brand with top quality videos.</b>
                                <div class="form-group"><label class="col-lg-1 control-label">Price</label>

                                    <div class="col-lg-11"><input name="quality_videos" type="text" value="<?php echo isset($setting->quality_videos) ? $setting->quality_videos : ''; ?>" class="form-control"> 
                                    </div>
                                </div> 
                               
                                <hr> 
                                <b  >Total control of privacy.</b>
                                <div class="form-group"  ><label class="col-lg-1 control-label">Price</label>

                                    <div class="col-lg-11"><input name="privacy" type="text" value="<?php echo isset($setting->privacy) ? $setting->privacy : ''; ?>" class="form-control"> 
                                    </div>
                                </div>
                               
                                <hr> 
                                <b>Work and do as a team</b>
                                <div class="form-group"><label class="col-lg-1 control-label">Price</label>

                                    <div class="col-lg-11"><input name="team" type="text" value="<?php echo isset($setting->team) ? $setting->team : ''; ?>" class="form-control"> 
                                    </div>
                                </div>
                                <hr> 
                                 

                                <hr>   
                                <b>In any public part and it grows.</b>

                                <div class="form-group">
                                    <label class="col-lg-1 control-label">Price</label>

                                    <div class="col-lg-11"><input name="public_grow" type="number" value="<?php echo isset($setting->public_grow) ? $setting->public_grow : ''; ?>" class="form-control"> 
                                    </div>
                                </div>


                                <hr>   
                                <b>Unlimited live streaming.</b>

                                <div class="form-group">
                                    <label class="col-lg-1 control-label">Price</label>

                                    <div class="col-lg-11"><input name="streaming" type="number" value="<?php echo isset($setting->streaming) ? $setting->streaming : ''; ?>" class="form-control"> 
                                    </div>
                                </div>


                                

                                <hr>   
                                <b>Plus.</b>

                                <div class="form-group">
                                    <label class="col-lg-1 control-label">Price</label>

                                    <div class="col-lg-11"><input name="plus" type="number" value="<?php echo isset($setting->plus) ? $setting->plus : ''; ?>" class="form-control"> 
                                    </div>
                                </div>

                                <hr>   
                                <b>Pro.</b>

                                <div class="form-group">
                                    <label class="col-lg-1 control-label">Price</label>

                                    <div class="col-lg-11"><input name="pro" type="number" value="<?php echo isset($setting->pro) ? $setting->pro : ''; ?>" class="form-control"> 
                                    </div>
                                </div>


                                <hr>   
                                <b>Business.</b>

                                <div class="form-group">
                                    <label class="col-lg-1 control-label">Price</label>

                                    <div class="col-lg-11"><input name="business" type="number" value="<?php echo isset($setting->business) ? $setting->business : ''; ?>" class="form-control"> 
                                    </div>
                                </div>


                                <hr>   
                                <b>Premium.</b>

                                <div class="form-group">
                                    <label class="col-lg-1 control-label">Price</label>

                                    <div class="col-lg-11"><input name="premium" type="number" value="<?php echo isset($setting->premium) ? $setting->premium : ''; ?>" class="form-control"> 
                                    </div>
                                </div>

                                 

                               
                                
                                <div class="form-group">
                                    <label class="col-lg-1 control-label"></label>
                                    <div class=" col-lg-8">
                                        <button name="submit" class="btn btn-lg btn-info" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php  echo $this->load->view('admin/layout/footer','',true); ?>
 