<?php echo $this->load->view('admin/layout/header','',true); ?>

         

        

<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>



<style type="text/css">

    form .form-group{

        padding: 23px 0px;

    } 



    form b{

        color: #f3103c;

    }

</style>

<div id="page-wrapper"  >

    <div class="row  border-bottom white-bg dashboard-header">

        <div class="panel-body widget-shadow ">

            <div class="row">  

                <div class="col-lg-12 style_mg"  >

                    <?php if(!empty($this->session->flashdata('success'))){ ?>

                    <div class="alert alert-success alert-dismissible">

                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                        <span><i class="fa fa-check-circle"></i></span> <?= $this->session->flashdata('success'); ?>

                    </div>

                    <?php } ?>

                    <div class="ibox float-e-margins">

                        <div class="ibox-title tables" style="padding: 14px 15px 7px;">

                            <h4>About Us Setting</h4> 

                        </div>

                        <hr>

                        <div class="ibox-content" style="padding: 14px 15px 7px;">

                            <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>admin/settings/about_us" enctype="multipart/form-data">


                                <div class="row">
                                    <div class="col-lg-6 padding_0_r_add">
                                        <b>Banner Heading</b>

                                        <div class="form-group"> 

                                            <div class="col-lg-12"><input name="banner_heading" type="text" value="<?php echo isset($about_us_data->banner_heading) ? $about_us_data->banner_heading : ''; ?>" class="form-control"> 

                                            </div>

                                        </div> 
                                    </div>


                                    <div class="col-lg-6 padding_0">
                                        <b>Banner Detail</b>

                                        <div class="form-group"> 

                                            <div class="col-lg-12"><input name="banner_detail" type="text" value="<?php echo isset($about_us_data->banner_detail) ? $about_us_data->banner_detail : ''; ?>" class="form-control"> 

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                


                                <hr>
                                 
 
                                <b>Heading</b>

                                <div class="form-group"> 

                                    <div class="col-lg-12"><input name="about_us_heading" type="text" value="<?php echo isset($about_us_data->about_us_heading) ? $about_us_data->about_us_heading : ''; ?>" class="form-control"> 

                                    </div>

                                </div> 

                               
 
                                <b  >Detail</b>

                                <div class="form-group"  > 

                                    <div class="col-lg-12"> 

                                        <textarea class="summernote" name="about_us_detail"><?php echo isset($about_us_data->about_us_detail) ? $about_us_data->about_us_detail : ''; ?></textarea>

                                     

                                    </div>

                                </div>

                                 

                                <div class="form-group"> 

                                    <div class=" col-lg-12">

                                        <button name="update" class="btn btn-lg btn-info" type="submit">Save</button>

                                    </div>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>



<?php  echo $this->load->view('admin/layout/footer','',true); ?>

 