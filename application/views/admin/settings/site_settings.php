<?php echo $this->load->view('admin/layout/header','',true); ?>
         
        
<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>
<div id="page-wrapper"  >
    <div class="row  border-bottom white-bg dashboard-header">
        <div class="panel-body widget-shadow ">
            <div class="row">  
                <div class="col-lg-12 style_mg"  >
                    <?php if(!empty($this->session->flashdata('success'))){ ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <span><i class="fa fa-check-circle"></i></span> <?= $this->session->flashdata('success'); ?>
                    </div>
                    <?php } ?>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title tables" style="padding: 14px 15px 7px;">
                            <h4>Setting</h4>
                             
                        </div>
                        <hr>
                        <div class="ibox-content" style="padding: 14px 15px 7px;">
                            <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>Admin/setting" enctype="multipart/form-data">
                                <b>Company Information</b>
                                <div class="form-group"><label class="col-lg-3 control-label">Company Name</label>

                                    <div class="col-lg-8"><input name="company_name" type="text" value="<?php echo isset($setting->company_name) ? $setting->company_name : ''; ?>" class="form-control"> 
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-lg-3 control-label">Company Email</label>

                                    <div class="col-lg-8"><input name="company_email" type="text" value="<?php echo isset($setting->company_email) ? $setting->company_email : ''; ?>" class="form-control"> 
                                    </div>
                                </div> 
                               
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Company Phone</label>

                                    <div class="col-lg-8"><input name="help_phone" type="text" value="<?php echo isset($setting->help_phone) ? $setting->help_phone : ''; ?>" class="form-control"> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Company Address</label>

                                    <div class="col-lg-8"><textarea name="company_address"  class="form-control"><?php echo isset($setting->company_address) ? $setting->company_address : ''; ?></textarea>
                                    </div>
                                </div>

                               
                                <hr> 
                                <b  >Stripe API</b>
                                <div class="form-group"  ><label class="col-lg-3 control-label">Secret key</label>

                                    <div class="col-lg-8"><input name="stripe_secret_key" type="text" value="<?php echo isset($setting->stripe_secret_key) ? $setting->stripe_secret_key : ''; ?>" class="form-control"> 
                                    </div>
                                </div>
                                <div class="form-group" ><label class="col-lg-3 control-label">Publishable key</label>

                                    <div class="col-lg-8"><input name="stripe_publish_key" type="text" value="<?php echo isset($setting->stripe_publish_key) ? $setting->stripe_publish_key : ''; ?>" class="form-control"> 
                                    </div>
                                </div>
                                <hr> 
                                <b>Paypal API</b>
                                <div class="form-group"><label class="col-lg-3 control-label">Business Account</label>

                                    <div class="col-lg-8"><input name="paypal_business" type="text" value="<?php echo isset($setting->paypal_business) ? $setting->paypal_business : ''; ?>" class="form-control"> 
                                    </div>
                                </div>
                                <hr> 
                                
                                  
                                <b>SMTP setting</b>
                                <div class="form-group"><label class="col-lg-3 control-label">Host Name</label>

                                    <div class="col-lg-8"><input name="host_name" type="text" value="<?php echo isset($setting->host_name) ? $setting->host_name : ''; ?>" class="form-control"> 
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-lg-3 control-label">User</label>

                                    <div class="col-lg-8"><input name="host_user" type="text" value="<?php echo isset($setting->host_user) ? $setting->host_user : ''; ?>" class="form-control"> 
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-lg-3 control-label">Password</label>

                                    <div class="col-lg-8"><input name="host_password" type="password" value="<?php echo isset($setting->host_password) ? $setting->host_password : ''; ?>" class="form-control"> 
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-lg-3 control-label">Port</label>

                                    <div class="col-lg-8"><input name="host_port" type="text" value="<?php echo isset($setting->host_port) ? $setting->host_port : ''; ?>" class="form-control"> 
                                    </div>
                                </div> 

                                <!-- 
                                <hr>   
                                <b>Social Icons links</b>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Facebook</label>

                                    <div class="col-lg-8"><input name="social_fb" type="text" value="<?php echo isset($setting->social_fb) ? $setting->social_fb : ''; ?>" class="form-control"> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Instagram</label>

                                    <div class="col-lg-8"><input name="social_insta" type="text" value="<?php echo isset($setting->social_insta) ? $setting->social_insta : ''; ?>" class="form-control"> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Twitter</label>

                                    <div class="col-lg-8"><input name="social_twitter" type="text"   value="<?php echo isset($setting->social_twitter) ? $setting->social_twitter : ''; ?>" class="form-control"> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Skype</label>

                                    <div class="col-lg-8"><input name="social_gplus" type="text" value="<?php echo isset($setting->social_gplus) ? $setting->social_gplus : ''; ?>" class="form-control"> 
                                    </div>
                                </div> 
                                 -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class=" col-lg-8">
                                        <button name="submit" class="btn btn-sm btn-info" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php  echo $this->load->view('admin/layout/footer','',true); ?>
 