<?php $this->load->view('admin/include/header') ; ?>
 
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />


<div id="page-wrapper" class="gray-bg dashbard-1">
       <?php $this->load->view('admin/include/sidebar') ; ?>

     <div class="col-lg-7">
        <?php if(!empty($this->session->flashdata('success'))){ ?>
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <span><i class="fa fa-check-circle"></i></span> <?= $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>

        <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>admin/update_package"  >
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="padding:13px 23px;">
                    <h5>Active Package Information</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="padding:13px 23px;">
                    
                    <input type="hidden" name="uid" value="<?php echo $active_package->id; ?>">

                                   
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Start Date</label>
                        <div class="col-lg-10">
                            <input name="start_date" value="<?php echo date("Y-m-d", strtotime($active_package->start_date)); ?>" type="text"  class="form-control datepicker" > 
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">End Date</label>
                        <div class="col-lg-10">
                            <input name="end_date" value="<?php echo date("Y-m-d", strtotime($active_package->end_date)); ?>" type="text"  class="form-control datepicker" > 
                        </div>
                    </div> 
                  
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button name="submit" class="btn btn-sm btn-info" type="submit">Update</button>
                        </div>
                    </div>

                </div>
            </div> 
        </form>
    </div>

<?php $this->load->view('admin/include/footer') ; ?>

 

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
    $(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: "yyyy-mm-dd"
  }) 
});
</script>