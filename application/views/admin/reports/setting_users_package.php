<?php echo $this->load->view('admin/layout/header','',true); ?>
         
        
<?php echo $this->load->view('admin/layout/sticky_header','',true); 
$CI = &get_instance();
$CI->load->model('CommonModel'); 

 ?>
<div id="page-wrapper"  > 
     <div class="row  border-bottom white-bg dashboard-header">
        <div class="panel-body widget-shadow ">
            <div class="row"> 
            <div class="col-lg-12 "  >
                <div class="ibox float-e-margins">
                    <div class="ibox-title tables">
                        <h4> Active Packages</h4>
                         
                    </div>
                    <hr>
                    <div class="ibox-content   ">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover data-table" >
                            <thead>
                            <tr>
                                <th>Sr#</th>
                                <th>Name</th>
                                <th>Package</th>
                                <th>Amount</th>
                                <th>Package Duration</th>
                                <th>Payment Method</th> 
                                <th>Payment ID</th> 
                                <th>Status</th>    
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            	<?php
                            	$sr=1;
                                if (isset($active_package)) 
                                {
                                   
                                
                                    foreach($active_package as $customer)
                                    { 
                                        $active_record = $CI->CommonModel->getAllActivePackages('payments',' payments.user_id= "'.$customer->user_id.'"   order by id desc  ');

                                        $active_record = $active_record[0];
                                         
                                        ?>
                                        <tr class="gradeX" style="text-align:center;">

                                        	<td class="text-left"><?php echo $sr; ?></td>

                                            <td class="text-left"><?php echo $customer->first_name.' '.$customer->last_name; ?></td>

                                            <td class="text-left"><?php echo ucfirst($active_record->pkg_name); ?></td>

                                            <td class="text-left"><?php echo $active_record->amount; ?></td>

                                            <td class="text-left"><?php echo  date("d-m-Y", strtotime($active_record->start_date)); ?> - <?php echo date("d-m-Y", strtotime($active_record->end_date)); ?></td>

                                            <td class="text-left"><?php echo ucfirst($active_record->method); ?></td>
         

                                            <td class="text-left">
                                                <?php 
                                                if (empty($active_record->paymentID)):  
                                                    echo "Free Trial ";
                                                else:
                                                    echo $active_record->paymentID;
                                                endif;
                                                ?>
                                                    
                                            </td>

                                            <td class="text-left"><?php echo ucfirst($active_record->status); ?></td>
         
                                            
                                            <td class="text-left">

                                                <a href="<?php echo base_url(); ?>admin/reports/edit_active_package/<?php echo $active_record->id; ?>" class="btn btn-info"><i class="fa fa-pencil"></i></a>

                                                

                                            </td>
                                        </tr>
                                        <?php 

                                        $sr++;
                                    }

                                }
                                ?>
                            </tbody>
                           
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<?php  echo $this->load->view('admin/layout/footer','',true); ?>
 