<?php echo $this->load->view('admin/layout/header','',true); ?>
         
        
<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>
<div id="page-wrapper"  >
    <div class="row  border-bottom white-bg dashboard-header">
        <div class="panel-body widget-shadow ">
            <div class="row">  
                <div class="col-lg-12  "  >
                    <div class="ibox float-e-margins">
                        <div class="ibox-title tables">
                            <h4> List</h4> 
                        </div>
                        <hr>
                        <div class="ibox-content">

                            <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover data-table" >
                        <thead>
                        <tr>
                            <th>Sr#</th>
                            <th>Name</th>
                            <th>Package</th>
                            <th>Amount</th>
                            <th>Package Duration</th>
                            <th>Payment Method</th> 
                            <th>Payment ID</th>   
                        </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	$sr=1;
                            if (isset($active_package)) 
                            {
                               
                            
                                foreach($active_package as $customer)
                                { 

                                    
                                    
                                    ?>
                                    <tr class="gradeX" style="text-align:center;">

                                    	<td class="text-left"><?php echo $sr; ?></td>

                                        <td class="text-left"><?php echo $customer->first_name.' '.$customer->last_name; ?></td>

                                        <td class="text-left"><?php echo ucfirst($customer->pkg_name); ?></td>

                                        <td class="text-left"><?php echo $customer->amount; ?></td>

                                        <td class="text-left"><?php echo  date("d-m-Y", strtotime($customer->start_date)); ?> - <?php echo date("d-m-Y", strtotime($customer->end_date)); ?></td>

                                        <td class="text-left"><?php echo ucfirst($customer->method); ?></td>
     

                                        <td class="text-left">
                                            <?php 
                                            if (empty($customer->paymentID)):  
                                                echo "Free Trial ";
                                            else:
                                                echo $customer->paymentID;
                                            endif;
                                            ?>
                                                
                                        </td>
     
     
                                         
                                    </tr>
                                    <?php 

                                    $sr++;
                                }

                            }
                            ?>
                        </tbody>
                       
                        </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div> 


<?php  echo $this->load->view('admin/layout/footer','',true); ?>