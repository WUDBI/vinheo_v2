<?php echo $this->load->view('admin/layout/header','',true); ?>
         
        
<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>
<div id="page-wrapper" class="gray-bg dashbard-1">  
    <div class="row  border-bottom white-bg dashboard-header">
        <div class="panel-body widget-shadow ">
            <div class="row"> 
            <div class="col-lg-12 "  >
            <form method="POST" action="<?php echo base_url(); ?>Admin/email_templates">
                <div class="col-lg-12">
                    <?php if(!empty($this->session->flashdata('success'))){ ?>
                      <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <span><i class="fa fa-check-circle"></i></span> <?= $this->session->flashdata('success'); ?>
                      </div>
                    <?php } ?>
                    <div class="tabs-container">
                        <div class="tabs-left">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> User Registration</a></li>
                                <li><a data-toggle="tab" href="#tab-2" aria-expanded="true"> User Verification</a></li>
                                <li><a data-toggle="tab" href="#tab-3" aria-expanded="true">Reset Password</a></li>
                                 


                                
                            </ul>
                            <div class="tab-content " style="float: right; width: 100%;">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">
                                        <textarea class="summernote" name="user_registration"><?php echo isset($page_content->user_registration) ? $page_content->user_registration : ''; ?></textarea>
                                    </div>
                                </div>
                                <div id="tab-2" class="tab-pane">
                                    <div class="panel-body">
                                        <textarea class="summernote" name="user_verification"><?php echo isset($page_content->user_verification) ? $page_content->user_verification : ''; ?></textarea>
                                    </div>
                                </div>
                                <div id="tab-3" class="tab-pane">
                                    <div class="panel-body">
                                         <textarea class="summernote" name="reset_pass"><?php echo isset($page_content->reset_pass) ? $page_content->reset_pass : ''; ?></textarea>
                                    </div>
                                </div>  
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <input type="submit" name="submit" class="btn btn-info btn_save" value="Save" style="margin-top:20px; margin-left:20px;">
                    </div>
                </div>
            </form>
            </div>
            </div>
        </div>
    </div>
</div>
<?php  echo $this->load->view('admin/layout/footer','',true); ?>
