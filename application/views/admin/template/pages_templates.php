<?php echo $this->load->view('admin/layout/header','',true); ?>
         
        
<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>

<div id="page-wrapper" class="gray-bg dashbard-1"> 
    <div class="row  border-bottom white-bg dashboard-header">
        <div class="panel-body widget-shadow ">
            <div class="row"> 
            <div class="col-lg-12 "  >
            <form method="POST" action="<?php echo base_url(); ?>Admin/pages_templates">
                <div class="col-lg-12">
                    <?php if(!empty($this->session->flashdata('success'))){ ?>
                      <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <span><i class="fa fa-check-circle"></i></span> <?= $this->session->flashdata('success'); ?>
                      </div>
                    <?php } ?>
                    <div class="tabs-container">
                        <div class="tabs-left">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Quality  Videos</a></li>
                                <li><a data-toggle="tab" href="#tab-2" aria-expanded="true"> Privacy Control</a></li>
                                <li><a data-toggle="tab" href="#tab-3" aria-expanded="true"> Team</a></li>
                                <li><a data-toggle="tab" href="#tab-4" aria-expanded="true"> Public and Grow</a></li>
                                <li><a data-toggle="tab" href="#tab-5" aria-expanded="true">Streaming</a></li>
                                 


                                
                            </ul>
                            <div class="tab-content " style="float: right; width: 100%;">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">
                                        <textarea class="summernote" name="quality_video"><?php echo isset($page_content->quality_video) ? $page_content->quality_video : ''; ?></textarea>
                                    </div>
                                </div>
                                <div id="tab-2" class="tab-pane">
                                    <div class="panel-body">
                                        <textarea class="summernote" name="privacy_control"><?php echo isset($page_content->privacy_control) ? $page_content->privacy_control : ''; ?></textarea>
                                    </div>
                                </div>
                                <div id="tab-3" class="tab-pane">
                                    <div class="panel-body">
                                         <textarea class="summernote" name="team"><?php echo isset($page_content->team) ? $page_content->team : ''; ?></textarea>
                                    </div>
                                </div> 

                                <div id="tab-4" class="tab-pane">
                                    <div class="panel-body">
                                         <textarea class="summernote" name="public_pg"><?php echo isset($page_content->public_pg) ? $page_content->public_pg : ''; ?></textarea>
                                    </div>
                                </div>

                                <div id="tab-5" class="tab-pane">
                                    <div class="panel-body">
                                         <textarea class="summernote" name="streaming"><?php echo isset($page_content->streaming) ? $page_content->streaming : ''; ?></textarea>
                                    </div>
                                </div>   
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <input type="submit" name="submit" class="btn btn-info btn_save" value="Save" style="margin-top:20px; margin-left:20px;">
                    </div>
                </div>
            </form>
            </div>
            </div>
        </div>
    </div>
</div>
<?php  echo $this->load->view('admin/layout/footer','',true); ?>
