<?php echo $this->load->view('admin/layout/header','',true); ?>

         

        

<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>

<div id="page-wrapper" class="gray-bg dashbard-1">

        

    <div class="row  border-bottom white-bg dashboard-header">

        <div class="panel-body widget-shadow ">

            <div class="row">

                <div class="col-lg-12">

                <div class="ibox float-e-margins">

                    <div class="ibox-title tables">

                        <h4> Videos Listing</h4>

                        

                    </div>

                    <hr>

                    <div class="ibox-content">



                    <div class="table-responsive">

                        <table class="table-responsive table table-striped table-bordered table-hover data-table dataTables2_w_o_pag2" >

                            <thead>

                                <tr>

                                    <th>Sr#</th>

                                    <th style="    width: 20%;">Name</th>

                                    <th>User Name </th>    

                                    <th>Location</th> 

                                    <th>Email</th>    

                                    <th>User Package</th>  

                                    <th>Download</th>    

                                    <th>Status</th>   

                                    <th style=" width: 20%;">Uploaded On</th>

                                    <th>Action</th>

                                </tr>

                            </thead>

                            <tbody>

                            	<?php

                            	$sr=1;

                                if (isset($videosList))  
                                { 
                                    foreach($videosList as $key =>  $video)

                                    {  

                                        ?>

                                        <tr class="gradeX" style="text-align:center;">



                                        	<td class="text-left"><?php echo $sr; ?></td>



                                            <td class="text-left"><?php echo $video->title; ?></td>



                                            <td class="text-left"><?php echo $video->first_name.' '.$video->last_name; ?></td>



                                            <td class="text-left"><?php echo $video->location; ?></td>



                                            <td class="text-left"><?php echo $video->email; ?></td>

                                            <td class="text-left"><?php echo ucfirst($video->user_package); ?></td>

                                            <td class="text-left"><?php echo $video->download; ?></td>


                                            <td class="text-left"><?php echo ucfirst($video->status); ?></td>



                                            <td class="text-left"><?php echo date('M d Y g:i A', strtotime($video->created_on)); ?></td>

                                            

                                            <td class="text-left">


 



                                                



                                            </td>

                                        </tr>

                                        <?php 



                                        $sr++;

                                    }



                                }

                                ?>

                            </tbody>

                       

                        </table>

                    </div>



                    </div>

                </div>

            </div>

            </div>

        </div>

    </div>

</div>

<?php  echo $this->load->view('admin/layout/footer','',true); ?>

