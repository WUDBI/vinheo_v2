<!DOCTYPE HTML>
<html>
<head>
<title>Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 


<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>assets/admin/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo base_url(); ?>assets/admin/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->

 <!-- side nav css file -->
 <link href='<?php echo base_url(); ?>assets/admin/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 
 <!-- js-->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- Metis Menu -->
<script src="<?php echo base_url(); ?>assets/admin/js/metisMenu.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/custom.js"></script>
<link href="<?php echo base_url(); ?>assets/admin/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

<link href="<?php echo base_url(); ?>assets/admin/summernote/summernote.css" rel="stylesheet">

</head> 
<body class="cbp-spmenu-push">  
	<div class="main-page login-page " style="width: 34%;">
		<h2 class="title1">Login</h2>
		<div class="widget-shadow">
			<div class="login-body">
				<form action="<?php echo base_url(); ?>admin/auth/userlogin" method="post">
					<input type="email"   class="user" name="user_name" id="username"  placeholder="Enter Your Email" required="">
					<input type="password" name="password" id="password" class="lock" placeholder="Password" required="">
					<div class="forgot-grid">
						 
					</div>
					<input type="submit" name="login" value="Login">
					<div class="registration">
						 
					</div>
				</form>
			</div>
		</div>
		 
	</div> 

	
	<!-- side nav js -->
	<script src='<?php echo base_url(); ?>assets/admin/js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="<?php echo base_url(); ?>assets/admin/js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="<?php echo base_url(); ?>assets/admin/js/jquery.nicescroll.js"></script>
	<script src="<?php echo base_url(); ?>assets/admin/js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.js"> </script>

   <script src="<?php echo base_url(); ?>assets/admin/summernote/summernote.min.js"></script>
    <!-- Flot --><script>
        $(document).ready(function(){

            $('.summernote').summernote({
                height: 350,
            });

       });
    </script>
   
</body>
</html>