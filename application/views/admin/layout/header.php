<!DOCTYPE HTML>

<html>

<head>

<title>Admin</title>

<meta name="viewport" content="width=device-width, initial-scale=1">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

 

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>



<!-- Bootstrap Core CSS -->

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->





<!-- Custom CSS -->

<link href="<?php echo base_url(); ?>assets/admin/css/bootstrap.css" rel='stylesheet' type='text/css' />

<link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel='stylesheet' type='text/css' />



<!-- font-awesome icons CSS -->

<link href="<?php echo base_url(); ?>assets/admin/css/font-awesome.css" rel="stylesheet"> 

<!-- //font-awesome icons CSS -->



 <!-- side nav css file -->

 <link href='<?php echo base_url(); ?>assets/admin/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>

 <!-- side nav css file -->

 

 <!-- js-->

<script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.11.1.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/modernizr.custom.js"></script>



<!--webfonts-->

<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">

<!--//webfonts--> 



<!-- Metis Menu -->

<script src="<?php echo base_url(); ?>assets/admin/js/metisMenu.min.js"></script>

<script src="<?php echo base_url(); ?>assets/admin/js/custom.js"></script>

<link href="<?php echo base_url(); ?>assets/admin/css/custom.css" rel="stylesheet">

<!--//Metis Menu -->



<link href="<?php echo base_url(); ?>assets/admin/summernote/summernote.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">

</head> 

<body class="cbp-spmenu-push">

	<div class="main-content">



	<?php if (!isset($hide_for_login)): ?>

	  

	

		<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">

		 

	   <aside class="sidebar-left">

	   <nav class="navbar navbar-inverse">

			<div class="navbar-header">

			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">

			  <span class="sr-only">Toggle navigation</span>

			  <span class="icon-bar"></span>

			  <span class="icon-bar"></span>

			  <span class="icon-bar"></span>

			  </button>

			  <h1><a class="navbar-brand" href="<?php echo base_url(); ?>"><span class="fa fa-video-camera"></span> Vinheo<span class="dashboard_text">Management</span></a></h1>

			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			 	<ul class="sidebar-menu">

				  	<li class="header">MAIN NAVIGATION</li>

				  	<li class="treeview">

					   	<a href="<?php echo base_url(); ?>admin/dashboard">

							<i class="fa fa-pie-chart"></i> <span>Dashboard</span>

					   	</a>

				  	</li>





				  	<li class="<?php echo isset($Customers) ? 'active_man' : ''; ?>">

						<a href="<?php echo base_url(); ?>admin/users/all"><i class="fa fa-users"></i> <span class="nav-label">Users</span></a>

				  	</li>


				  	<li class="<?php echo isset($Customers) ? 'active_man' : ''; ?>">

						<a href="<?php echo base_url(); ?>admin/videos/all"><i class="fa fa-video-camera"></i> <span class="nav-label">Videos</span></a>

				  	</li>



				  	<li class="treeview">

					   	<a href="<?php echo base_url(); ?>admin/slider">

							<i class="fa fa-picture-o"></i> <span>Slider</span>

					   	</a>

				  	</li> 

				  



				   	<li class="treeview">

					   	<a href="#">

							<i class="fa fa-laptop"></i>

							<span>Templates</span>

							<i class="fa fa-angle-left pull-right"></i>

					   	</a>

					   	<ul class="treeview-menu">

							<li><a href="<?php echo base_url(); ?>admin/template/email_templates"><i class="fa fa-angle-right"></i>Email Template</a></li>

							<li><a href="<?php echo base_url(); ?>admin/template/pages_templates"><i class="fa fa-angle-right"></i> Page Template</a></li>

					   	</ul>

				  	</li> 





				  	<li class="treeview">

					   	<a href="#">

							<i class="fa fa-cog"></i>

							<span>Setting</span>

							<i class="fa fa-angle-left pull-right"></i>

					   	</a>

					   	<ul class="treeview-menu">

							<li><a href="<?php echo base_url(); ?>admin/settings"><i class="fa fa-angle-right"></i>Site Setting</a></li>

							<li><a href="<?php echo base_url(); ?>admin/settings/prices_setting"><i class="fa fa-angle-right"></i>Prices Setting</a></li>



							<li><a href="<?php echo base_url(); ?>admin/settings/pages_template_setting"><i class="fa fa-angle-right"></i>Template Setting</a></li>



							<li><a href="<?php echo base_url(); ?>admin/settings/about_us"><i class="fa fa-angle-right"></i>About Us</a></li>

							 

					   	</ul>

				  	</li> 





				  	<li class="treeview">

					   	<a href="#">

							<i class="fa fa-file-text-o"></i>

							<span>Reports</span>

							<i class="fa fa-angle-left pull-right"></i>

					   	</a>

					   	<ul class="treeview-menu">

							<li><a href="<?php echo base_url(); ?>admin/reports/subscription_report"><i class="fa fa-angle-right"></i>Subscription Report</a></li>

							<li><a href="<?php echo base_url(); ?>admin/reports/active_package"><i class="fa fa-angle-right"></i>Active Packages</a></li>

							 

					   	</ul>

				  	</li> 

				 

					 

			 	</ul>

			</div>

			<!-- /.navbar-collapse -->

	   </nav>

	   </aside>

		</div>

	<?php endif ?>