<?php echo $this->load->view('admin/layout/header','',true); ?>
         
        
<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />

<div id="page-wrapper"  >  
    <div class="row  border-bottom white-bg dashboard-header">
        <div class="panel-body widget-shadow ">
            <div class="row">
                <div class="col-lg-12">
                    <?php if(!empty($this->session->flashdata('success'))){ ?>
                      <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <span><i class="fa fa-check-circle"></i></span> <?= $this->session->flashdata('success'); ?>
                      </div>
                    <?php } ?>

                    <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>admin/users/update" enctype="multipart/form-data">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title tables" style="padding:13px 23px;">
                                <h4>Edit Information</h4> 
                            </div>
                            <hr>
                            <div class="ibox-content" style="padding:13px 23px;">
                                
                                    <input type="hidden" name="uid" value="<?php echo isset($uid) ? $uid : ''; ?>">

                                                   
                                    
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">First Name</label>
                                        <div class="col-lg-10">
                                            <input name="first_name" value="<?php echo $customer->first_name; ?>" type="text" placeholder="First Name" class="form-control" > 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Last Name</label>
                                        <div class="col-lg-10">
                                            <input name="last_name" value="<?php echo $customer->last_name; ?>" type="text" placeholder="Last Name" class="form-control" > 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Email</label>
                                        <div class="col-lg-10">
                                            <input name="email" value="<?php echo $customer->email; ?>" type="text" placeholder="Email" class="form-control" > 
                                        </div>
                                    </div>
                 

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Location</label>
                                        <div class="col-lg-10">
                                            <textarea  name="location" rows="4" placeholder="Location" class="form-control" ><?php echo $customer->location; ?></textarea> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Status</label>
                                        <div class="col-lg-10">
                                            <select class="form-control" name="status">
                                                <option <?php if($customer->status == 'active'){ echo "selected"; } ?> value="active">Active</option>
                                                <option <?php if($customer->status == 'deactive'){ echo "selected"; } ?>  value="deactive">Deactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Confirm Email</label>
                                        <div class="col-lg-10">
                                            <select class="form-control" name="email_confirm">
                                                <option  <?php if($customer->email_confirm == 'yes'){ echo "selected"; } ?>  value="yes">Yes</option>
                                                <option <?php if($customer->email_confirm == 'not'){ echo "selected"; } ?>  value="not">No</option>
                                            </select>
                                        </div>
                                    </div>



                                  
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button name="submit" class="btn btn-sm btn-info" type="submit">Update</button>
                                        </div>
                                    </div>

                            </div>
                        </div>
                        
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php  echo $this->load->view('admin/layout/footer','',true); ?>


<script type="text/javascript">
  $(document).ready(function() {
    $('.country').select2({
      placeholder: 'Select Country'
    });
  });
</script>


