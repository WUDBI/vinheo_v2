<?php echo $this->load->view('admin/layout/header','',true); ?>

         

        

<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>

<div id="page-wrapper" class="gray-bg dashbard-1">

        

    <div class="row  border-bottom white-bg dashboard-header">

        <div class="panel-body widget-shadow ">

            <div class="row">

                <div class="col-lg-12">

                <div class="ibox float-e-margins">

                    <div class="ibox-title tables">

                        <h4> Users List</h4>

                        

                    </div>

                    <hr>

                    <div class="ibox-content">



                        <div class="table-responsive">

                    <table class="table table-striped table-bordered table-hover data-table dataTables2_w_o_pag2" >

                    <thead>

                    <tr>

                        <th>Sr#</th>

                        <th>Name</th>

                        <th>Email</th>    

                        <th>Location</th>    

                        <th>Status</th>   

                        <th>Created on</th>

                        <th>Action</th>

                    </tr>

                    </thead>

                    <tbody>

                    	<?php

                    	$sr=1;

                        if (isset($customers)) 

                        {

                           

                        

                            foreach($customers as $customer)

                            { 

                                

                                ?>

                                <tr class="gradeX" style="text-align:center;">



                                	<td class="text-left"><?php echo $sr; ?></td>



                                    <td class="text-left"><?php echo $customer->first_name.' '.$customer->last_name; ?></td>



                                    <td class="text-left"><?php echo $customer->email; ?></td>



                                    <td class="text-left"><?php echo $customer->location; ?></td>



                                    <td class="text-left"><?php echo ucfirst($customer->status); ?></td>



                                    <td class="text-left"><?php echo date('d-m-Y', strtotime($customer->created_on)); ?></td>

                                    

                                    <td class="text-left">



                                        <a href="<?php echo base_url(); ?>admin/users/edit/<?php echo $customer->user_id; ?>" class="btn btn-info"><i class="fa fa-pencil"></i></a>



                                        



                                    </td>

                                </tr>

                                <?php 



                                $sr++;

                            }



                        }

                        ?>

                    </tbody>

                   

                    </table>

                        </div>



                    </div>

                </div>

            </div>

            </div>

        </div>

    </div>

</div>

<?php  echo $this->load->view('admin/layout/footer','',true); ?>

