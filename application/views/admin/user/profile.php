<?php echo $this->load->view('admin/layout/header','',true); ?>
         
        
<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>
<div id="page-wrapper" class="gray-bg dashbard-1">  
    <div class="row  border-bottom white-bg dashboard-header">
        <div class="panel-body widget-shadow ">
            <div class="row"> 
            <div class="col-lg-12 "  >
        <?php if(!empty($this->session->flashdata('success'))){ ?>
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <span><i class="fa fa-check-circle"></i></span> <?= $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>
        <div class="ibox float-e-margins">
            <div class="ibox-title" style="padding:13px 23px;">
                <h4>Profile</h4>
                
            </div>

            <hr>
            <div class="ibox-content" style="padding:13px 23px;">
                <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>admin/admin/profile" enctype="multipart/form-data">
                    
                    <div class="form-group"><label class="col-lg-2 control-label">First Name</label>
                        <div class="col-lg-10"><input name="first_name" value="<?php echo isset($admin_profile->first_name) ? $admin_profile->first_name : ''; ?>" type="text" placeholder="First Name" class="form-control"> 
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Last Name</label>
                        <div class="col-lg-10"><input name="last_name" value="<?php echo isset($admin_profile->last_name) ? $admin_profile->last_name : ''; ?>" type="text" placeholder="Last Name" class="form-control"> 
                        </div>
                    </div>
                     
                    <div class="form-group"><label class="col-lg-2 control-label">Email</label>
                        <div class="col-lg-10"><input name="email" value="<?php echo isset($admin_profile->email) ? $admin_profile->email : ''; ?>" type="text" placeholder="Email" class="form-control"> 
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Password</label>
                        <div class="col-lg-10"><input name="password" value="" type="password" autocomplete="off" placeholder="Password" class="form-control"> 
                        </div>
                    </div>
                   <!-- <div class="form-group"><label class="col-lg-2 control-label">Image</label>

                        <div class="col-lg-10"><input name="cover_pic" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" type="file" class="form-control"> 
                        </div>
                       
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label"></label>

                        <div class="col-lg-10">
                            <?php  
                              if(!empty($admin_profile->profile_pic)){
                                $profile_pic = base_url().'assets/images/profiles/admin/'.$admin_profile->profile_pic; 
                              }else{
                                $profile_pic = base_url().'assets/images/profiles/admin/avatar2.png'; 
                              } 
                            ?> 

                             <img id="blah" src="<?php echo $profile_pic; ?>"width="200" height="200" />
                        </div>
                    </div> -->
                    
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button name="submit" class="btn btn-sm btn-info" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
            </div>
            </div>
        </div>
    </div>
</div>
<?php  echo $this->load->view('admin/layout/footer','',true); ?>
