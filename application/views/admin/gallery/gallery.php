<?php echo $this->load->view('admin/layout/header','',true); ?>
		 
		
<?php echo $this->load->view('admin/layout/sticky_header','',true); ?>

<style type="text/css">
.modal-dialog {width:800px;}
.thumbnail {margin-bottom:6px;}
.modal.fade .modal-dialog {
  	-webkit-transform: translate(0, 0);
  	-ms-transform: translate(0, 0); // IE9 only
  	transform: translate(0, 0); 
}
</style>
		<div id="page-wrapper">
			<div class="main-page">
				<div class="forms">
					 
					<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
						<div class="form-title">
							<h4>Add New Image</h4>
						</div>
						<div class="form-body">
							<form action="<?php echo base_url(); ?>admin/gallery/upload_gallery" method="post" enctype="multipart/form-data"> 
								<div class="form-group"> 
									<label for="title_image">Title</label> 
									<input type="text" class="form-control" id="title_image" placeholder="Title" name="title_image"> 
								</div> 

								<div class="form-group"> 
									<label for="title_image">Gallery Image</label> 
									<input type="file" class="form-control" id="gallery_img"   name="gallery_img"> 
								</div>  	

								<div class="checkbox"> 
									<label> <input type="checkbox" name="active">Active </label> 
								</div> 

								<button type="submit" class="btn btn-default">Submit</button> 
							</form> 
						</div>
					</div>


					<div class="form-grids row widget-shadow" style="    padding: 30px 0px;" >
					 	<div class="row"> 
					      	<?php if (isset($gallery_list) and !empty($gallery_list)): ?>
					      		<?php foreach ($gallery_list as $key => $value): ?>
					      			<div class="col-lg-4  ">
					      				<a href="#" title="<?php echo $value->title; ?>">
					      					<img src="<?php echo base_url(); ?>assets/gallery_images/<?php echo $value->name; ?>" class="thumbnail img-responsive">
					      				</a>
					      			</div>	
					      		<?php endforeach ?>
					      	<?php endif ?>
				    	</div>
			    	</div>
				</div>

  
			   
				    	 
				 
				<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
				  	<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h3 class="modal-title">Heading</h3>
							</div>
							<div class="modal-body">
								
							</div>
							<div class="modal-footer">
								<a href="#" id="gallery-previous" class="controls btn btn-lg">Previous</a>
								<a href="#" id="gallery-next" class="controls btn btn-lg">Next</a>
								<button class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
				  	</div>
				</div>
				 
			</div>
		</div>
 
<?php  echo $this->load->view('admin/layout/footer','',true); ?>

<script type="text/javascript">
$('.thumbnail').click(function(){
	$('.modal-body').empty();
	var title = $(this).parent('a').attr("title");
	$('.modal-title').html(title);
	$($(this).parents('div').html()).appendTo('.modal-body');
	$('#myModal').modal({show:true});
});

$('a.controls').click(function(){
   
  
  alert ('control!');
  
  });  
</script>