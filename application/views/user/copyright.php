<?php include('layout/header.php'); ?>

<style type="text/css">
	.legal_doc p{
		text-align: justify;
	}
</style>	
		<section class="privacy_sset">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div id="legal_doc" class="legal_doc">
							 
							<h1 style="text-align: center;border: none;padding-bottom: 57px;">VINHEO COPYRIGHT POLICY</h1>
						 	

						 	<p>This Copyright Policy is a portion of the Terms of Vinheo’s Service and presents the procedure for copyright agents and owners to take off any material that ostensibly infringes upon their rights from Vinheo’s services online.</p>


						 	<h2>1). DMCA Policy</h2>
						 	<p>Vinheo values the ideas and inventions of others and requires that users do likewise. Users are charged to make sure that any material uploaded is not an infringement upon the copyrights of third parties. Any material that is an infringement for which Vinheo is adequately informed according to the DMCA (Digital Millennium Copyright Act) will be taken down immediately. Additionally, in applicable situations, Vinheo will not hesitate to close the accounts of those who repeatedly infringe upon third-party copyrights.</p>

						 	<p>As you file a request, make sure that you complete your notice and submit precise statements. Should we ask for more information, please ensure that you respond immediately. Failure to offer the needed details may deter further processing of your request. If your complaint does not pertain to copyright, look at the Privacy Complaint Form or the Trademark Infringement Complaint Form.</p>


						 	<h2>2). DMCA Notices for Takedowns</h2>

						 	<p>When requesting for copyright infringing materials to be removed, the notice to be filed has to include : </p>

						 	<ul style="list-style-type: circle;">
						 		<li><p> Name, telephone, address and email.</p></li>
						 		<li><p> Narrative of the work for which the copyright has been impinged on.</p></li>
						 		<li><p> Narrative of the work for which the copyright has been impinged on.</p></li>
						 		<li><p> Narrative of the location of the infringing material on Vinheo’s service, to help Vinheo find and identify it – the URL, for instance.</p></li>
						 		<li><p> Good faith declaration of belief that the use of the copyrighted work is unauthorized by the law, the agent or the owner.</p></li>
						 		<li><p> Declaration UNDER THE PENALTY OF PERJURY that all the information provided is precise and that you are either the owner by copyright or an authorized agent acting on their behalf.</p></li>

						 		<li><p> Physical or electronic sign.</p></li>
						 	</ul>

						 	<p>Notices may be filed:</p>

						 	<ul style="list-style-type: circle;">
						 		<li><p> Offline: Vinheo’s Copyright Agent</p></li>
						 		<li><p> By email: soporte@vinheo.com</p></li>
						 	</ul>

						 	<p>Vinheo may make notices known to the users and third-parties involved via databases for collecting data concerning notices of copyright takedowns.</p>


						 	<h2>3). DMCA Counter-Notifications</h2>

						 	<p>For Vimeo users wishing to contest the deletion of materials resulting from DMCA takedown notices, counter-notifications will have to be files and include:</p>

						 	<ul style="list-style-type: circle;">
						 		<li><p> Name, telephone, address and email.</p></li>
						 		<li><p> Narrative of the removed work and its former location on Vinheo’s service, to help Vinheo find and identify it – the URL, for instance.</p></li>
						 		<li><p> Good faith declaration UNDER THE PENALTY OF PERJURY that the removed work was misidentified or mistaken.</p></li>
						 		<li><p> Declaration of consent to the authority of the Federal District Court of your location. For addresses outside the COLOMBIA, any judicial district in which Vinheo is found can be used. You must also obtain service of process from the filer or agent responsible for the initial DMCA notice.</p></li> 

						 		<li><p> Physical or electronic sign.</p></li>
						 	</ul>


						 	<p>Notices may be filed:</p>

						 	<ul style="list-style-type: circle;">
						 		<li><p> Offline: Vinheo’s Copyright Agent</p></li>
						 		<li><p> By email: soporte@vinheo.com</p></li>
						 	</ul>

						 	<p>Vinheo will transfer completed counter-notifications to the filer of the initial DMCA notice. The owner of the copyright may choose to file a lawsuit against the filer of the counter-notification if no notice of such a lawsuit is given after a period of ten working days after they are informed of the counter-notification, the work in question may be restored, but till then, it stays down.</p>

						 	<h2>4). Recurring Offenders</h2>

						 	<p>After three DMCA strikes – each time that work is taken down from a user’s account owing to a DMCA notice – Vinheo will close the account of offending users. Multiple DMCA notices per time may be classified as a single strike.</p>

						 	<p>DMCA strikes may be removed in fitting situations, for instance, when:</p>

						 	<ul style="list-style-type: circle;">
						 		<li><p> The work in question is restored owing to a counter-notification</p></li>
						 		<li><p> The claim of infringement is withdrawn.</p></li>
						 	</ul>

						 	<p>Users with less than three DMCA strikes may be terminated in fitting situation, for instance, if the user has been known to violate or disregard Vinheo’s Terms of Service.</p>

						 	<h2>5). Vinheo’s Copyright Agent</h2>
						 	
						 	<p>Inquiries can be addressed to our Copyright Agent.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

<?php include('layout/footer.php'); ?>