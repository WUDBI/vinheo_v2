<?php $this->load->view('user/layout/header'); ?>



 <style type="text/css">

 	.label_profile

 	{

		font-weight: bolder !important;

   	 	font-size: larger !important;

    	margin: 0px !important;

 	}

 	.p_profile{

 		font-size: 12px !important;

 	}

 	.vertical_tab{

	    padding: 15px 6px !important;

    	border-radius: 5px !important;

    	height: 300px !important;

    	background-color: #EDEDED !important;

 	}



 	.setting_tab{

 		font-size: large !important;

    	text-align: center !important;

    	padding: 0px 8px 15px 0px !important;

    	margin-bottom: 11px; 

   	 	border-bottom: 1px solid #D5BA9F;

 	}



 	#profile .nice-select{

 		display: none !important;

 	}



 	#show_me{

 		display: block !important;

 		margin-top: 9px;

 	}

 	#myTabContent h2{

 		margin-bottom: 41px;

 	}

 	.btn_style{

 		 width: 25%;

 		 background-color: #333333 !important;

 	}

 </style>





<div class="manage_video" style="background: #f9fcff url(<?php echo base_url() ?>assets/img/user_profile.jpg) no-repeat;background-size: 100% 100%;">

		<h2 style="color: #fff;"><?php echo  lang('User_Profile') ?></h2>

	</div>



	<section class="contact-section" id="user_profiles">

		<div class="container">

			<div class="row" id="tabs_layouts"> 

			 	<div class="col-12  ">

			 		<div class="container">





						<?php if ($this->session->flashdata('success')): ?> 

					      <div class="alert alert-success alert-dismissible " role="alert">

					            <?php echo $this->session->flashdata('success'); ?>

					            <button type="button" class="close" data-dismiss="alert" aria-label="Close">

					                  <span aria-hidden="true">&times;</span>

					            </button>

					      </div> 

					    <?php endif ?>





					    <?php if ($this->session->flashdata('error')): ?> 

					      <div class="alert alert-warning alert-dismissible " role="alert">

					            <?php echo $this->session->flashdata('error'); ?>

					            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> 
				                  	<span aria-hidden="true">&times;</span> 
					            </button>

					      </div> 

					    <?php endif ?>

					    <!-- <h1>Bootstrap 4 Vertical Nav Tabs</h1>

					  	<hr> -->

				  		<div class="row">

						    <div class="col-md-3 mb-3 vertical_tab" >

						        <ul class="nav nav-pills flex-column" id="myTab" role="tablist">

								  	<li class="nav-item setting_tab">

								    	<?php echo  lang('Settings') ?>

								  	</li>

								  	<li class="nav-item">

								    	<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><?php echo  lang('General') ?></a>

								  	</li>

								  	<li class="nav-item">

								    	<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><?php echo  lang('Work') ?></a>

								  	</li>

									<li class="nav-item">

										<a class="nav-link" id="password-tab" data-toggle="tab" href="#password" role="tab" aria-controls="password" aria-selected="false">Change Password</a>

									</li>


									<li class="nav-item">

										<a class="nav-link" id="profilepicture-tab" data-toggle="tab" href="#profilepicture" role="tab" aria-controls="profilepicture" aria-selected="false">Profile Picture</a>

									</li>

								</ul>

						    </div>

					    	<!-- /.col-md-4 -->

			        		<div class="col-md-9">



					      		<div class="tab-content" id="myTabContent"  >



					  				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"> 

					  					<form action="<?php echo base_url(); ?>profile/save_general" method="post">

						  					<h2 class="heading_tab_h2"><?php echo  lang('General') ?></h2>

						    				<div class="row padding_tab_row" >

						    					<div class="col-md-12  form-group">

						    						<label class="label_profile">Bio</label>

						    						<p class="p_profile"><?php echo  lang('max_note') ?></p>



						    						<textarea rows="4"  class="form-control" name="bio"><?php echo isset($user_date[0]->bio)? $user_date[0]->bio : ''; ?></textarea>

						    					</div>





						    					<div class="col-md-12  form-group">

						    						<label class="label_profile"><?php echo  lang('About') ?></label>

						    						<p class="p_profile"><?php echo  lang('max_note2') ?></p>



						    						<textarea rows="7"  class="form-control" name="about"><?php echo isset($user_date[0]->about)? $user_date[0]->about : ''; ?></textarea>

						    					</div>





						    					<div class="col-md-12 form-group">

						    						<label class="label_profile"><?php echo  lang('Location') ?></label>

						    						 



						    						<textarea rows="1"  class="form-control" name="location"><?php echo isset($user_date[0]->location)? $user_date[0]->location : ''; ?></textarea>

						    					</div>



						    					<div class="col-md-12 form-group">

						    						<label class="label_profile"><?php echo  lang('Your_website') ?></label>

					    						 	<p class="p_profile"><?php echo  lang('Your_website2') ?></p>



					    						 	<label class="label_profile">Facebook</label>
					    						 	<input type="text" name="site1" class="form-control">

					    						 	<label class="label_profile">LinkedIn</label>
					    						 	<input type="text" name="site2" class="form-control">

					    						 	<label class="label_profile">Twitter</label>
					    						 	<input type="text" name="site3" class="form-control">

					    						 	 

						    					</div>	



						    					<div class="col-md-12 form-group" style="text-align: center;margin-top: 0%;">

								 					 

								 					<input   type="submit" value="Save" class="btn btn-primary btn_style vinheo_green_button" name="save_general" >

								 				</div>

						    				</div> 

					    				</form>

					  				</div>



					  				<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

					  					<form action="<?php echo base_url(); ?>profile/save_work" method="post">

						  					<h2 class="heading_tab_h2"><?php echo  lang('Work') ?></h2>

						    				<div class="row padding_tab_row">

						    					<div class="col-md-12  form-group">

						    						<label class="label_profile"><?php echo  lang('Roles') ?></label>

						    						 



						    						<textarea rows="1"  class="form-control" name="roles"><?php echo isset($user_date[0]->roles)? $user_date[0]->roles : ''; ?></textarea>

						    					</div>





						    					<div class="col-md-12  form-group">

						    						<label class="label_profile"><?php echo  lang('Project_types') ?></label>

						    						<p class="p_profile"><?php echo  lang('Project_types2') ?></p>



						    						<textarea rows="1"  class="form-control" name="project_type"><?php echo isset($user_date[0]->project_type)? $user_date[0]->project_type : ''; ?></textarea>

						    					</div>





						    					<div class="col-md-12 form-group">

						    						<label class="label_profile"><?php echo  lang('Project_types3') ?>?</label>

						    						 



						    						<select class="form-control" name="your_service" id="show_me">

						    							<option <?php if ($user_date[0]->your_service == 'Freelance professional'): ?> selected <?php endif ?> value="Freelance professional"><?php echo  lang('Project_types4') ?></option>

						    							<option 

						    							 <?php if ($user_date[0]->your_service == 'Agency/Studio'): ?> selected <?php endif ?> 



						    							value="Agency/Studio"><?php echo  lang('Project_types6') ?></option>

						    							<option 

						    							<?php if ($user_date[0]->your_service == 'Brand/Business'): ?> selected <?php endif ?> 



						    							value="Brand/Business"><?php echo  lang('Project_types5') ?></option>

						    						</select>

						    					</div>







								 				<div class="col-md-12 form-group" style="text-align: center;    margin-top: 4%;">

								 					 

								 					<input type="submit" value="Save" class="btn btn-primary btn_style vinheo_green_button" name="save_work" >

								 				</div> 

						    				</div>

						    			</form>

					  				</div>



					  				<div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">

					  					<h2 class="heading_tab_h2">Change Password</h2>
					  					<form  action="<?php echo base_url(); ?>profile/change_password"  method="post">
					    				<div class="row padding_tab_row" > 


					    					
					    					<div class="col-md-12 form-group">

					    						<label class="label_profile">Old Password</label> 

					    						<input type="text" name="password" class="form-control">

					    					</div>

					    					<div class="col-md-12 form-group">

					    						<label class="label_profile">New Password</label> 

					    						<input type="text" name="newpassword" class="form-control">

					    					</div>



					    					<div class="col-md-12 form-group">

					    						<label class="label_profile">Old Password</label> 

					    						<input type="text" name="confirmpassword" class="form-control">

					    					</div>
 

					    					<div class="col-md-12 form-group" style="text-align: center;margin-top: 0%;">

							 					 

							 					<input   type="submit" value="Save" class="btn btn-primary btn_style vinheo_green_button" name="save_general">

							 				</div>
							 				
					    				</div> 
					    				</form>
					  				</div>



					  				<div class="tab-pane fade" id="profilepicture" role="tabpanel" aria-labelledby="profilepicture-tab">

					  					<h2 class="heading_tab_h2">Change Profile Picture</h2>
					  					<form action="<?php echo base_url(); ?>profile/update_profilepicture" enctype="multipart/form-data" method="post">
						    				<div class="row padding_tab_row" >  

						    					<div class="col-md-12 form-group">

						    						<label class="label_profile">Choose Image</label> 

						    						<input type="file" name="profile_img"  onchange="readURL(this);"  class="form-control">

						    					</div>

						    					<div class="col-md-12 form-group">
						    						
						    						<img   src="<?php echo base_url().'assets/img/user_profiles/'.$user_date[0]->user_id.'/'.$user_date[0]->profile_pic; ?>" style="max-width: 100%;" id="preview_img" >

						    					</div>
						    					 


	 

						    					<div class="col-md-12 form-group" style="text-align: center;margin-top: 0%;">

								 					 

								 					<input   type="submit" value="Save" class="btn btn-primary btn_style vinheo_green_button" name="save_general">

								 				</div> 
						    				</div> 
					    				</form>
					  				</div>

								</div>



					    	</div>
 
					  	</div> 

					</div> 

			 	</div>



			</div>

		</div>

	</section>

		



<?php $this->load->view('user/layout/footer'); ?>

