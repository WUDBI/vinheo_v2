<?php include('layout/header.php'); ?>

<style type="text/css">
    h3{
        padding: 20px 0px;
    }
</style>

		<!-- banner part start-->
	<section class="breadcrumb_part breadcrumb_bg" style="    height: 200px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner" style="height: auto;padding-top: 44px;">
                        <div class="breadcrumb_iner_item">
                            <h2><?php echo $this->lang->line('vinheo_info'); ?></h2>
                            <p><?php echo $this->lang->line('vinheo_info2'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section style="padding-top: 27px;">
    	<div class="container"> 
    		<div class="row">
                    <div class="col-md-12 col-lg-12 ">
                    <h2>Cookies</h2> 

                    <p>This website uses third-party and proprietary cookies to get you to have a better browsing experience, you can share content on social networks and so that we can obtain user statistics.</p> 

                    <p>You can avoid downloading cookies through your browser settings, preventing cookies from being stored on your device.</p>

                    <p>As the owner of this website, I inform you that we do not use any personal information from cookies, we only make general statistics of visits that do not involve any personal information.</p>

                    <p>It is very important that you read this cookie policy and understand that, if you continue browsing, we will consider that you accept its use.</p>

                    <p>According to the terms included in article 22.2 of Law 34/2002 on Services of the Information Society and Electronic Commerce, if you continue browsing, you will be giving your consent for the use of the aforementioned mechanisms.</p>

                    <h3>Responsible Entity</h3>

                    <p>The entity responsible for the collection, processing and use of your personal data, in the sense established by the Personal Data Protection Law is the website https://vinheo.com, owned by Vinheo - Soacha parque campestre.</p>

                    <h3>What are cookies?</h3>

                    <p>Cookies are a set of data that a server deposits in the user's browser to collect standard Internet log information and information on the behavior of visitors to a website. In other words, they are small text files that are stored on the computer's hard drive and serve to identify the user when he or she connects to the website again. Their purpose is to record the user's visit and save certain information. Their use is common and frequent on the web since it allows the pages to function more efficiently and to achieve greater personalisation and analysis of user behaviour.</p>

                    <p>Translated with www.DeepL.com/Translator (free version).</p>

                    <h3>What types of cookies exist?</h3>

                    <p>The cookies used on our website are session and third-party, and allow us to store and access information related to the language, the type of browser used, and other general characteristics predefined by the user, as well as, track and analyze the activity which is carried out, in order to introduce improvements and provide our services in a more efficient and personalized way.</p>

                    <p>Cookies, depending on their permanence, can be divided into session or permanent cookies. Those that expire when the user closes the browser. Those that expire depending on when the objective for which they serve is fulfilled (for example, so that the user remains identified in Vinheo services) or when they are manually deleted. </p>



                    <p>Additionally, depending on their objective, cookies can be classified as follows:</p>

                    <h3>Performance Cookies</h3>

                    <p>This type of Cookie remembers your preferences for the tools found in the services, so you do not have to reconfigure the service every time you visit. As an example, this typology includes: Volume adjustments of video or sound players. The video transmission speeds that are compatible with your browser. Objects stored in the “shopping cart” in e-commerce services such as stores.</p>

                    <h3>Geo-location cookies</h3>

                    <p>These cookies are used to find out what country you are in when a service is requested. This cookie is completely anonymous, and is only used to help guide the content to its location.</p>

                    <h3>Registration Cookies</h3>

                    <p>Registration cookies are generated once a user has registered or subsequently opened a session, and are used to identify you in the services for the following purposes:</p>

                    <p>To keep the user identified so that, if he closes a service, the browser or the computer and at another time or another day he enters the service again, he will continue to be identified, thus facilitating his navigation without having to identify himself again. This functionality can be suppressed if the user presses the [close session] functionality, so that this cookie is eliminated and the next time the user enters the service he will have to log in to be identified.</p>

                    <p>Check if the user is authorized to access certain services, for example, to participate in a contest.</p>

                    <p>Additionally, some services may use social network connectors such as Facebook or Twitter. When the user signs up for a service with social network credentials, he or she authorizes the social network to save a persistent Cookie that remembers his or her identity and guarantees access to the services until it expires. The user can delete this Cookie and revoke access to the services through social networks by updating their preferences in the social network they specify.</p>

                    <h3>Analytical Cookies</h3>

                    <p>Each time a user visits a service, a tool from a third-party provider generates an analytical cookie on the user's computer. This cookie, which is only generated during the visit, will be used on future visits to Vinheo's services to identify the visitor anonymously. The main objectives pursued are</p>

                    <ul style="list-style-type: circle;">
                       <li><p>To allow the anonymous identification of the browsing users through the cookie (it identifies browsers and devices, not people) and therefore the approximate counting of the number of visitors and their tendency in time.</p></li>

                       <li><p>Anonymously identify the most visited content and therefore more attractive to users. Know if the user who is accessing is new or repeats a visit.</p></li> 

                    </ul>

                    <p>Important: Unless the user decides to register in a Vinheo service, the cookie will never be associated with any personal data that could identify him. These cookies will only be used for statistical purposes to help optimise the user's experience on the site.</p>

                    <h3>Advertising Cookies</h3>

                    <p>This type of cookie allows to extend the information of the advertisements shown to each anonymous user in the services of Vinheo. Among others, the duration or frequency of visualization of advertising positions, the interaction with them, or the navigation patterns and/or behavior of the user are stored since they help to conform a profile of advertising interest. In this way, they make it possible to offer advertising related to the interests of the user.</p>


                    <h3>Third party advertising cookies</h3>

                    <p>In addition to the advertising managed by the Vinheo websites in their services, the Vinheo websites offer their advertisers the option of serving advertisements through third parties (“Ad-Servers”). In this way, these third parties can store cookies sent from Vinheo services from users' browsers, as well as access the data stored in them.</p>

                    <p>The companies that generate these cookies have their own privacy policies. Currently, Vinheo websites use the Doubleclick (Google) platform to manage these services. For more information, go to <span  style="color: blue">http://www.google.es/policies/privacy/ads/#toc-doubleclick</span> and <span style="color: blue">http://www.google.es/policies/privacy/ads</span> .</p>

                    <h3>How can I disable cookies in my browser?</h3>

                    <p>
                       The different browsers can be configured to notify the user of the reception of cookies and, if desired, prevent their installation on the computer. Likewise, the user can check in his browser what cookies he has installed and what is the expiration date of them, being able to eliminate them.
                    </p>

                    <p>For more information, see the instructions and manuals of your browser:</p>


                    <p>For more information about managing cookies in Google </p>
                    <p  style="color: blue">Chrome: https://support.google.com/chrome/answer/95647?hl=es </p>

                    <p>For more information about the administration of cookies in Internet  </p>
                    <p  style="color: blue">Explorer: http://windows.microsoft.com/es-es/windows-vista/cookies-frequently-asked-questions  </p>

                    <p>Para más información sobre la administración de las cookies en Mozilla </p>
                    <p  style="color: blue">Firefox: http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we</p>

                    <p>For more information about the administration of cookies in Mozilla </p>

                    <p  style="color: blue">Firefox: http://www.apple.com/es/privacy/use-of-cookies/</p>


                    <p>For more information about the administration of cookies in </p> 
                    <p  style="color: blue">Opera: http://help.opera.com/Windows/11.50/es-ES/cookies.html</p>

                    <p>If you want to stop being followed by Google Analytics </p>

                    <p  style="color: blue">visit: http://tools.google.com/dlpage/gaoptout</p>



                    <h3>To know more about cookies</h3>

                    <p>You can get more information about online advertising based on online behavior and privacy at the following link: http://www.youronlinechoices.com/es/</p>

                    <p>Google Analytics data protection: http://www.google.com/analytics/learn/privacy.html</p>

                    <p>How Google Analytics uses cookies: https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage?hl=es#analyticsjs</p>

                    <h3>Updates and changes in the privacy policy / cookies</h3>

                    <p>Vinheo websites may modify this Cookies Policy based on legislative, regulatory requirements, or with the purpose of adapting said policy to the instructions issued by the Spanish Agency for Data Protection, therefore users are advised to visit it periodically . </p>
                    <p>
                    When there are significant changes in this Cookies Policy, they will be communicated to users either through the web or through email to registered users.
                    </p>
                </div>
    		</div>
    	</div>
    </section>
	
<?php include('layout/footer.php'); ?>
	
	 