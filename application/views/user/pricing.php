<?php 

	include('layout/header.php');  

    $CI = &get_instance();

    $CI->load->model('CommonModelAdmin');
    $CI->load->model('CommonModel');

    $setting = $CI->CommonModelAdmin->getsetting();


if ($this->session->userdata('user_id')) 
{
	$trail_data = $CI->CommonModel->getAll('payments','user_id = "'.$this->session->userdata('user_id').'"   order by id desc limit 1 ');


}

 ?>



		<!-- banner part start-->

		<section class="banner_part height-ss">

			<div class="container">

				<div class="row align-items-center">

					<div class="col-lg-12 col-xl-12">

						<div class="banner_text">

						  <div class="banner_text_iner">

							<h1>Choose a plan.</h1>

							  <p>Try a plan free for 30 days, or get started with Vinheo Basic.							</p>

						  </div>

						</div>

					</div>

				</div>

			</div>

		</section>

		<!-- banner part start-->

	

			<section class="pricing_data">

			<div class="container">

				<div class="row top_sticy ">

					<div class="col-lg-4 col-md-12 col-sm-12 pricing-set">

						<h1>Choose a plan.</h1>

						<p>Try any plan risk-free for 30 days. Or, start free with 

						<?php if ( $this->session->userdata('user_id') ): ?>

							<?php if (empty($trail_data) or $trail_data[0]->pkg_name == 'basic'): ?>
								<a  style="font-size: 18px; font-weight: 600; color: #333;" onclick="  alert('You are already using Basic Package');" >Vinheo Basic.</a>

							<?php else: ?>
								<a  href="<?php echo base_url('package/turn_free') ?>" onclick="return confirm('Are you sure you want to convert to free?');" >Vinheo Basic.</a>
							<?php endif ?>
							
						<?php else: ?>
							<a href="<?php echo base_url('home/login') ?>">Vinheo Basic.</a>
						<?php endif; ?>


						</p>
							

					</div>

					<div class="col-lg-2  pricing-one border_top_1" id="border_top_1">

						<ul>

							<li><h3>Plus</h3>

						    </li>

							<li>5GB/week</li>

						</ul>

					</div>

					<div class="col-lg-2  pricing-one border_top_2" id="border_top_2">

						<ul>

							<li><h3>Pro</h3>

							</li>

                            <li>20GB/week</li>

						</ul>

					</div>

					<div class="col-lg-2  pricing-one border_top_3" id="border_top_3">

						<ul>

							<li><h3>Business</h3>

						    </li>

							<li>No weekly limits</li>

						</ul>

					</div>

					<div class="col-lg-2  pricing-one border_top_4" id="border_top_4">

						<ul>

							<li><h3>Premium</h3>

						    </li>

							<li>Unlimited live streaming</li>

						</ul>

					</div>

				</div>

			</div>

		</section>

		<section class="pricing_data_set">

			<div class="container">

				<div class="row top_sticy pricing_data_234">

					<div class="col-lg-4  pricing-set">

						

					</div>

					<div class="col-lg-2  pricing-one border_top_1" id="border_top_1">

					  <ul>

						  <li>250GB every year</li>

                          <li>Single user </li> 

						  <li>$ <b><?php echo $setting->quality_videos; ?></b> per month, Billed annually.</li>

						  <li><span>$<?php echo $setting->plus; ?></span> <span class="p_bottom">per</span> <span class="p_bottom">year.</span></li>

						  <!-- <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown_1"

								role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

							  Enterprise

							</a>

							<div class="dropdown-menu" aria-labelledby="navbarDropdown">

								<a class="dropdown-item" href="services.html">$6  per month, billed annually (save 40%)</a>

								<a class="dropdown-item" href="dep.html">$10 per month, billed monthly</a>

							</div>

						</li> -->

						<li><a href="<?php echo base_url() ?>package/plus" class="pricing_ddr">Get Plus</a></li>

					</ul>

				</div>

				<div class="col-lg-2  pricing-one border_top_2" id="border_top_2">

				  <ul>

					  <li>1TB every year</li>

                      <li>3 team members </li> 

					  <li>$<b> <?php echo $setting->public_grow; ?></b> per month, Billed annually.</li>

					  <li><span>$<?php echo $setting->pro; ?></span> <span class="p_bottom">per</span> <span class="p_bottom">year.</span></li>

					  <li><a href="<?php echo base_url() ?>package/pro" class="bbusiness_set">Get Pro</a></li>

					</ul>

				</div>

				<div class="col-lg-2  pricing-one border_top_3" id="border_top_3">

				  <ul>

					  <li>5TB total storage</li>

					  <li>10 team members</li>
 

					  <li>$ 44 per month, Billed annually.</li>

					  <li><span>$<?php echo $setting->business; ?></span> <span class="p_bottom">per</span> <span class="p_bottom">year.</span></li>

					  <li><a href="<?php echo base_url() ?>package/business" class="bbusiness">Get Business</a></li>

					</ul>

				</div>

				<div class="col-lg-2  pricing-one border_top_4" id="border_top_4">

				  <ul>

					  <li>7TB total storage </li>

					  <li>No weekly limits</li>

                      <li>Unlimited viewers</li> 

					  <li>$ <b><?php echo $setting->streaming; ?></b> per month, Billed annually.</li>

					  <li><span>$<?php echo $setting->premium; ?></span> <span class="p_bottom">per</span> <span class="p_bottom">year.</span></li>

					  <li><a href="<?php echo base_url() ?>package/premium" class="bbusiness_set_last">Get Premium</a></li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4  pricing-set">

					

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Player customization</li>

						<li>Privacy controls</li>

						<li>Social distribution</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Review and approval</li>

						<li>Private team projects</li>

						<li>Customizable Showcase sites</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Player calls-to-action</li>

						<li>Lead generation</li>

						<li>Engagement graphs</li>

						<li>Google Analytics</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited live events</li>

						<li>Live stream to multiple destinations</li>

						<li>Live Q&A, graphics and polls</li>

						<li>Audience chat</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Video Player <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_222343">

				<div class="col-lg-4 pricing-set">

					

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Embed anywhere</li>

						<li>Customize video player <i class="fa fa-angle-right"></i></li>

						<li>Custom end screens</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Embed anywhere</li>

						<li>Customize colors & components</li>

						<li>Customize end-screens</li>

						<li>Add your logo</li>

						<li>Playback speed control</li>

						<li>Third-party player support</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Embed anywhere</li>

						<li>Customize colors & components</li>

						<li>Customize end-screens</li>

						<li>Add your logo</li>

						<li>Playback speed control</li>

						<li>Third-party player support</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Embed anywhere</li>

						<li>Customize colors & components</li>

						<li>Customize end-screens</li>

						<li>Add your logo</li>

						<li>Playback speed control</li>

						<li>Third-party player support</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Privacy <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Collaborations <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Distribution & marketing <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Analytics <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Live streaming <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Priority support <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234 pricing_2223434">

				<div class="col-lg-4 pricing-set">

					<a href="#">Earn money <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>



<?php include('layout/footer.php'); ?>