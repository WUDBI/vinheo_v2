<?php $this->load->view('user/layout/header.php'); ?>
<?php 
$CI = &get_instance();
$CI->load->model('CommonModelAdmin');
$CI->load->model('CommonModel');
$setting = $CI->CommonModelAdmin->getsetting();

if ($this->session->userdata('user_id')) 
{
	$trail_data = $CI->CommonModel->getAll('payments','user_id = "'.$this->session->userdata('user_id').'"   order by id desc limit 1 ');
}

 
?>

<style type="text/css">

  .paypal-button-label-container .paypal-button-text{

    display: none !important;

  }

</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/packages.css">  

	<section class="contact-section padding_red">

		<div class="container">

			<div class="row">

				<div class="col-12">
				 	<div class="login_msg"> </div>

					<?php if ($this->session->flashdata('error')): ?>  
				      	<div class="alert alert-warning alert-dismissible " role="alert"> 
				            <?php echo $this->session->flashdata('error'); ?> 
				            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> 
			                  	<span aria-hidden="true">&times;</span> 
				            </button> 
				      	</div>  
				    <?php endif ?>
 
				    <?php if ($this->session->flashdata('success')): ?> 

				      	<div class="alert alert-success alert-dismissible " role="alert">

				            <?php echo $this->session->flashdata('success'); ?>

				            <button type="button" class="close" data-dismiss="alert" aria-label="Close">

				                  <span aria-hidden="true">&times;</span>

				            </button>

				      	</div>  
				    <?php endif ?>
				</div>

				<div class="col-12 col-lg-7">
					
					<?php if (isset($trail_data) and !empty($trail_data)): ?>
						<h3>Plans Vinheo Pro</h3>
					<p class="p_bottom">$<?php echo $setting->pro; ?> per year.</p>
					<?php else: ?>
						<h3>Plans Vinheo Pro</h3>
					<p class="p_bottom">30 days free trial</p>
					<?php endif; ?>

					<h3>A short summary of what your Plus plan includes:</h3>
					<p><i class="fa fa-check font_tick"></i>Up to 20 GB of space per week to upload videos (Up to 1 TB per year).</p>
 
					 
					<p><i class="fa fa-check font_tick"></i>Tools to review your videos.</p>
					<p><i class="fa fa-check font_tick"></i>3 members to work and share as a team.</p>
					<p><i class="fa fa-check font_tick"></i>Unlimited bandwidth for the VINHEO player.</p>
					<p><i class="fa fa-check font_tick"></i>A / B tests.</p>
					<p><i class="fa fa-check font_tick"></i>Customizable final screens.</p>
					<p  class="p_bottom"><i class="fa fa-check font_tick"></i>Advanced privacy options, insertion and much more.</p>

					<h3>30 day warranty</h3>
					<p  class="p_bottom">Try without obligation, and if you don't feel comfortable, ask for the refund before 30 days, and we will gladly refund your total money.</p>
				</div>
				<div class="col-1 col-sm-1">
					
				</div>
				<?php if(!$this->session->userdata('user_id')): ?>
				 	<div class="col-lg-4  col-12">

				 		<div class="center_me" style="padding-bottom: 0px;">  

				 			<div class="row"> 
				 				<div class="col-12  "> 
									<h2 class="contact-title"><?php echo  lang('Log_in') ?></h2> 

									<p style="margin: 15px 0px">You do not have an account? <a style="color: blue" href="<?php echo base_url() ?>home/signup">Sign up</a></p>
								</div> 
							</div> 
					 		<form  action="<?php echo base_url() ?>home/validate_login" method="post" >

					 			<div class="row">

					 				<div class="col-12  form-group">

					 					<label><?php echo  lang('Email') ?></label>

					 					<input type="email" class="form-control" name="email" placeholder="<?php echo  lang('Email') ?>">

					 					<input type="hidden" class="form-control" name="redirect_true"  value="package/pro">
					 				</div>

					 			</div>



					 			<div class="row">

					 				<div class="col-12 form-group">

					 					<label><?php echo  lang('Password') ?></label>

					 					<input type="password"  class="form-control"  name="password" placeholder="<?php echo  lang('Password') ?>">

					 				</div>

					 			</div>





					 			<div class="row">

					 				<div class="col-12 form-group" style="margin-bottom: 9px;text-align: center;    margin-top: 4%;">
	 

					 					<input type="submit"  value="Login" class="btn btn-primary login_btn" name="valide_login" >

					 				</div>

					 			</div>


					 			<div class="row"> 
					 				<div class="col-12 form-group" style="margin-bottom: 9px;text-align: center; ">  <button class="btn btn-info fb_register"  ><i class="fa fa-facebook"></i> Facebook</button> 
					 				</div> 
					 			</div>

					 			<div class="row"> 
					 				<div class="col-12 form-group" style="margin-bottom: 9px;text-align: center; ">
	 									<div style="display: none;" class="g-signin2" data-onsuccess="onSignIn"></div>

					 				 
					 				</div> 
					 			</div>

					 			<div class="row">

					 				<div class="col-12 form-group" style="text-align: left;    margin-top: 4%;"> 

					 					<a style=" font-weight: 600; text-transform: capitalize;" href="<?php echo base_url(); ?>home/forget_password"><?php echo  lang('Forget_password') ?>?</a>

					 				</div>

					 			</div>

					 		</form>



				 		</div> 
				 	</div>
				<?php else:  
					if (isset($trail_data) and empty($trail_data)): ?>
		               	<div class="col-lg-4  col-12"> 
		                    <div id="pay-invoice" class="card">
		                        <div class="card-body">
		                              <div class="card-title">
		                                   <h2 class="text-center">Get Your Free Trial</h2>
		                              </div>
		                              <hr>
		                              <form action="<?php echo base_url(); ?>package/get_trail" method="post" novalidate="novalidate">

		                                   <input type="hidden" name="item_name" value="pro">
		                                   <input type="hidden" name="amount" value="<?php echo $setting->pro; ?>"> 
		                                   <input type="hidden" name="item_number" value="1" /> 
		                                      

		                                   <div>
		                                        <button id="payment-button" type="submit" class="btn btn-lg btn-success btn-block">
		                                             <i class="fa fa-lock fa-lg"></i>&nbsp;
		                                             <span id="payment-button-amount">Get Trial</span>
		                                             <span id="payment-button-sending" style="display:none;">Sending…</span>
		                                        </button>
		                                   </div>

		                                   <p class="renew_msg">By making the purchase you authorize Vinheo to charge your paypal account and agree to our Terms and Privacy.Your membership will automatically renew.</p>
		                              </form>
		                        </div>
		                    </div>   
		               	</div>
		          <?php else:  
                    $end_date   = $trail_data[0]->end_date;
                    $today_date =  date('Y-m-d');

                    if ($end_date <$today_date) 
                    {
                         ?>  
                         <div class="col-lg-4  col-12"> 
                              <div id="pay-invoice" class="card">
                                  <div class="card-body">
                                        <div class="card-title">
                                             <h2 class="text-center">Payment Details</h2>
                                        </div>
                                        <hr>
                                        <p style="color: red;font-size: 12px !important; margin-bottom: 21px;">You’ll be redirected to PayPal, where you can review your order before you purchase.</p>
                                        <div class="row">
                                             <div class="col-6"><h2>Total</h2></div>
                                             <div class="col-6"><h2 style="text-align: right;">$<?php echo $setting->pro; ?></h2></div>
                                        </div>

                                         

                                        <div id="paypal-button" style="text-align: center; margin-top: 12px;"></div>
                                        <p class="renew_msg">By making the purchase you authorize Vinheo to charge your paypal account and agree to our Terms and Privacy.Your membership will automatically renew.</p> 
                                  </div>
                              </div>   
                         </div>


                         <?php 
                    }else{

                         if ($trail_data[0]->method == 'free') 
                         { 
                              ?>
                              <div class="col-lg-4  col-12" > 
                                   <div id="pay-invoice" class="card" >
                                       <div class="card-body">
                                             <div class="card-title">
                                                  <h2 class="text-center"   >Subscribed</h2>
                                             </div>
                                             <p style="text-align: justify;">
                                                  Your Free Trail will expire on <?php echo date('F d, Y', strtotime($end_date)); ?> and your subscription package is <?php echo ucfirst($trail_data[0]->pkg_name) ?>.
                                             </p>
                                              
                                       </div>
                                   </div>   
                              </div>

                              <?php 
                         }else{
                              ?>
                              <div class="col-lg-4  col-12" > 
                                   	<div id="pay-invoice" class="card" >
                                       	<div class="card-body">
                                         	<div class="card-title">
                                              	<h2 class="text-center"   >Subscribed</h2>
                                         	</div>
                                         	<p style="text-align: justify;">
                                              	Your package will expire on <?php echo date('F d, Y', strtotime($end_date)); ?> and your subscription package is <?php echo ucfirst($trail_data[0]->pkg_name) ?>.
                                         	</p>
                                              
                                       	</div>
                                   </div>   
                              </div>
                              <?php 
                         }
                         ?> 
                         
                         <?php 
                    }  
                	endif;  
                endif; ?>
			</div>

		</div>

	</section>

		



<?php $this->load->view('user/layout/footer.php'); ?>



<script type="text/javascript"> 
	var ajaxURLPath = '<?php echo base_url(); ?>';

		
	$(window).load(function() {
		$('.g-signin2').show();
		$('.abcRioButtonContents').find('span:first-child').html('<i style="    margin-right: 45px;" class="fa fa-google-plus"> Gmail</i> '); 
	});  
</script>
 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/login.js"></script>

<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  paypal.Button.render({
    // Configure environment
    env: 'sandbox',
    client: {
      sandbox: 'AaSm7SshONZtuwmkq-LTmHb061aPQfzlJ0-HufGM_j79J4BZuvzml-LavYfX2QUe_YsOKxlc8cscFqx9'
    },
    // Set up a payment
    payment: function(data, actions) {
      return actions.payment.create({
        transactions: [{
          amount: {
            total: '<?php echo $setting->pro; ?>',
            currency: 'USD'
          }
        }]
      });
    },
    // Execute the payment
    onAuthorize: function(data, actions) {
      return actions.payment.execute().then(function() {

        window.location = "<?php echo base_url(); ?>package/paypal_plus?paymentID="+data.paymentID+"&payerID="+data.payerID+"&token="+data.paymentToken+"&amount=<?php echo $setting->pro; ?>&item_name=pro";

        document.getElementById("response").style.display = 'inline-block';
        document.getElementById("response").innerHTML = 'Thank you for making the payment!';

      });
    }
  }, '#paypal-button'); 

</script>