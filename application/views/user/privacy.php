<?php include('layout/header.php'); ?>
		
		<section class="privacy_sset">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div id="legal_doc" class="legal_doc">
							<h1>Vinheo Terms of Service Agreement</h1>
							<p>Last Updated: March 12, 2020</p>
							 


							<h2>Privacy policy of 	<a href="<?php echo base_url(); ?>">Vinheo</a>  </h2> 
							 
							<p>This Privacy Policy establishes the terms in which Vinheo uses and protects the information that is provided by its users when using its website. This company is committed to the security of its users' data. When we ask you to fill in the personal information fields with which you can be identified, we do so by ensuring that it will only be used in accordance with the terms of this document. However, this Privacy Policy may change over time or be updated so we recommend and emphasize continually reviewing this page to ensure you agree with those changes.</p>


							<h2> Information that is collected</h2>
							<p>Our website may collect personal information such as: Name, contact information such as your email address and demographic information. Also, when necessary, specific information may be required to process an order or make a delivery or billing.</p>


							<h2> Use of collected information</h2>
							<p>Our website uses the information in order to provide the best possible service, particularly to maintain a user registry, of orders if applicable, and to improve our products and services. E-mails may be sent periodically through our site with special offers, new products and other advertising information that we consider relevant to you or that may provide you with some benefit, these emails will be sent to the address you provide and may be canceled anytime.</p>

							<P><strong>Vinheo</strong> is highly committed to fulfill the commitment to keep your information secure. We use the most advanced systems and constantly update them to ensure that there is no unauthorized access.</P>

							<h2>Cookies</h2>

							<p>A cookie refers to a file that is sent for the purpose of requesting permission to be stored on your computer, by accepting said file it is created and the cookie then serves to have information regarding web traffic, and also facilitates future visits to a web recurrent. Another function that cookies have is that with them the websites can recognize you individually and therefore provide you with the best personalized service on your website.</p>


							<P>Our website uses cookies to identify the pages that are visited and their frequency. This information is used only for statistical analysis and then the information is permanently deleted. You can delete cookies at any time from your computer. However, cookies help to provide a better service for websites, they do not give access to information from your computer or from you, unless you want it and provide it directly, visits to a website. You can accept or deny the use of cookies, however most browsers accept cookies automatically as it serves to have a better web service. You can also change your computer settings to decline cookies. If they decline, you may not be able to use some of our services.</P>

							<h2>Third Party Links</h2>

							<p>This website may contain links to other sites that may be of interest to you. Once you click on these links and leave our page, we no longer have control over the site to which you are redirected and therefore we are not responsible for the terms or privacy or the protection of your data on those other third-party sites. These sites are subject to their own privacy policies, so it is recommended that you consult them to confirm that you agree with them.</p>

							<h2>Control of your personal information</h2>
							<p>At any time you may restrict the collection or use of personal information that is provided to our website. Each time you are asked to fill out a form, such as user registration, you can check or uncheck the option to receive information by email. In case you have marked the option to receive our newsletter or advertising you can cancel it at any time.</p>

							<p>This company will not sell, assign or distribute personal information that is collected without your consent, unless required by a judge with a court order.</p>

							<p><strong>Vinheo</strong> reserves the right to change the terms of this Privacy Policy at any time.</p>
 
						</div>
					</div>
				</div>
			</div>
		</section>



<?php include('layout/footer.php'); ?>