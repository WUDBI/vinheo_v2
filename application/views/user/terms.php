<?php include('layout/header.php'); ?>

<style type="text/css">
	.legal_doc p{
		text-align: justify;
	}
</style>	
		<section class="privacy_sset">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div id="legal_doc" class="legal_doc">
							<h1 style="text-align: center;border: none;padding-bottom: 57px;">Terms and conditions of use</h1>
							 
							<h2>RELEVANT INFORMATION</h2>
						 	

						 	<p>It is a necessary requirement for the acquisition of the products offered on this site, that you read and accept the following Terms and Conditions that are written below. The use of our services as well as the purchase of our products will imply that you have read and accepted the Terms and Conditions of Use in this document. All the products that are offered by our website could be created, collected, sent or presented by a third web page and in that case they would be subject to their own Terms and Conditions. In some cases, to acquire a product, registration by the user will be necessary, with the entry of reliable personal data and definition of a password.</p>

						 	<p>The user can choose and change the password for his account administration access at any time, in case he has registered and is necessary for the purchase of any of our products. https://vinheo.com does not assume responsibility in the event that you deliver this key to third parties.</p>

						 	<p>All purchases and transactions carried out through this website are subject to a confirmation and verification process, which could include the verification of the stock and product availability, validation of the payment method, validation of the invoice (if any) and compliance with the conditions required by the selected payment method. In some cases, verification by email may be required.</p>

						 	<p>The prices of the products offered in this Online Store is valid only on purchases made on this website.</p>

						 	<h2>LICENSE</h2>

						 	<p>Vinheo through its website grants a license for users to use the products that are sold on this website according to the Terms and Conditions described in this document.</p>

						 	<h2>UNAUTHORIZED USE</h2>

						 	<p>If applicable (for sale of software, temperaments, or other design and programming product) you cannot place one of our products, modified or unmodified, on a CD, website or any other means and offer them for redistribution or resale of any kind.</p>

						 	<h2>PROPERTY</h2>

						 	<p>You cannot declare intellectual or exclusive property to any of our products, modified or unmodified. All products are property of the content providers. In the event that the contrary is not specified, our products are provided without any warranty, express or implied. Under no circumstances will this company be liable for any damage including, but not limited to, direct, indirect, special, incidental or consequential damages or other losses resulting from the use or inability to use our products.</p>

						 	<h2>REFUND AND GUARANTEE POLICY</h2>

						 	<p>In the case of products that are irrevocable non-tangible goods, we do not make refunds after the product is sent, you have the responsibility to understand before buying it. We ask that you read carefully before buying it. We only make exceptions with this rule when the description does not fit the product. There are some products that could have a guarantee and possibility of reimbursement but this will be specified when you buy the product. In such cases the warranty will only cover factory faults and will only be effective when the product has been used correctly. The warranty does not cover breakdowns or damage caused by misuse. The terms of the warranty are associated with manufacturing and operation failures under normal product conditions and these terms will only become effective if the equipment has been used correctly. This includes:</p>

						 	<ul style="list-style-type: circle;">
						 		<li><p>According to the technical specifications indicated for each product.</p></li>

						 		<li><p>Under environmental conditions in accordance with the specifications indicated by the manufacturer.</p></li>

						 		<li><p>In specific use for the function with which it was designed at the factory.</p></li>

						 		<li><p>Under electrical operating conditions in accordance with the specifications and tolerances indicated.</p></li>
						 	</ul>


						 	<h2>ANTI-FRAUD CHECK</h2>

						 	<p>The customer's purchase can be postponed for the anti-fraud check. It can also be suspended for longer for a more rigorous investigation, to avoid fraudulent transactions.</p>

						 	<h2>PRIVACIDAD</h2>

						 	<p>This website https://vinheo.com guarantees that the personal information you send has the necessary security. The data entered by the user or in the case of requiring a validation of the orders will not be delivered to third parties, unless it must be disclosed in compliance with a court order or legal requirements.</p>

						 	<p>Subscription to advertising email newsletters is voluntary and could be selected when creating your account.</p>

						 	<p>Vinheo reserves the rights to change or modify these terms without prior notice.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

<?php include('layout/footer.php'); ?>