<?php $this->load->view('user/layout/header'); 
function make_links_clickable($text){
    return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a target="_blank" href="$1">$1</a>', $text);
}

?>
<style type="text/css">
   .single-post-video-full-width-wrapper {
        /*background-color: transparent; */
       padding-bottom: 30px;
   }
</style>
<section class="contact-section  " style="padding: ">
	<!-- <div class="manage_video">
		<h2>Mange your videos</h2>
	</div> -->
	<div class="single-post-video-full-width-wrapper dark-background overlay-background is_vid_playlist_layout">
         <div class="full-width-breadcrumbs">
            <div class="site__container fullwidth-vidorev-ctrl">
               <div class="site__row nav-breadcrumbs-elm">
                  <div class="site__col"></div>
               </div>
            </div>
         </div>
         <div class="single-post-video-player-content">
            <div class="site__container fullwidth-vidorev-ctrl">
               <div class="site__row">
                  <div class="site__col">
                     <div class="site__row playlist-frame playlist-frame-control">
                        <div class="site__col player-in-playlist" style="width: 100%;">
                           <div class="single-player-video-wrapper vp-small-item">
                              <div class="light-off light-off-control"></div>
                              <div id="video-player-wrap-control" class="video-player-wrap">
                                 <div class="video-player-ratio"></div>
                                 <div class="video-player-content video-player-control">
                                    <div class="float-video-title">
                                       <h6>
                                          <?php if (!empty($video[0]->title)): ?>
                                             <?php echo $video[0]->title; ?>
                                          <?php else: ?>
                                             <?php echo $video[0]->video_name; ?>
                                          <?php endif; ?>
                                           
                                       </h6>
                                    </div>
                                    <a href="#" title="Close" class="close-floating-video close-floating-video-control"><i class="fa fa-times" aria-hidden="true"></i></a> <a href="#" title="Scroll Up" class="scroll-up-floating-video scroll-up-floating-video-control"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></a>
                                    <div class="player-3rdparty player-3rdparty-control  player-loaded">
                                        
                                       
                                       <video  poster="https://s3.eu-central-1.amazonaws.com/pipe.public.content/poster.png"  playsinline    src="<?php echo base_url(); ?>assets/work_files/<?php echo $video[0]->user_id; ?>/<?php echo $video[0]->video_name; ?>"  preload="none"  style="width: 100%;height: 100%;" controls    controlsList="nofullscreen nodownload"></video>
                                     
                                       
                                       <div class="autoplay-off-elm autoplay-off-elm-control video-play-control" data-id="64" data-background-url="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir-1500x844.jpg"> <span class="video-icon big-icon video-play-control" data-id="64"></span> <img class="poster-preload" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir-1500x844.jpg" alt="Preload Image"></div>
                                       <div class="player-muted ads-mute-elm ads-muted-control"><i class="fa fa-volume-off" aria-hidden="true"></i></div>
                                       <div class="auto-next-elm auto-next-elm-control dark-background" data-background-url="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir-1500x844.jpg" data-next-url="http://demo.beeteam368.com/vidorev/only-in-battlefield-4-anthem-tv-trailer/?playlist=209">
                                          <div class="auto-next-content">
                                             <div class="up-next-text font-size-12">Up next</div>
                                             <h3 class="h6-mobile video-next-title video-next-title-control"><?php if (!empty($video[0]->title)): ?> <?php echo $video[0]->title; ?> <?php else: ?> <?php echo $video[0]->video_name; ?> <?php endif; ?></h3>
                                             <div class="loader-timer-wrapper loader-timer-control">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" class="loader-timer">
                                                   <circle class="progress-timer" fill="none" stroke-linecap="round" cx="20" cy="20" r="15.915494309"></circle>
                                                </svg>
                                                <i class="fa fa-fast-forward" aria-hidden="true"></i>
                                             </div>
                                             <a href="#" class="basic-button basic-button-default cancel-btn cancel-btn-control">Cancel</a>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="video-loading video-loading-control"> <span class="video-load-icon"></span></div>
                                 </div>
                              </div>
                              <div class="video-toolbar dark-background video-toolbar-control">
                                 <div class="tb-left">
                                    <div class="site__row">
                                       
                                       <div class="site__col toolbar-item">
                                          <div class="toolbar-item-content like-action-control " data-id="64" data-action="like"> <span class="like-tooltip like-tooltip-control"><span class="likethis">I Like This</span><span class="unlike">Unlike</span></span> <span class="item-icon font-size-18"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span><span class="item-text">Like</span> <span class="video-load-icon small-icon"></span> <span class="login-tooltip login-req"><span>Please Login to Vote</span></span></div>
                                       </div>
                                       <div class="site__col toolbar-item">
                                          <div class="toolbar-item-content like-action-control " data-id="64" data-action="dislike"> <span class="dislike-tooltip dislike-tooltip-control"><span class="dislikethis">I Dislike This</span><span class="undislike">Un-Dislike</span></span> <span class="item-icon font-size-18"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span><span class="item-text">Dislike</span> <span class="video-load-icon small-icon"></span> <span class="login-tooltip login-req"><span>Please Login to Vote</span></span></div>
                                       </div>
                                      
                                    </div>
                                 </div>
                                 <div class="tb-right">
                                    <div class="site__row">
                                      
                                       <div class="site__col toolbar-item">
                                          <div class="toolbar-item-content comment-video-control scroll-elm-control" data-href="#comments"> <span class="item-text">0 Comments</span><span class="item-icon font-size-18"><i class="fa fa-comment" aria-hidden="true"></i></span></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="social-share-toolbar social-share-toolbar-control">
                                 <div class="social-share-toolbar-content">
                                    <ul class="social-block s-grid big-icon">
                                       <li class="facebook-link"> <a href="//facebook.com/sharer/sharer.php?u=http://demo.beeteam368.com/vidorev/battlefield-hardline-into-the-jungle/" data-share="on" data-source="facebook" target="_blank" title="Share on Facebook"> <span class="icon"><i class="fa fa-facebook"></i></span> </a></li>
                                       <li class="twitter-link"> <a href="//twitter.com/share?text=Battlefield%20Hardline:%20Into%20the%20Jungle&amp;url=http://demo.beeteam368.com/vidorev/battlefield-hardline-into-the-jungle/" data-share="on" data-source="twitter" target="_blank" title="Share on Twitter"> <span class="icon"><i class="fa fa-twitter"></i></span> </a></li>
                                       <li class="google-plus-link"> <a href="//plus.google.com/share?url=http://demo.beeteam368.com/vidorev/battlefield-hardline-into-the-jungle/" target="_blank" data-share="on" data-source="googleplus" title="Share on Google Plus"> <span class="icon"><i class="fa fa-google-plus"></i></span> </a></li>
                                       <li class="whatsapp-link"> <a href="whatsapp://send?text=Battlefield Hardline: Into the Jungle http://demo.beeteam368.com/vidorev/battlefield-hardline-into-the-jungle/" target="_blank" data-share="on" data-source="whatsapp" data-action="share/whatsapp/share" title="Share on WhatsApp"> <span class="icon"><i class="fa fa-whatsapp"></i></span> </a></li>
                                       <li class="linkedin-link"> <a href="//linkedin.com/shareArticle?mini=true&amp;url=http://demo.beeteam368.com/vidorev/battlefield-hardline-into-the-jungle/&amp;title=Battlefield%20Hardline:%20Into%20the%20Jungle&amp;source=VidoRev" target="_blank" data-share="on" data-source="linkedin" title="Share on LinkedIn"> <span class="icon"><i class="fa fa-linkedin"></i></span> </a></li>
                                       <li class="pinterest-link"> <a href="//pinterest.com/pin/create/button/?url=http://demo.beeteam368.com/vidorev/battlefield-hardline-into-the-jungle/&amp;media=http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir.jpg&amp;description=Battlefield%20Hardline:%20Into%20the%20Jungle" data-share="on" data-source="pinterest" target="_blank" title="Pin this"> <span class="icon"><i class="fa fa-pinterest"></i></span> </a></li>
                                    </ul>
                                    <input type="text" onclick="this.select()" class="share-iframe-embed" readonly="" value="<iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;http://demo.beeteam368.com/vidorev/battlefield-hardline-into-the-jungle/?video_embed=64&quot; frameborder=&quot;0&quot; allow=&quot;accelerometer; encrypted-media; gyroscope; picture-in-picture&quot; allowfullscreen></iframe>">
                                 </div>
                              </div>
                              <div class="video-sub-toolbar">
                                 <div class="tb-left">
                                    <div class="site__row">
                                       <div class="site__col toolbar-item">
                                          <div class="toolbar-item-content view-like-information">
                                             <div class="view-count h4"> 0 Views</div>
                                             <div class="like-dislike-bar"> 
                                                <span class="like-dislike-bar-percent-control" style="width:89.531023368251%"></span>
                                             </div>
                                             <div class="like-dislike-infor">
                                                <div class="like-number"><span class="block-icon"><i class="fa fa-thumbs-up" aria-hidden="true"></i></span><span class="like-count-full" data-id="64">0</span></div>
                                                <div class="dislike-number"><span class="block-icon"><i class="fa fa-thumbs-down" aria-hidden="true"></i></span><span class="dislike-count-full" data-id="64">0</span></div>
                                                <span></span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="tb-right">
                                    <div class="site__row">
                                        
                                      
                                       <div class="site__col toolbar-item">
                                          <div class="toolbar-item-content"> <a href="http://demo.beeteam368.com/vidorev/only-in-battlefield-4-how-to-blow-up-an-elevator/?playlist=209" class="item-button prev-video"> <span class="subtollbartolltip"><span class="repeatthis">Previous Video</span></span> <span class="item-icon"><i class="fa fa-fast-backward" aria-hidden="true"></i></span><span class="item-text">Prev</span> </a></div>
                                       </div>
                                       <div class="site__col toolbar-item">
                                          <div class="toolbar-item-content"> <a href="http://demo.beeteam368.com/vidorev/only-in-battlefield-4-anthem-tv-trailer/?playlist=209" class="item-button next-video"> <span class="subtollbartolltip"><span class="repeatthis">Next Video</span></span> <span class="item-text">Next</span><span class="item-icon"><i class="fa fa-fast-forward" aria-hidden="true"></i></span> </a></div>
                                       </div>
                                       <?php if (isset($video[0]->download) and  $video[0]->download =='allowed'): ?> 
                                           
                                          <div class="site__col toolbar-item">
                                             <a href="<?php echo base_url(); ?>assets/work_files/<?php echo $video[0]->user_id; ?>/<?php echo $video[0]->video_name; ?>" download>
                                             <div class="toolbar-item-content"> <span class="item-button more-videos more-videos-control" style="background-color: #0EF91E;"> <span class="subtollbartolltip"><span class="repeatthis">Download</span></span> <span class="item-text">Download</span><span class="item-icon"><i class="fa fa-download" aria-hidden="true"></i></span> </span></div></a>
                                          </div>
                                       <?php endif; ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="member-playlist-lightbox dark-background">
                                 <div class="playlist-lightbox-listing">
                                    <div class="playlist-close playlist-items-control"> <i class="fa fa-times" aria-hidden="true"></i></div>
                                    <div class="ajax-playlist-list ajax-playlist-list-control">
                                       <span class="loadmore-loading playlist-loading-control">
                                          <span class="loadmore-indicator">
                                             <svg>
                                                <polyline id="back" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
                                                <polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
                                             </svg>
                                          </span>
                                       </span>
                                    </div>
                                    
                                 </div>
                              </div>
                              <div class="show-more-videos show-more-videos-control">
                                 <div class="blog-wrapper global-blog-wrapper blog-wrapper-control mCustomScrollbar _mCS_1">
                                    <div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_horizontal mCSB_inside" style="max-height: none;" tabindex="0">
                                       <div id="mCSB_1_container" class="mCSB_container" style="position: relative; top: 0px; left: 0px; width: 2740px;" dir="ltr">
                                          <div class="blog-items blog-items-control site__row grid-small">
                                             <article class="post-item site__col post-438 post type-post status-publish format-video has-post-thumbnail hentry category-game tag-153 tag-20th-century-fox tag-action tag-feature-film tag-mono tag-united-states post_format-post-format-video pmpro-has-access">
                                                <div class="post-item-wrap">
                                                   <div class="blog-pic">
                                                      <div class="blog-pic-wrap wrap_preview wrap_preview_control preview-df-video">
                                                         <a data-post-id="438" href="http://demo.beeteam368.com/vidorev/battlefield-hardline-beta-trailer-complete-fps-experience-gameplay/?playlist=209" title="Battlefield Hardline Beta Trailer" class="blog-img">
                                                            <img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-236x133.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-236x133.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW.jpg 1920w" data-sizes="(max-width: 236px) 100vw, 236px" alt="battlefield-4-ddx_pdp_3840x2160_en_WW" sizes="(max-width: 236px) 100vw, 236px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW.jpg 1920w"><span class="ul-placeholder-bg class-16x9"></span>
                                                            <div class="preview-video preview-video-control" data-iframepreview="http://demo.beeteam368.com/vidorev/battlefield-hardline-beta-trailer-complete-fps-experience-gameplay/?video_embed=438&amp;preview_mode=1"></div>
                                                         </a>
                                                         <span class="video-icon video-popup-control" data-id="438"></span><span class="watch-later-icon watch-later-control" data-id="438" data-img-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/battlefield-4-ddx_pdp_3840x2160_en_WW-150x150.jpg" data-hyperlink="http://demo.beeteam368.com/vidorev/battlefield-hardline-beta-trailer-complete-fps-experience-gameplay/" data-title="Battlefield Hardline Beta Trailer"> <i class="fa fa-clock-o" aria-hidden="true"></i><span class="watch-text font-size-12">Watch Later</span><span class="watch-remove-text font-size-12">Added</span> </span><span class="duration-text font-size-12 meta-font">05:40</span>
                                                      </div>
                                                   </div>
                                                   <div class="listing-content">
                                                      <h3 class="entry-title h6 post-title"> <a href="http://demo.beeteam368.com/vidorev/battlefield-hardline-beta-trailer-complete-fps-experience-gameplay/?playlist=209" title="Battlefield Hardline Beta Trailer">Battlefield Hardline Beta Trailer</a></h3>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="http://demo.beeteam368.com/vidorev/members/admin/">Nicolas</a></div>
                                                            <div class="date-time"><i class="fa fa-calendar" aria-hidden="true"></i><span><time class="entry-date published" datetime="2018-01-17T21:57:56+07:00">January 17, 2018</time><time class="updated" datetime="2019-09-09T14:37:39+07:00">September 9, 2019</time></span></div>
                                                         </div>
                                                      </div>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="comment-count"><i class="fa fa-comment" aria-hidden="true"></i><span>0</span></div>
                                                            <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>1M</span></div>
                                                            <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="438">18K</span></div>
                                                            <div class="dislike-count"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="dislike-count" data-id="438">1.1K</span></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </article>
                                             <article class="post-item site__col post-62 post type-post status-publish format-video has-post-thumbnail hentry category-game tag-151 tag-adventure tag-silent tag-sony tag-tv-movie tag-united-kingdom post_format-post-format-video pmpro-has-access">
                                                <div class="post-item-wrap">
                                                   <div class="blog-pic">
                                                      <div class="blog-pic-wrap wrap_preview wrap_preview_control preview-df-video">
                                                         <a data-post-id="62" href="http://demo.beeteam368.com/vidorev/battlefield-hardline-official-launch-gameplay-trailer-teaser/?playlist=209" title="Battlefield Hardline: Official Launch Gameplay Trailer Teaser" class="blog-img">
                                                            <img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-236x133.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-236x133.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g.jpg 1920w" data-sizes="(max-width: 236px) 100vw, 236px" alt="Need for Speed Movie – Pulse Big Game Spot" sizes="(max-width: 236px) 100vw, 236px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g.jpg 1920w"><span class="ul-placeholder-bg class-16x9"></span>
                                                            <div class="preview-video preview-video-control" data-iframepreview="http://demo.beeteam368.com/vidorev/battlefield-hardline-official-launch-gameplay-trailer-teaser/?video_embed=62&amp;preview_mode=1"></div>
                                                         </a>
                                                         <span class="video-icon video-popup-control" data-id="62"></span><span class="watch-later-icon watch-later-control" data-id="62" data-img-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-movie-pulse-big-g-150x150.jpg" data-hyperlink="http://demo.beeteam368.com/vidorev/battlefield-hardline-official-launch-gameplay-trailer-teaser/" data-title="Battlefield Hardline: Official Launch Gameplay Trailer Teaser"> <i class="fa fa-clock-o" aria-hidden="true"></i><span class="watch-text font-size-12">Watch Later</span><span class="watch-remove-text font-size-12">Added</span> </span><span class="duration-text font-size-12 meta-font">00:31</span>
                                                      </div>
                                                   </div>
                                                   <div class="listing-content">
                                                      <h3 class="entry-title h6 post-title"> <a href="http://demo.beeteam368.com/vidorev/battlefield-hardline-official-launch-gameplay-trailer-teaser/?playlist=209" title="Battlefield Hardline: Official Launch Gameplay Trailer Teaser">Battlefield Hardline: Official Launch Gameplay Trailer Teaser</a></h3>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="http://demo.beeteam368.com/vidorev/members/admin/">Nicolas</a></div>
                                                            <div class="date-time"><i class="fa fa-calendar" aria-hidden="true"></i><span><time class="entry-date published" datetime="2018-01-17T18:48:14+07:00">January 17, 2018</time><time class="updated" datetime="2019-09-09T14:40:23+07:00">September 9, 2019</time></span></div>
                                                         </div>
                                                      </div>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="comment-count"><i class="fa fa-comment" aria-hidden="true"></i><span>0</span></div>
                                                            <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>12K</span></div>
                                                            <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="62">103</span></div>
                                                            <div class="dislike-count"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="dislike-count" data-id="62">8</span></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </article>
                                             <article class="post-item site__col post-28 post type-post status-publish format-video has-post-thumbnail hentry category-game tag-151 tag-documentary tag-feature-film tag-mono tag-united-states tag-walt-disney post_format-post-format-video pmpro-has-access">
                                                <div class="post-item-wrap">
                                                   <div class="blog-pic">
                                                      <div class="blog-pic-wrap wrap_preview wrap_preview_control preview-df-video">
                                                         <a data-post-id="28" href="http://demo.beeteam368.com/vidorev/mcm-comic-con-league-of-legends-cosplay/?playlist=209" title="MCM Comic Con – League of Legends Cosplay" class="blog-img">
                                                            <img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-236x133.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-236x133.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook.jpg 1920w" data-sizes="(max-width: 236px) 100vw, 236px" alt="FACEBOOK" sizes="(max-width: 236px) 100vw, 236px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook.jpg 1920w"><span class="ul-placeholder-bg class-16x9"></span>
                                                            <div class="preview-video preview-video-control" data-iframepreview="http://demo.beeteam368.com/vidorev/mcm-comic-con-league-of-legends-cosplay/?video_embed=28&amp;preview_mode=1"></div>
                                                         </a>
                                                         <span class="video-icon video-popup-control" data-id="28"></span><span class="watch-later-icon watch-later-control" data-id="28" data-img-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/facebook-150x150.jpg" data-hyperlink="http://demo.beeteam368.com/vidorev/mcm-comic-con-league-of-legends-cosplay/" data-title="MCM Comic Con – League of Legends Cosplay"> <i class="fa fa-clock-o" aria-hidden="true"></i><span class="watch-text font-size-12">Watch Later</span><span class="watch-remove-text font-size-12">Added</span> </span><span class="duration-text font-size-12 meta-font">00:01:49</span>
                                                      </div>
                                                   </div>
                                                   <div class="listing-content">
                                                      <h3 class="entry-title h6 post-title"> <a href="http://demo.beeteam368.com/vidorev/mcm-comic-con-league-of-legends-cosplay/?playlist=209" title="MCM Comic Con – League of Legends Cosplay">MCM Comic Con – League of Legends Cosplay</a></h3>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="http://demo.beeteam368.com/vidorev/members/admin/">Nicolas</a></div>
                                                            <div class="date-time"><i class="fa fa-calendar" aria-hidden="true"></i><span><time class="entry-date published" datetime="2018-01-15T03:43:33+07:00">January 15, 2018</time><time class="updated" datetime="2019-09-09T14:46:19+07:00">September 9, 2019</time></span></div>
                                                         </div>
                                                      </div>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="comment-count"><i class="fa fa-comment" aria-hidden="true"></i><span>0</span></div>
                                                            <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>1.6K</span></div>
                                                            <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="28">29.2K</span></div>
                                                            <div class="dislike-count"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="dislike-count" data-id="28">0</span></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </article>
                                             <article class="post-item site__col post-430 post type-post status-publish format-video has-post-thumbnail hentry category-game tag-151 tag-feature-film tag-history tag-mono tag-paramount tag-united-states post_format-post-format-video pmpro-has-access">
                                                <div class="post-item-wrap">
                                                   <div class="blog-pic">
                                                      <div class="blog-pic-wrap wrap_preview wrap_preview_control preview-df-video">
                                                         <a data-post-id="430" href="http://demo.beeteam368.com/vidorev/borgore-plays-gta-online-live-with-typical-gamer-rockstar/?playlist=209" title="Borgore Plays GTA Online Live – with Typical Gamer &amp; Rockstar" class="blog-img">
                                                            <img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-236x133.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-236x133.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2.jpg 1920w" data-sizes="(max-width: 236px) 100vw, 236px" alt="Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2" sizes="(max-width: 236px) 100vw, 236px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2.jpg 1920w"><span class="ul-placeholder-bg class-16x9"></span>
                                                            <div class="preview-video preview-video-control" data-iframepreview="http://demo.beeteam368.com/vidorev/borgore-plays-gta-online-live-with-typical-gamer-rockstar/?video_embed=430&amp;preview_mode=1"></div>
                                                         </a>
                                                         <span class="video-icon video-popup-control" data-id="430"></span><span class="watch-later-icon watch-later-control" data-id="430" data-img-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/Download-Now-GeForce-Game-Ready-Driver-for-Grand-Theft-Auto-V-478276-2-150x150.jpg" data-hyperlink="http://demo.beeteam368.com/vidorev/borgore-plays-gta-online-live-with-typical-gamer-rockstar/" data-title="Borgore Plays GTA Online Live – with Typical Gamer &amp; Rockstar"> <i class="fa fa-clock-o" aria-hidden="true"></i><span class="watch-text font-size-12">Watch Later</span><span class="watch-remove-text font-size-12">Added</span> </span><span class="duration-text font-size-12 meta-font">01:29:04</span>
                                                      </div>
                                                   </div>
                                                   <div class="listing-content">
                                                      <h3 class="entry-title h6 post-title"> <a href="http://demo.beeteam368.com/vidorev/borgore-plays-gta-online-live-with-typical-gamer-rockstar/?playlist=209" title="Borgore Plays GTA Online Live – with Typical Gamer &amp; Rockstar">Borgore Plays GTA Online Live – with Typical Gamer &amp; Rockstar</a></h3>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="http://demo.beeteam368.com/vidorev/members/admin/">Nicolas</a></div>
                                                            <div class="date-time"><i class="fa fa-calendar" aria-hidden="true"></i><span><time class="entry-date published" datetime="2018-01-12T16:34:49+07:00">January 12, 2018</time><time class="updated" datetime="2019-09-09T14:53:17+07:00">September 9, 2019</time></span></div>
                                                         </div>
                                                      </div>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="comment-count"><i class="fa fa-comment" aria-hidden="true"></i><span>0</span></div>
                                                            <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>83.8K</span></div>
                                                            <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="430">3.5K</span></div>
                                                            <div class="dislike-count"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="dislike-count" data-id="430">381</span></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </article>
                                             <article class="post-item site__col post-426 post type-post status-publish format-video has-post-thumbnail hentry category-game tag-153 tag-drama tag-silent tag-tv-movie tag-united-kingdom tag-warner-bros post_format-post-format-video pmpro-has-access">
                                                <div class="post-item-wrap">
                                                   <div class="blog-pic">
                                                      <div class="blog-pic-wrap wrap_preview wrap_preview_control preview-df-video">
                                                         <a data-post-id="426" href="http://demo.beeteam368.com/vidorev/unravel-official-gamescom-gameplay-trailer/?playlist=209" title="Unravel: Official Gamescom Gameplay Trailer" class="blog-img">
                                                            <img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-236x133.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-236x133.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7.jpg 1920w" data-sizes="(max-width: 236px) 100vw, 236px" alt="SS_7" sizes="(max-width: 236px) 100vw, 236px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7.jpg 1920w"><span class="ul-placeholder-bg class-16x9"></span>
                                                            <div class="preview-video preview-video-control" data-iframepreview="http://demo.beeteam368.com/vidorev/unravel-official-gamescom-gameplay-trailer/?video_embed=426&amp;preview_mode=1"></div>
                                                         </a>
                                                         <span class="video-icon video-popup-control" data-id="426"></span><span class="watch-later-icon watch-later-control" data-id="426" data-img-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2018/01/SS_7-150x150.jpg" data-hyperlink="http://demo.beeteam368.com/vidorev/unravel-official-gamescom-gameplay-trailer/" data-title="Unravel: Official Gamescom Gameplay Trailer"> <i class="fa fa-clock-o" aria-hidden="true"></i><span class="watch-text font-size-12">Watch Later</span><span class="watch-remove-text font-size-12">Added</span> </span><span class="duration-text font-size-12 meta-font">03:35</span>
                                                      </div>
                                                   </div>
                                                   <div class="listing-content">
                                                      <h3 class="entry-title h6 post-title"> <a href="http://demo.beeteam368.com/vidorev/unravel-official-gamescom-gameplay-trailer/?playlist=209" title="Unravel: Official Gamescom Gameplay Trailer">Unravel: Official Gamescom Gameplay Trailer</a></h3>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="http://demo.beeteam368.com/vidorev/members/admin/">Nicolas</a></div>
                                                            <div class="date-time"><i class="fa fa-calendar" aria-hidden="true"></i><span><time class="entry-date published" datetime="2018-01-02T08:48:37+07:00">January 2, 2018</time><time class="updated" datetime="2019-09-09T15:05:20+07:00">September 9, 2019</time></span></div>
                                                         </div>
                                                      </div>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="comment-count"><i class="fa fa-comment" aria-hidden="true"></i><span>0</span></div>
                                                            <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>259K</span></div>
                                                            <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="426">2.9K</span></div>
                                                            <div class="dislike-count"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="dislike-count" data-id="426">53</span></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </article>
                                             <article class="post-item site__col post-418 post type-post status-publish format-video has-post-thumbnail hentry category-game tag-153 tag-dolby-digital tag-fantasy tag-india tag-sony tag-tv-episode post_format-post-format-video pmpro-has-access">
                                                <div class="post-item-wrap">
                                                   <div class="blog-pic">
                                                      <div class="blog-pic-wrap wrap_preview wrap_preview_control preview-df-video">
                                                         <a data-post-id="418" href="http://demo.beeteam368.com/vidorev/need-for-speed-payback-welcome-to-fortune-valley/?playlist=209" title="Need for Speed Payback Welcome to Fortune Valley" class="blog-img">
                                                            <img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-236x133.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-236x133.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW.jpg 1920w" data-sizes="(max-width: 236px) 100vw, 236px" alt="need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW" sizes="(max-width: 236px) 100vw, 236px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW.jpg 1920w"><span class="ul-placeholder-bg class-16x9"></span>
                                                            <div class="preview-video preview-video-control" data-iframepreview="http://demo.beeteam368.com/vidorev/need-for-speed-payback-welcome-to-fortune-valley/?video_embed=418&amp;preview_mode=1"></div>
                                                         </a>
                                                         <span class="video-icon video-popup-control" data-id="418"></span><span class="watch-later-icon watch-later-control" data-id="418" data-img-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-shift-standard-edition_pdp_1920x1080_en_WW-150x150.jpg" data-hyperlink="http://demo.beeteam368.com/vidorev/need-for-speed-payback-welcome-to-fortune-valley/" data-title="Need for Speed Payback Welcome to Fortune Valley"> <i class="fa fa-clock-o" aria-hidden="true"></i><span class="watch-text font-size-12">Watch Later</span><span class="watch-remove-text font-size-12">Added</span> </span><span class="duration-text font-size-12 meta-font">02:26</span>
                                                      </div>
                                                   </div>
                                                   <div class="listing-content">
                                                      <h3 class="entry-title h6 post-title"> <a href="http://demo.beeteam368.com/vidorev/need-for-speed-payback-welcome-to-fortune-valley/?playlist=209" title="Need for Speed Payback Welcome to Fortune Valley">Need for Speed Payback Welcome to Fortune Valley</a></h3>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="http://demo.beeteam368.com/vidorev/members/admin/">Nicolas</a></div>
                                                            <div class="date-time"><i class="fa fa-calendar" aria-hidden="true"></i><span><time class="entry-date published" datetime="2017-12-27T22:48:30+07:00">December 27, 2017</time><time class="updated" datetime="2019-09-09T15:05:39+07:00">September 9, 2019</time></span></div>
                                                         </div>
                                                      </div>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="comment-count"><i class="fa fa-comment" aria-hidden="true"></i><span>0</span></div>
                                                            <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>2.3M</span></div>
                                                            <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="418">44K</span></div>
                                                            <div class="dislike-count"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="dislike-count" data-id="418">1.2K</span></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </article>
                                             <article class="post-item site__col post-71 post type-post status-publish format-video has-post-thumbnail hentry category-game tag-151 tag-feature-film tag-history tag-mono tag-paramount tag-united-states post_format-post-format-video pmpro-has-access">
                                                <div class="post-item-wrap">
                                                   <div class="blog-pic">
                                                      <div class="blog-pic-wrap wrap_preview wrap_preview_control preview-df-video">
                                                         <a data-post-id="71" href="http://demo.beeteam368.com/vidorev/only-in-battlefield-4-c4-house-party/?playlist=209" title="Only in Battlefield 4: C4 House Party" class="blog-img">
                                                            <img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-236x133.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-236x133.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l.jpg 1920w" data-sizes="(max-width: 236px) 100vw, 236px" alt="Only In Battlefield 4: Kill the Lights" sizes="(max-width: 236px) 100vw, 236px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l.jpg 1920w"><span class="ul-placeholder-bg class-16x9"></span>
                                                            <div class="preview-video preview-video-control" data-iframepreview="http://demo.beeteam368.com/vidorev/only-in-battlefield-4-c4-house-party/?video_embed=71&amp;preview_mode=1"></div>
                                                         </a>
                                                         <span class="video-icon video-popup-control" data-id="71"></span><span class="watch-later-icon watch-later-control" data-id="71" data-img-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/only-in-battlefield-4-kill-the-l-150x150.jpg" data-hyperlink="http://demo.beeteam368.com/vidorev/only-in-battlefield-4-c4-house-party/" data-title="Only in Battlefield 4: C4 House Party"> <i class="fa fa-clock-o" aria-hidden="true"></i><span class="watch-text font-size-12">Watch Later</span><span class="watch-remove-text font-size-12">Added</span> </span><span class="duration-text font-size-12 meta-font">00:30</span>
                                                      </div>
                                                   </div>
                                                   <div class="listing-content">
                                                      <h3 class="entry-title h6 post-title"> <a href="http://demo.beeteam368.com/vidorev/only-in-battlefield-4-c4-house-party/?playlist=209" title="Only in Battlefield 4: C4 House Party">Only in Battlefield 4: C4 House Party</a></h3>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="http://demo.beeteam368.com/vidorev/members/admin/">Nicolas</a></div>
                                                            <div class="date-time"><i class="fa fa-calendar" aria-hidden="true"></i><span><time class="entry-date published" datetime="2017-12-24T04:19:06+07:00">December 24, 2017</time><time class="updated" datetime="2019-09-09T15:06:38+07:00">September 9, 2019</time></span></div>
                                                         </div>
                                                      </div>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="comment-count"><i class="fa fa-comment" aria-hidden="true"></i><span>0</span></div>
                                                            <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>228K</span></div>
                                                            <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="71">5.8K</span></div>
                                                            <div class="dislike-count"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="dislike-count" data-id="71">834</span></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </article>
                                             <article class="post-item site__col post-30 post type-post status-publish format-video has-post-thumbnail hentry category-game tag-153 tag-20th-century-fox tag-action tag-feature-film tag-mono tag-united-states post_format-post-format-video pmpro-has-access">
                                                <div class="post-item-wrap">
                                                   <div class="blog-pic">
                                                      <div class="blog-pic-wrap wrap_preview wrap_preview_control preview-df-video">
                                                         <a data-post-id="30" href="http://demo.beeteam368.com/vidorev/new-map-6-lets-go-boys/?playlist=209" title="Playerunknown’s Battlegrounds – 33 Kills Let’s Go" class="blog-img">
                                                            <img class="blog-picture ul-lazysizes-effect ul-lazysizes-load mCS_img_loaded" src="http://demo.beeteam368.com/vidorev/wp-content/themes/vidorev/img/placeholder.png" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch-236x133.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch.jpg 1920w" data-sizes="(max-width: 236px) 100vw, 236px" alt="TWITCH"><span class="ul-placeholder-bg class-16x9"></span>
                                                            <div class="preview-video preview-video-control" data-iframepreview="http://demo.beeteam368.com/vidorev/new-map-6-lets-go-boys/?video_embed=30&amp;preview_mode=1"></div>
                                                         </a>
                                                         <span class="video-icon video-popup-control" data-id="30"></span><span class="watch-later-icon watch-later-control" data-id="30" data-img-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/twitch-150x150.jpg" data-hyperlink="http://demo.beeteam368.com/vidorev/new-map-6-lets-go-boys/" data-title="Playerunknown’s Battlegrounds – 33 Kills Let’s Go"> <i class="fa fa-clock-o" aria-hidden="true"></i><span class="watch-text font-size-12">Watch Later</span><span class="watch-remove-text font-size-12">Added</span> </span><span class="duration-text font-size-12 meta-font">00:27:27</span>
                                                      </div>
                                                   </div>
                                                   <div class="listing-content">
                                                      <h3 class="entry-title h6 post-title"> <a href="http://demo.beeteam368.com/vidorev/new-map-6-lets-go-boys/?playlist=209" title="Playerunknown’s Battlegrounds – 33 Kills Let’s Go">Playerunknown’s Battlegrounds – 33 Kills Let’s Go</a></h3>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="http://demo.beeteam368.com/vidorev/members/admin/">Nicolas</a></div>
                                                            <div class="date-time"><i class="fa fa-calendar" aria-hidden="true"></i><span><time class="entry-date published" datetime="2017-12-19T22:47:09+07:00">December 19, 2017</time><time class="updated" datetime="2019-09-09T15:07:49+07:00">September 9, 2019</time></span></div>
                                                         </div>
                                                      </div>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="comment-count"><i class="fa fa-comment" aria-hidden="true"></i><span>0</span></div>
                                                            <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>8.3K</span></div>
                                                            <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="30">1</span></div>
                                                            <div class="dislike-count"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="dislike-count" data-id="30">0</span></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </article>
                                             <article class="post-item site__col post-400 post type-post status-publish format-video has-post-thumbnail hentry category-game tag-151 tag-adventure tag-silent tag-sony tag-tv-movie tag-united-kingdom post_format-post-format-video pmpro-has-access">
                                                <div class="post-item-wrap">
                                                   <div class="blog-pic">
                                                      <div class="blog-pic-wrap wrap_preview wrap_preview_control preview-df-video">
                                                         <a data-post-id="400" href="http://demo.beeteam368.com/vidorev/overwatch-cinematic-trailer/?playlist=209" title="Overwatch Cinematic Trailer" class="blog-img">
                                                            <img class="blog-picture ul-lazysizes-effect ul-lazysizes-load mCS_img_loaded" src="http://demo.beeteam368.com/vidorev/wp-content/themes/vidorev/img/placeholder.png" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400-236x133.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400.jpg 1920w" data-sizes="(max-width: 236px) 100vw, 236px" alt=""><span class="ul-placeholder-bg class-16x9"></span>
                                                            <div class="preview-video preview-video-control" data-iframepreview="http://demo.beeteam368.com/vidorev/overwatch-cinematic-trailer/?video_embed=400&amp;preview_mode=1"></div>
                                                         </a>
                                                         <span class="video-icon video-popup-control" data-id="400"></span><span class="watch-later-icon watch-later-control" data-id="400" data-img-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/400-150x150.jpg" data-hyperlink="http://demo.beeteam368.com/vidorev/overwatch-cinematic-trailer/" data-title="Overwatch Cinematic Trailer"> <i class="fa fa-clock-o" aria-hidden="true"></i><span class="watch-text font-size-12">Watch Later</span><span class="watch-remove-text font-size-12">Added</span> </span><span class="duration-text font-size-12 meta-font">06:01</span>
                                                      </div>
                                                   </div>
                                                   <div class="listing-content">
                                                      <h3 class="entry-title h6 post-title"> <a href="http://demo.beeteam368.com/vidorev/overwatch-cinematic-trailer/?playlist=209" title="Overwatch Cinematic Trailer">Overwatch Cinematic Trailer</a></h3>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="http://demo.beeteam368.com/vidorev/members/admin/">Nicolas</a></div>
                                                            <div class="date-time"><i class="fa fa-calendar" aria-hidden="true"></i><span><time class="entry-date published" datetime="2017-12-18T22:29:11+07:00">December 18, 2017</time><time class="updated" datetime="2019-09-09T15:07:58+07:00">September 9, 2019</time></span></div>
                                                         </div>
                                                      </div>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="comment-count"><i class="fa fa-comment" aria-hidden="true"></i><span>0</span></div>
                                                            <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>15.1M</span></div>
                                                            <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="400">172.7K</span></div>
                                                            <div class="dislike-count"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="dislike-count" data-id="400">3.8K</span></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </article>
                                             <article class="post-item site__col post-412 post type-post status-publish format-video has-post-thumbnail hentry category-game tag-152 tag-dolby-sr tag-game-show tag-mgm tag-mini-series tag-sweden post_format-post-format-video pmpro-has-access">
                                                <div class="post-item-wrap">
                                                   <div class="blog-pic">
                                                      <div class="blog-pic-wrap wrap_preview wrap_preview_control preview-df-video">
                                                         <a data-post-id="412" href="http://demo.beeteam368.com/vidorev/need-for-speed-payback-driving-the-incredible-all-new-bmw-m5/?playlist=209" title="Need for Speed Payback Driving the Incredible all-new BMW M5" class="blog-img">
                                                            <img class="blog-picture ul-lazysizes-effect ul-lazysizes-load mCS_img_loaded" src="http://demo.beeteam368.com/vidorev/wp-content/themes/vidorev/img/placeholder.png" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW-236x133.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW-236x133.jpg 236w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW-416x234.jpg 416w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW-300x169.jpg 300w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW-768x432.jpg 768w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW-1024x576.jpg 1024w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW-600x338.jpg 600w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW-360x203.jpg 360w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW-750x422.jpg 750w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW-1500x844.jpg 1500w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW.jpg 1920w" data-sizes="(max-width: 236px) 100vw, 236px" alt="need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW"><span class="ul-placeholder-bg class-16x9"></span>
                                                            <div class="preview-video preview-video-control" data-iframepreview="http://demo.beeteam368.com/vidorev/need-for-speed-payback-driving-the-incredible-all-new-bmw-m5/?video_embed=412&amp;preview_mode=1"></div>
                                                         </a>
                                                         <span class="video-icon video-popup-control" data-id="412"></span><span class="watch-later-icon watch-later-control" data-id="412" data-img-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/need-for-speed-most-wanted-mod-unlocks-dlc_pdp_1920x1080_en_WW-150x150.jpg" data-hyperlink="http://demo.beeteam368.com/vidorev/need-for-speed-payback-driving-the-incredible-all-new-bmw-m5/" data-title="Need for Speed Payback Driving the Incredible all-new BMW M5"> <i class="fa fa-clock-o" aria-hidden="true"></i><span class="watch-text font-size-12">Watch Later</span><span class="watch-remove-text font-size-12">Added</span> </span><span class="duration-text font-size-12 meta-font">00:36</span>
                                                      </div>
                                                   </div>
                                                   <div class="listing-content">
                                                      <h3 class="entry-title h6 post-title"> <a href="http://demo.beeteam368.com/vidorev/need-for-speed-payback-driving-the-incredible-all-new-bmw-m5/?playlist=209" title="Need for Speed Payback Driving the Incredible all-new BMW M5">Need for Speed Payback Driving the Incredible all-new BMW M5</a></h3>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="http://demo.beeteam368.com/vidorev/members/admin/">Nicolas</a></div>
                                                            <div class="date-time"><i class="fa fa-calendar" aria-hidden="true"></i><span><time class="entry-date published" datetime="2017-12-13T14:33:59+07:00">December 13, 2017</time><time class="updated" datetime="2019-09-09T15:09:39+07:00">September 9, 2019</time></span></div>
                                                         </div>
                                                      </div>
                                                      <div class="entry-meta post-meta meta-font">
                                                         <div class="post-meta-wrap">
                                                            <div class="comment-count"><i class="fa fa-comment" aria-hidden="true"></i><span>0</span></div>
                                                            <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>528.8K</span></div>
                                                            <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="412">12.5K</span></div>
                                                            <div class="dislike-count"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span class="dislike-count" data-id="412">369</span></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </article>
                                          </div>
                                       </div>
                                       <div id="mCSB_1_scrollbar_horizontal" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_horizontal" style="display: block;">
                                          <div class="mCSB_draggerContainer">
                                             <div id="mCSB_1_dragger_horizontal" class="mCSB_dragger" style="position: absolute; min-width: 30px; display: block; width: 222px; max-width: 770px; left: 0px;">
                                                <div class="mCSB_dragger_bar"></div>
                                             </div>
                                             <div class="mCSB_draggerRail"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- <div class="site__col playlist-videos">
                           <div class="video-listing video-playlist-listing-control mCustomScrollbar _mCS_2 mCS_no_scrollbar">
                              <div id="mCSB_2" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" tabindex="0" style="max-height: none;">
                                 <div id="mCSB_2_container" class="mCSB_container mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                                    <div class="video-listing-header">
                                       <h5 class="header-title extra-bold">PLAYLIST</h5>
                                       <div class="header-total-videos font-size-12"> <a href="#" class="neutral" title="View Playlist"> 8 Videos <i class="fa fa-angle-double-right" aria-hidden="true"></i> </a></div>
                                    </div>
                                    <div class="video-listing-body">
                                       <div class="video-listing-item video-listing-item-control current-item" data-index="1">
                                          <div class="video-img"><a data-post-id="64" href="#" title="Battlefield Hardline: Into the Jungle" class="blog-img"><img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir-346x130.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir-346x130.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir-346x130.jpg 346w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir-568x213.jpg 568w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir-1136x426.jpg 1136w" data-sizes="(max-width: 346px) 100vw, 346px" alt="CANNES VLOG with Isadora &amp; the girls!" sizes="(max-width: 346px) 100vw, 346px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir-346x130.jpg 346w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir-568x213.jpg 568w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/cannes-vlog-with-isadora-the-gir-1136x426.jpg 1136w"><span class="ul-placeholder-bg class-2point7x1"></span></a></div>
                                          <div class="absolute-gradient"></div>
                                          <div class="video-content">
                                             <span class="video-icon small-icon alway-active"></span>
                                             <h3 class="h6 post-title"> <a class="check-url-control" href="http://demo.beeteam368.com/vidorev/battlefield-hardline-into-the-jungle/?playlist=209" title="Battlefield Hardline: Into the Jungle">Battlefield Hardline: Into the Jungle</a></h3>
                                             <div class="entry-meta post-meta meta-font">
                                                <div class="post-meta-wrap">
                                                   <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="http://demo.beeteam368.com/vidorev/members/admin/">Nicolas</a></div>
                                                   <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>3.2M</span></div>
                                                   <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="64">27.8K</span></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="video-listing-item video-listing-item-control" data-index="0">
                                          <div class="video-img"><a data-post-id="66" href="#" title="Only In Battlefield 4: How To Blow Up An Elevator" class="blog-img"><img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg 100w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-324x324.jpg 324w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-150x150.jpg 150w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-60x60.jpg 60w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-200x200.jpg 200w" data-sizes="(max-width: 100px) 100vw, 100px" alt="Battlefield 4: Multiplayer Tutorial of Carrier Assault" sizes="(max-width: 100px) 100vw, 100px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg 100w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-324x324.jpg 324w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-150x150.jpg 150w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-60x60.jpg 60w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-200x200.jpg 200w"><span class="ul-placeholder-bg class-1x1"></span></a></div>
                                          <div class="video-content">
                                             <h3 class="h6 post-title"> <a class="check-url-control" href="#" title="Only In Battlefield 4: How To Blow Up An Elevator">Only In Battlefield 4: How To Blow Up An Elevator</a></h3>
                                             <div class="entry-meta post-meta meta-font">
                                                <div class="post-meta-wrap">
                                                   <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="#">Nicolas</a></div>
                                                   <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>411.4K</span></div>
                                                   <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="66">6.9K</span></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="video-listing-item video-listing-item-control" data-index="0">
                                          <div class="video-img"><a data-post-id="66" href="#" title="Only In Battlefield 4: How To Blow Up An Elevator" class="blog-img"><img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg 100w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-324x324.jpg 324w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-150x150.jpg 150w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-60x60.jpg 60w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-200x200.jpg 200w" data-sizes="(max-width: 100px) 100vw, 100px" alt="Battlefield 4: Multiplayer Tutorial of Carrier Assault" sizes="(max-width: 100px) 100vw, 100px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg 100w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-324x324.jpg 324w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-150x150.jpg 150w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-60x60.jpg 60w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-200x200.jpg 200w"><span class="ul-placeholder-bg class-1x1"></span></a></div>
                                          <div class="video-content">
                                             <h3 class="h6 post-title"> <a class="check-url-control" href="#" title="Only In Battlefield 4: How To Blow Up An Elevator">Only In Battlefield 4: How To Blow Up An Elevator</a></h3>
                                             <div class="entry-meta post-meta meta-font">
                                                <div class="post-meta-wrap">
                                                   <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="#">Nicolas</a></div>
                                                   <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>411.4K</span></div>
                                                   <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="66">6.9K</span></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="video-listing-item video-listing-item-control" data-index="0">
                                          <div class="video-img"><a data-post-id="66" href="#" title="Only In Battlefield 4: How To Blow Up An Elevator" class="blog-img"><img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg 100w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-324x324.jpg 324w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-150x150.jpg 150w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-60x60.jpg 60w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-200x200.jpg 200w" data-sizes="(max-width: 100px) 100vw, 100px" alt="Battlefield 4: Multiplayer Tutorial of Carrier Assault" sizes="(max-width: 100px) 100vw, 100px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg 100w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-324x324.jpg 324w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-150x150.jpg 150w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-60x60.jpg 60w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-200x200.jpg 200w"><span class="ul-placeholder-bg class-1x1"></span></a></div>
                                          <div class="video-content">
                                             <h3 class="h6 post-title"> <a class="check-url-control" href="#" title="Only In Battlefield 4: How To Blow Up An Elevator">Only In Battlefield 4: How To Blow Up An Elevator</a></h3>
                                             <div class="entry-meta post-meta meta-font">
                                                <div class="post-meta-wrap">
                                                   <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="#">Nicolas</a></div>
                                                   <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>411.4K</span></div>
                                                   <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="66">6.9K</span></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="video-listing-item video-listing-item-control" data-index="0">
                                          <div class="video-img"><a data-post-id="66" href="#" title="Only In Battlefield 4: How To Blow Up An Elevator" class="blog-img"><img class="blog-picture ul-lazysizes-effect mCS_img_loaded ls-is-cached ul-lazysizes-loaded" src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg" data-src="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg" data-srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg 100w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-324x324.jpg 324w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-150x150.jpg 150w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-60x60.jpg 60w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-200x200.jpg 200w" data-sizes="(max-width: 100px) 100vw, 100px" alt="Battlefield 4: Multiplayer Tutorial of Carrier Assault" sizes="(max-width: 100px) 100vw, 100px" srcset="http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-100x100.jpg 100w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-324x324.jpg 324w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-150x150.jpg 150w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-60x60.jpg 60w, http://demo.beeteam368.com/vidorev/wp-content/uploads/2017/12/battlefield-4-multiplayer-tutori-200x200.jpg 200w"><span class="ul-placeholder-bg class-1x1"></span></a></div>
                                          <div class="video-content">
                                             <h3 class="h6 post-title"> <a class="check-url-control" href="#" title="Only In Battlefield 4: How To Blow Up An Elevator">Only In Battlefield 4: How To Blow Up An Elevator</a></h3>
                                             <div class="entry-meta post-meta meta-font">
                                                <div class="post-meta-wrap">
                                                   <div class="author vcard"><i class="fa fa-user-circle" aria-hidden="true"></i><a href="#">Nicolas</a></div>
                                                   <div class="view-count"><i class="fa fa-eye" aria-hidden="true"></i><span>411.4K</span></div>
                                                   <div class="like-count"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span class="like-count" data-id="66">6.9K</span></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div id="mCSB_2_scrollbar_vertical" class="mCSB_scrollTools mCSB_2_scrollbar mCS-light mCSB_scrollTools_vertical" style="display: block;">
                                    <div class="mCSB_draggerContainer">
                                       <div id="mCSB_2_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; display: none; height: 615px; max-height: 578px; top: 0px;">
                                          <div class="mCSB_dragger_bar" style="line-height: 30px;"></div>
                                       </div>
                                       <div class="mCSB_draggerRail"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="bottom-gradient">
                              <div class="absolute-gradient"></div>
                           </div>
                        </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div id="primary-content-wrap" class="primary-content-wrap" style="transform: none;">
         <div class="primary-content-control" style="transform: none;">
            <div class="site__container fullwidth-vidorev-ctrl container-control" style="transform: none;">
               <div class="site__row sidebar-direction" style="transform: none;">
                  <main id="main-content" class="site__col main-content">
                     <div class="single-post-wrapper global-single-wrapper">
                        <article id="post-64" class="single-post-content global-single-content post-64 post type-post status-publish format-video has-post-thumbnail hentry category-game tag-152 tag-crime tag-dolby-sr tag-mini-series tag-sweden tag-universal post_format-post-format-video pmpro-has-access">
                           <div class="entry-content ">
                              <p><?php if (!empty($video[0]->description)): ?> <?php echo make_links_clickable($video[0]->description); ?> <?php endif; ?></p>
                           </div>
                           <div class="post-footer">
                              <div class="like-dislike-toolbar-footer">
                                 <div class="ld-t-footer-wrapper">
                                    <div class="ld-t-item-content like-action-control " data-id="64" data-action="like"> <span class="like-tooltip like-tooltip-control"><span class="likethis">I Like This</span><span class="unlike">Unlike</span></span> <span class="item-icon font-size-18"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span><span class="item-text like-count" data-id="64">27.8K</span> <span class="video-load-icon small-icon"></span> <span class="login-tooltip login-req"><span>Please Login to Vote</span></span></div>
                                    <div class="ld-t-item-content like-action-control " data-id="64" data-action="dislike"> <span class="dislike-tooltip dislike-tooltip-control"><span class="dislikethis">I Dislike This</span><span class="undislike">Un-Dislike</span></span> <span class="item-icon font-size-18"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span><span class="item-text dislike-count" data-id="64">3.2K</span> <span class="video-load-icon small-icon"></span> <span class="login-tooltip login-req"><span>Please Login to Vote</span></span></div>
                                 </div>
                                 <div class="ld-t-footer-sharing">
                                    <ul class="social-block s-grid big-icon">
                                       <li class="facebook-link"> <a href="#" data-share="on" data-source="facebook" target="_blank" title="Share on Facebook"> <span class="icon"><i class="fa fa-facebook"></i></span> </a></li>
                                       <li class="twitter-link"> <a href="#" data-share="on" data-source="twitter" target="_blank" title="Share on Twitter"> <span class="icon"><i class="fa fa-twitter"></i></span> </a></li>
                                       <li class="google-plus-link"> <a href="#" target="_blank" data-share="on" data-source="googleplus" title="Share on Google Plus"> <span class="icon"><i class="fa fa-google-plus"></i></span> </a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tags-socialsharing">
                                 <div class="tags-items"> <span class="tag-title"><i class="fa fa-tags" aria-hidden="true"></i> <span class="h5 extra-bold">Tags</span></span> 

                                    <?php if (isset($video[0]->tags) and !empty($video[0]->tags)): ?> <?php  $tags_list =  explode(',', $video[0]->tags); ?> <?php endif; ?>


                                    <?php if (isset($tags_list) and !empty($tags_list)): ?>
                                       <?php foreach ($tags_list as $key => $value): ?>
                                          <a href="#" title="2020" class="tag-item font-size-12"><?php echo $value; ?></a>
                                       <?php endforeach ?> 
                                    <?php endif; ?>

                                     
                                 </div>
                              </div>
                           </div>
                        </article>
                        <div id="comments" class="comments-area" style="border: none;">
                           <div id="respond" class="comment-respond">
                              <h3 id="reply-title" class="comment-reply-title h5 extra-bold">
                                 Leave your comment 
                                 <small>
                                    <a rel="nofollow" id="cancel-comment-reply-link" href="/vidorev/battlefield-hardline-into-the-jungle/?playlist=209#respond" style="display:none;">Cancel reply</a>
                                 </small>
                              </h3>
                              <form action="http://demo.beeteam368.com/vidorev/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate="">
                                 <p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span></p>
                                 <p class="comment-form-comment"><textarea id="comment" name="comment" required="required" placeholder="Your comment *"></textarea></p>
                                  
                         
                          
                              </form>
                           </div>
                           <!-- #respond -->
                        </div>
                     </div>
                  </main>
                  
               </div>
            </div>
         </div>
      </div>
</section>
<?php $this->load->view('user/layout/footer'); ?>