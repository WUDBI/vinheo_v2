<?php $this->load->view('user/layout/header');  

$ci=& get_instance(); 

$ci->load->model('CommonModel'); 



 

function make_links_clickable($text){

    return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a target="_blank" href="$1">$1</a>', $text);

}



?>

<style type="text/css">

   .single-post-video-full-width-wrapper { 
       padding-bottom: 30px; 
   }
   	.top_thumbnail{
	   	position: absolute;
	    top: 0px; 
	    border-bottom: 5px solid red;
   	}



   	.required_label{

   		color: red;

   	}

   	.inactiveLink {
	    pointer-events: none;
	    cursor: no-drop;
	}

   	.add_border{

   		border: 1px solid;

   	}



   	.img_height_thumb{

   		width: 100%;

   		height: 3.83333rem;	

   	}





   	/* Fixed sidenav, full height */

	.sidenav { 

	  	background-color: #111; 

	  	padding-top: 20px;

	}



	/* Style the sidenav links and the dropdown button */

	.sidenav a, .dropdown-btn {

		padding: 6px 8px 6px 16px;

		text-decoration: none;

		font-size: 20px;

		color: #818181;

		display: block;

		border: none;

		background: none;

		width: 100%;

		text-align: left;

		cursor: pointer;

		outline: none;

	}



	/* On mouse-over */

	.sidenav a:hover, .dropdown-btn:hover {

	  	color: #f1f1f1;

	}



	/* Main content */

	.main {

		margin-left: 200px; /* Same as the width of the sidenav */

		font-size: 20px; /* Increased text to enable scrolling */

		padding: 0px 10px;

	}



	/* Add an active class to the active dropdown button */

	.active {

		background-color: green;

		color: white;

	}



	/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */

	.dropdown-container {

		display: block;

		background-color: #262626;

		padding-left: 8px;

	}



	/* Optional: Style the caret down icon */

	.fa-caret-down {

		float: right;

		padding-right: 8px;

	}



	/* Some media queries for responsiveness */

	@media screen and (min-width: 450px) {

	  	.sidenav {padding-top: 15px;}

	  	.sidenav a {font-size: 18px;}

	  	.img_height_thumb img { height: 400px !important; }
	  	.img_height_thumb{  height: 400px !important;  }

	  	.remove_height{
	  		height: auto !important;
	  	}

	}


	@media screen and (min-width: 300px) {

	  	.sidenav {padding-top: 15px;}

	  	.sidenav a {font-size: 18px;}

	  	.img_height_thumb img { height: 200px !important; }
	  	.img_height_thumb{  height: 200px !important;  }

	  	.remove_height{
	  		height: auto !important;
	  	}

	}



	.overflow_style{

	    padding: 20px 0px 0px 0px;

	    height: 661px;

	    overflow: scroll;

	    overflow-x: hidden;

	}

	#style-2::-webkit-scrollbar-track

	{

	     -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);

	     border-radius: 10px;

	     background-color: #F5F5F5;

	}



	#style-2::-webkit-scrollbar

	{

	     width: 12px;

	     background-color: #F5F5F5;

	}



	#style-2::-webkit-scrollbar-thumb

	{

	     border-radius: 10px;

	     -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);

	     background-color: #10F920;

	}



	.show_me{

		display: block;

	}

	.select_style .show_me{
		height: 42px !important;
		line-height: 34px !important;
	}
	.select_style .current{
		font-size: 22px !important;
	}

	.nice-select{

		width: 100%;

	}



	.footer-area{

		display: none;

	}



	.img-thumbnail{ 
		height: 100%;
		width: 100% !important;
		padding: 0px !important; 
	}
	.list{
		width: 100% !important;
	}

	.select_style .nice-select{
	    text-align: -webkit-center !important;
	    /* margin-bottom: 10px !important; */ 
	    background-color: #10F41F !important;
	}

	.creat_thumbnail{
		display: none;
	}


.progress 
{     
	position: absolute;
    width: 100%;
    bottom: 89px; 
    border: 1px solid #ddd; 
    padding: 1px; 
    border-radius: 3px; 
    display: none;
}
.bar { 
	background-color: #B4F5B4; 
	width:0%; height:20px; 
	border-radius: 3px; 
}
.percent { 
	position:absolute; 
	display:inline-block; 
	top:-2px; left:48%; 
}

.cancel_btn{
	position: absolute;
    /* width: 100%; */
    display: none;
    bottom: 45px;
}
.update_box_on{
	display: none;
}
</style>



<div class="row">

	<div class="col-md-12">

		<?php if ($this->session->flashdata('error')): ?>  

	      	<div class="alert alert-warning alert-dismissible " role="alert" style="margin-top:10px "> 

	            <?php echo $this->session->flashdata('error'); ?> 

	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> 

                  	<span aria-hidden="true">&times;</span> 

	            </button> 

	      	</div>  

	    <?php endif ?>



	    <?php if ($this->session->flashdata('success')): ?>  

	      	<div class="alert alert-success alert-dismissible " role="alert" style="margin-top:10px "> 

	            <?php echo $this->session->flashdata('success'); ?> 

	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> 

                  	<span aria-hidden="true">&times;</span> 

	            </button> 

	      	</div>  

	    <?php endif ?>

	</div>

<!-- 	<div class="col-md-2 col-sm-12 remove_height" style="height: 661px;padding: 0px;">

		<div class="sidenav" style="height: 100%;padding-top: 20px"> 

		  	<button class="dropdown-btn  active">General 

			    <i class="fa fa-caret-down"></i>

		  	</button>

		  	<div class="dropdown-container">

			    <a href="#information">Info</a>

			    <a href="#privacy">Privacy</a> 

		  	</div>

		  	<div class="progress">
		        <div class="bar"></div >
		        <div class="percent">0%</div >
		    </div>

		    <button id="cancel_ajax" class="btn btn-primary cancel_btn">Cancel</button>
		</div> 
	</div> -->



	<div class="col-md-6 overflow_style" id="style-2" style="padding: 0px" >

		<form method="post" action="<?php echo base_url() ?>videoedit/update_date" id='update_data'>
			<input type="hidden" name="video_id" id="video_id" value="<?php echo $video[0]->video_id; ?>">
			<div class="row" style="padding-top: 20px">

				<div  class="col-md-12" id="information">

					<h3>General</h3>

					<p>Adjust your settings for video ,edit title ,description or add new thumbnail.</p>

				</div>



				<div  class="col-md-12  form-group" >

					<label>Title</label><span class="required_label">*</span>

					<input type="text" name="title" class="form-control" value="<?php echo $video[0]->title; ?>">

				</div>



				<div  class="col-md-12  form-group">

					<label>Description</label><span class="required_label">*</span>

					<textarea class="form-control" name="description" rows="4"><?php echo $video[0]->description; ?></textarea>

				</div>

 
				<form action="<?php echo base_url() ?>videoedit/update_thumbnail">
					<input type="hidden" name="video_id" value="<?php echo $video[0]->video_id; ?>"> 
					<div  class="col-md-12 form-group"> 
						<label>Thumbnail</label><span class="required_label">*</span>

						<div class="row">

							<?php  

							$thumbnails = $ci->CommonModel->getAll('video_thumbnails', ' video_id ="'.$video[0]->video_id.'" order by id desc limit 3 ');

							?>

							<div class="col-md-4  " style="padding: 0px 0px;" >

								<div class="img_height_thumb add_border">

									<?php if (isset($thumbnails[0]) and $thumbnails[0]->base_img == 'no'): ?>

										<img src="<?php echo base_url(); ?>assets/video_thumnails/<?php echo $this->session->userdata('user_id') ?>/<?php echo $thumbnails[0]->thumbnail; ?>" class='img-thumbnail'>

									<?php elseif(isset($thumbnails[0]->base_img) and $thumbnails[0]->base_img == 'yes'):



										?>  

										<img src="<?php echo base_url(); ?><?php echo $thumbnails[0]->thumbnail; ?>" class='img-thumbnail'>

										 

									<?php endif; ?> 

								</div>

								<?php if (isset($thumbnails[0]) and !empty($thumbnails[0])): ?>
									<a href="<?php echo base_url(); ?>videoedit/delete_thumbnail/<?php echo $thumbnails[0]->id; ?>/<?php echo $thumbnails[0]->video_id; ?>" class="top_thumbnail btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>

									<input type="radio" name="box" value="<?php echo $thumbnails[0]->id; ?>" class="check_box check1"> 
								<?php endif; ?>


							</div>



							<div class="col-md-4  " style="padding: 0px 4px;" >

								<div class="img_height_thumb add_border">

									<?php if (isset($thumbnails[1]) and  $thumbnails[1]->base_img == 'no'): ?>

										<img src="<?php echo base_url(); ?>assets/video_thumnails/<?php echo $this->session->userdata('user_id') ?>/<?php echo $thumbnails[1]->thumbnail; ?>" class='img-thumbnail'>

									<?php elseif(isset($thumbnails[1]->base_img) and $thumbnails[1]->base_img == 'yes'): ?> 

										<img src="<?php echo base_url(); ?><?php echo $thumbnails[1]->thumbnail; ?>" class='img-thumbnail'>

									<?php endif; ?> 

								</div>

								<?php if (isset($thumbnails[1]) and !empty($thumbnails[1])): ?>
									<a href="<?php echo base_url(); ?>videoedit/delete_thumbnail/<?php echo $thumbnails[1]->id; ?>/<?php echo $thumbnails[1]->video_id; ?>" class="top_thumbnail btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>

									<input type="radio" name="box" value="<?php echo $thumbnails[1]->id; ?>" class="check_box check2"> 
								<?php endif; ?>

							</div>



							<div class="col-md-4  " style="padding: 0px 0px;" >

								<div class="img_height_thumb add_border">

									<?php if (isset($thumbnails[2]->base_img) and $thumbnails[2]->base_img == 'no'): ?>

										<img src="<?php echo base_url(); ?>assets/video_thumnails/<?php echo $this->session->userdata('user_id') ?>/<?php echo $thumbnails[2]->thumbnail; ?>" class='img-thumbnail'>

									<?php elseif(isset($thumbnails[2]->base_img) and $thumbnails[2]->base_img == 'yes'): ?> 

										<img src="<?php echo base_url(); ?><?php echo $thumbnails[0]->thumbnail; ?>" class='img-thumbnail'>

									<?php endif; ?> 

								</div>

								<?php if (isset($thumbnails[2]) and !empty($thumbnails[2])): ?>
									<a href="<?php echo base_url(); ?>videoedit/delete_thumbnail/<?php echo $thumbnails[2]->id; ?>/<?php echo $thumbnails[2]->video_id; ?>" class="top_thumbnail btn btn-primary btn-sm"><i class="fa fa-trash"></i></a>

									<input type="radio" name="box" value="<?php echo $thumbnails[2]->id; ?>" class="check_box check3"> 
								<?php endif; ?>

							</div> 
						</div> 
					</div> 

					<div  class="col-md-12 form-group update_box_on"  >  
						<button class="btn btn-info  submit_thumbnail" type="button" >Update</button>  
					</div>
				 

				<div  class="col-md-12 form-group" style="padding: 2rem 0px; border-bottom: 1px solid rgb(227, 232, 233)"> 
					<div class="row">
						<div class="col-md-12 select_style">
							<select required class="form-control show_me" id="edit_thumbnail" > 
			                    <option value="" hidden="" selected="" disabled="">Edit Thumbnail</option>
			                    <option value="1" >Save from video</option>

			                    <option value="2" >Upload</option>

			                    <option value="3" >Random</option> 

			                </select>
						</div>
						<div class="col-md-6">

							<!-- <button class="btn btn-info creat_thumbnail" >Create</button> -->

						</div>



						<div class="col-md-6" >

							

							<!-- <button class="btn btn-info pull-right" id="custom_upload">Upload</button> -->

						</div>

					</div> 

				</div>





				<div  class="col-md-12  form-group" id="privacy">

					<h3>Privacy</h3>

					<p>Choose who can and who can't see this video.</p>

				</div>



				<div  class="col-md-12  form-group"  >

					<label>Who can watch</label> 

					<select required class="form-control show_me"  name="privacy" >

	                    <option value="1" <?php if ($video[0]->privacy == 1): ?> selected <?php endif ?> >Anyone can see this video</option>

	                    <option value="2" <?php if ($video[0]->privacy == 2): ?> selected <?php endif ?> >Only i can see this video</option>

	                    <option value="3" <?php if ($video[0]->privacy == 3): ?> selected <?php endif ?> >Only people with private link</option>

	                    <option value="4" <?php if ($video[0]->privacy == 4): ?> selected <?php endif ?> >Only people with password can see see this video</option>

	                </select>

				</div>





				<div  class="col-md-12  form-group"  >

					<label><?php echo  lang('Privacy2') ?></label>

	                <input required type="text" name="tags" class="form-control" value="<?php echo $video[0]->tags; ?>">

				</div>



				<div  class="col-md-12  form-group"  >

					<label>Download</label>

	                <select required class="form-control show_me" name="download">

	                    <option value="allowed"  <?php if ($video[0]->download == 'allowed'): ?> selected <?php endif ?> >Allowed</option>

	                    <option value="not"  <?php if ($video[0]->download == 'not'): ?> selected <?php endif ?> >Not Allowed</option>

	                     

	                </select>

				</div>



				<div  class="col-md-12  form-group"  >

					<div class="row">

						<div class="col-md-6" style="padding: 0px">

							<a href="<?php echo base_url(); ?>upload/all_videos" class="btn btn-danger close_me_off " onclick="return confirm('Are you sure you want to exit?');" >Exit</a>	

						</div>



						<div class="col-md-6"  style="padding: 0px">

							<input type="submit" style=" background: #1cb11c;" name="update" class="btn btn-info pull-right update_me close_me_off" value="Update">

						</div>

					</div> 



					<div class="row"> 

						<div class="col-md-12  form-group"  style="padding: 0px;margin-top: 15px;" > 
							<button   style="width: 100%;" class="btn btn-primary  replace_press_btn"  > Replace Video</button>

						</div>
 
					</div> 


					<div class="row"> 

						<div class="col-md-12  form-group"  style="padding: 0px; " >
							<a  style="width: 100%;" onclick="return confirm('Are you sure you want to delete this video?');" href="<?php echo base_url() ?>/videoedit/delete_video/<?php echo $video[0]->video_id; ?>"  class="btn btn-danger" >Delete Video</a>
 
						</div>
 
					</div> 
				</div>

			</div> 
		</form>

		<form style="display: none;" action="<?php echo base_url() ?>videoedit/replace_video" method="post" id='replace_video_form' enctype="multipart/form-data">
        	<input type="file" name="video_file" id="video_replace_file" ><br> 
        	<input type="hidden" name="video_id" id="video_id" value="<?php echo $video[0]->video_id; ?>">
    	</form>
    	
	    
	    <div id="status"></div>

		<form style='display: none;' action="<?php echo base_url(); ?>videoedit/upload_thumbnail"  method="post" enctype="multipart/form-data"  id='custom_thumbnail_form'>

			<input type="file" style='display: none;' name="file_thumbnail" id="file_upload" value="">

			<input type="hidden" name="video_id" id="video_id" value="<?php echo $video[0]->video_id; ?>">

		</form>

	</div>



	<div class="col-md-6" style="padding: 0px">

		<section class="contact-section  "  > 

			<div class="single-post-video-full-width-wrapper dark-background overlay-background is_vid_playlist_layout" style="height: 661px">

		         <div class="full-width-breadcrumbs">

		            <div class="site__container fullwidth-vidorev-ctrl">

		               <div class="site__row nav-breadcrumbs-elm">

		                  <div class="site__col"></div>

		               </div>

		            </div>

		         </div>

		         <div class="single-post-video-player-content" style="    padding-bottom: 0px;">

		            <div class="site__container fullwidth-vidorev-ctrl">

		               <div class="site__row">

		                  <div class="site__col">

		                  	<i class="fa fa-link" aria-hidden="true" style="margin-right: 5px"></i><a href="<?php echo base_url(); ?>video/<?php echo $video[0]->video_id; ?>"><?php echo base_url(); ?>video/<?php echo $video[0]->video_id; ?></a>

		                     <div class="site__row playlist-frame playlist-frame-control">

		                        <div class="site__col player-in-playlist" style="width: 100%;">

		                           <div class="single-player-video-wrapper vp-small-item">

		                              <div class="light-off light-off-control"></div>

		                              <div id="video-player-wrap-control" class="video-player-wrap">

		                                 <div class="video-player-ratio"></div>

		                                 <div class="video-player-content video-player-control">

		                                    <div class="float-video-title">

		                                       <h6>

		                                          <?php if (!empty($video[0]->title)): ?>

		                                             <?php echo $video[0]->title; ?>

		                                          <?php else: ?>

		                                             <?php echo $video[0]->video_name; ?>

		                                          <?php endif; ?>

		                                           

		                                       </h6>

		                                    </div>

		                                    <a href="#" title="Close" class="close-floating-video close-floating-video-control"><i class="fa fa-times" aria-hidden="true"></i></a> <a href="#" title="Scroll Up" class="scroll-up-floating-video scroll-up-floating-video-control"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></a>

		                                    <div class="player-3rdparty player-3rdparty-control  player-loaded">

		                                        
		                                    	<button class="btn btn-info creat_thumbnail" >Select this frame</button>


		                                        <?php 

					

													$thumbnail = $ci->CommonModel->getAll('video_thumbnails', ' video_id ="'.$video[0]->video_id.'" and active="yes" order by id desc limit 1 ');

												?>

		                                       

		                                       <video  <?php if (isset($thumbnail[0]) and $thumbnail[0]->base_img == 'no'): ?> poster="<?php echo base_url(); ?>assets/video_thumnails/<?php echo $this->session->userdata('user_id') ?>/<?php echo $thumbnail[0]->thumbnail; ?>" <?php elseif(isset($thumbnail[0]) and $thumbnail[0]->base_img == 'yes'): ?> poster="<?php echo base_url(); ?><?php echo $thumbnail[0]->thumbnail; ?>" <?php endif; ?>  playsinline    src="<?php echo base_url(); ?>assets/work_files/<?php echo $video[0]->user_id; ?>/<?php echo $video[0]->video_name; ?>"  preload="none"  style="width: 100%;height: 100%;" controls    controlsList="nofullscreen nodownload" id='video_current'></video>

		                                     

		                                       

		                                       <div class="autoplay-off-elm autoplay-off-elm-control video-play-control" data-id="64"  > <span class="video-icon big-icon video-play-control" data-id="64"></span> <img class="poster-preload" src=" " alt="Preload Image"></div>

		                                       <div class="player-muted ads-mute-elm ads-muted-control"><i class="fa fa-volume-off" aria-hidden="true"></i></div>

		                                       <div class="auto-next-elm auto-next-elm-control dark-background"    >

		                                          <div class="auto-next-content">

		                                             <div class="up-next-text font-size-12">Up next</div>

		                                             <h3 class="h6-mobile video-next-title video-next-title-control"><?php if (!empty($video[0]->title)): ?> <?php echo $video[0]->title; ?> <?php else: ?> <?php echo $video[0]->video_name; ?> <?php endif; ?></h3>

		                                             <div class="loader-timer-wrapper loader-timer-control">

		                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" class="loader-timer">

		                                                   <circle class="progress-timer" fill="none" stroke-linecap="round" cx="20" cy="20" r="15.915494309"></circle>

		                                                </svg>

		                                                <i class="fa fa-fast-forward" aria-hidden="true"></i>

		                                             </div>

		                                             <a href="#" class="basic-button basic-button-default cancel-btn cancel-btn-control">Cancel</a>

		                                          </div>

		                                       </div>

		                                    </div>

		                                    <div class="video-loading video-loading-control"> <span class="video-load-icon"></span></div>

		                                 </div>

		                              </div>

		                              <div class="video-toolbar dark-background video-toolbar-control">

		                                 <div class="tb-left">

		                                    <div class="site__row">

		                                       

		                                       <div class="site__col toolbar-item">

		                                          <div class="toolbar-item-content like-action-control " data-id="64" data-action="like"> <span class="like-tooltip like-tooltip-control"><span class="likethis">I Like This</span><span class="unlike">Unlike</span></span> <span class="item-icon font-size-18"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span><span class="item-text">Like</span> <span class="video-load-icon small-icon"></span> <span class="login-tooltip login-req"><span>Please Login to Vote</span></span></div>

		                                       </div>

		                                       <div class="site__col toolbar-item">

		                                          <div class="toolbar-item-content like-action-control " data-id="64" data-action="dislike"> <span class="dislike-tooltip dislike-tooltip-control"><span class="dislikethis">I Dislike This</span><span class="undislike">Un-Dislike</span></span> <span class="item-icon font-size-18"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span><span class="item-text">Dislike</span> <span class="video-load-icon small-icon"></span> <span class="login-tooltip login-req"><span>Please Login to Vote</span></span></div>

		                                       </div>

		                                      

		                                    </div>

		                                 </div>

		                                 <div class="tb-right">

		                                    <div class="site__row">

		                                      

		                                       <div class="site__col toolbar-item">

		                                          <div class="toolbar-item-content comment-video-control scroll-elm-control" data-href="#comments"> <span class="item-text">0 Comments</span><span class="item-icon font-size-18"><i class="fa fa-comment" aria-hidden="true"></i></span></div>

		                                       </div>

		                                    </div>

		                                 </div>

		                              </div>

		                              <div class="social-share-toolbar social-share-toolbar-control">

		                                 <div class="social-share-toolbar-content">

		                                    <ul class="social-block s-grid big-icon">

		                                       <li class="facebook-link"> <a href="" data-share="on" data-source="facebook" target="_blank" title="Share on Facebook"> <span class="icon"><i class="fa fa-facebook"></i></span> </a></li>

		                                       <li class="twitter-link"> <a href="" data-share="on" data-source="twitter" target="_blank" title="Share on Twitter"> <span class="icon"><i class="fa fa-twitter"></i></span> </a></li>

		                                       <li class="google-plus-link"> <a href="" target="_blank" data-share="on" data-source="googleplus" title="Share on Google Plus"> <span class="icon"><i class="fa fa-google-plus"></i></span> </a></li>

		                                       <li class="whatsapp-link"> <a href="" target="_blank" data-share="on" data-source="whatsapp" data-action="share/whatsapp/share" title="Share on WhatsApp"> <span class="icon"><i class="fa fa-whatsapp"></i></span> </a></li>

		                                       <li class="linkedin-link"> <a href="" target="_blank" data-share="on" data-source="linkedin" title="Share on LinkedIn"> <span class="icon"><i class="fa fa-linkedin"></i></span> </a></li>

		                                       <li class="pinterest-link"> <a href="" data-share="on" data-source="pinterest" target="_blank" title="Pin this"> <span class="icon"><i class="fa fa-pinterest"></i></span> </a></li>

		                                    </ul>
 

		                                 </div>

		                              </div>

		                              <div class="video-sub-toolbar">

		                                 <div class="tb-left">

		                                    <div class="site__row">

		                                       <div class="site__col toolbar-item">

		                                          <div class="toolbar-item-content view-like-information">

		                                             <div class="view-count h4"> 0 Views</div>

		                                             <div class="like-dislike-bar"> 

		                                                <span class="like-dislike-bar-percent-control" style="width:89.531023368251%"></span>

		                                             </div>

		                                             <div class="like-dislike-infor">

		                                                <div class="like-number"><span class="block-icon"><i class="fa fa-thumbs-up" aria-hidden="true"></i></span><span class="like-count-full" data-id="64">0</span></div>

		                                                <div class="dislike-number"><span class="block-icon"><i class="fa fa-thumbs-down" aria-hidden="true"></i></span><span class="dislike-count-full" data-id="64">0</span></div>

		                                                <span></span>

		                                             </div>

		                                          </div>

		                                       </div>

		                                    </div>

		                                 </div>

		                                 <div class="tb-right">

		                                    <div class="site__row">

		                                        

		                                      

		                                       <div class="site__col toolbar-item">

		                                          <div class="toolbar-item-content"> <a href="" class="item-button prev-video"> <span class="subtollbartolltip"><span class="repeatthis">Previous Video</span></span> <span class="item-icon"><i class="fa fa-fast-backward" aria-hidden="true"></i></span><span class="item-text">Prev</span> </a></div>

		                                       </div>

		                                       <div class="site__col toolbar-item">

		                                          <div class="toolbar-item-content"> <a href="" class="item-button next-video"> <span class="subtollbartolltip"><span class="repeatthis">Next Video</span></span> <span class="item-text">Next</span><span class="item-icon"><i class="fa fa-fast-forward" aria-hidden="true"></i></span> </a></div>

		                                       </div>

		                                       <?php if (isset($video[0]->download) and  $video[0]->download =='allowed'): ?> 

		                                           

		                                          <div class="site__col toolbar-item">

		                                             <a href="<?php echo base_url(); ?>assets/work_files/<?php echo $video[0]->user_id; ?>/<?php echo $video[0]->video_name; ?>" download>

		                                             <div class="toolbar-item-content"> <span class="item-button more-videos more-videos-control" style="background-color: #0EF91E;"> <span class="subtollbartolltip"><span class="repeatthis">Download</span></span> <span class="item-text">Download</span><span class="item-icon"><i class="fa fa-download" aria-hidden="true"></i></span> </span></div></a>

		                                          </div>

		                                       <?php endif; ?>

		                                    </div>

		                                 </div>

		                              </div>

		                              <div class="member-playlist-lightbox dark-background">

		                                 <div class="playlist-lightbox-listing">

		                                    <div class="playlist-close playlist-items-control"> <i class="fa fa-times" aria-hidden="true"></i></div>

		                                    <div class="ajax-playlist-list ajax-playlist-list-control">

		                                       <span class="loadmore-loading playlist-loading-control">

		                                          <span class="loadmore-indicator">

		                                             <svg>

		                                                <polyline id="back" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>

		                                                <polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>

		                                             </svg>

		                                          </span>

		                                       </span>

		                                    </div>

		                                    

		                                 </div>

		                              </div>

		                              <div class="show-more-videos show-more-videos-control">

		                                 <div class="blog-wrapper global-blog-wrapper blog-wrapper-control mCustomScrollbar _mCS_1">

		                                    <div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_horizontal mCSB_inside" style="max-height: none;" tabindex="0">

		                                       	<div id="mCSB_1_container" class="mCSB_container" style="position: relative; top: 0px; left: 0px; width: 2740px;" dir="ltr">

		                                          	<div class="blog-items blog-items-control site__row grid-small"> 
		                                          	</div>

		                                       	</div>

		                                       <div id="mCSB_1_scrollbar_horizontal" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_horizontal" style="display: block;">

		                                          <div class="mCSB_draggerContainer">

		                                             <div id="mCSB_1_dragger_horizontal" class="mCSB_dragger" style="position: absolute; min-width: 30px; display: block; width: 222px; max-width: 770px; left: 0px;">

		                                                <div class="mCSB_dragger_bar"></div>

		                                             </div>

		                                             <div class="mCSB_draggerRail"></div>

		                                          </div>

		                                       </div>

		                                    </div>

		                                 </div>

		                              </div>

		                           </div>

		                        </div>

		                        

		                     </div>

		                  </div>

		               </div>

		            </div>

		         </div>

		    </div> 

		</section> 

	</div>

</div>
 

<div id="thumbnailContainer" style="display: none;">

	

</div>

<canvas  id="canvas"  style="display: none;" ></canvas>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="  max-width: 850px;">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0EF91E;">
        <h5 class="modal-title" id="exampleModalLabel" style="width: 100%; font-size: 26px;text-align: center;">Random Images</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="row">

      		<?php foreach ($random_images as $key => $value): ?> 
      			<?php if ( $key < 6): ?>  
      			<div class="col-md-4 form-group" style=" text-align: center;"> 
	      			<img src="<?php echo base_url().$value; ?>" style="width: 100%;border: 1px solid black;">
	      			<p><label>Select </label> <input type="radio" name="dataURL" id="data_<?php echo ++$key; ?>" value="<?php echo $value; ?>" ></p> 
	      		</div>
	      		<?php endif ?>
      		<?php endforeach ?>
      		
 
      	</div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary random_save">Save changes</button>
      </div>
    </div>
  </div>
</div>

 

<?php $this->load->view('user/layout/footer'); ?>



<script type="text/javascript">

 var checker ='no';

 
var dropdown = document.getElementsByClassName("dropdown-btn");

var i;



for (i = 0; i < dropdown.length; i++) {

	dropdown[i].addEventListener("click", function() {

		this.classList.toggle("active");

		var dropdownContent = this.nextElementSibling;

		if (dropdownContent.style.display === "block") {

			dropdownContent.style.display = "none";

		} else {

			dropdownContent.style.display = "block";

		}

	});

}





// $(document).on('click','#custom_upload',function(){ 

	

// })



$(document).on('change','#file_upload',function(){  
	$('#custom_thumbnail_form').submit();  
})
$(document).on('click','.update_me',function(){  
	$('#update_data').submit();  
})


//replace video
$(document).on('click','.replace_press_btn',function(e){ 
	
	var result = confirm('Are you sure you want to replace this video?');
    if(!result) {
        e.preventDefault();
        return false;
    }
	$('#video_replace_file').click();  
})



$(document).on('change','#video_replace_file',function()
{  
	$('.progress').show(); 
	$('.cancel_btn').show(); 
	$('.close_me_off').attr('disabled',true); 
	$('.close_me_off').addClass('inactiveLink'); 
	$('#replace_video_form').submit();  

	checker  = 'on';
})



$(document).on('click','a',function(e)
{  
	if (checker == 'on') 
	{
		var result = confirm('There are unsaved changes.Are you sure you want to exit?');
	    if(!result) {
	        e.preventDefault();
	        return false;
	    }
	}
	
})






$(document).on('change','#edit_thumbnail',function(e){
	var valueis = $(this).val();

	if (valueis== 2) 
	{
		$('#file_upload').click();
		$('.creat_thumbnail').css('display','none')
	}
	if (valueis== 1) 
	{
		$('.creat_thumbnail').css('display','block')
		 
	}

	if (valueis== 3) 
	{  
		$('#exampleModal').modal('toggle');

		$('.creat_thumbnail').css('display','none')  
	}
});





$(document).on('click','.creat_thumbnail',function(e){

	$(this).attr('disabled',true)
	

	e.preventDefault();

	var thecanvas = document.getElementById('canvas');



    var video = document.getElementById('video_current');

     

    var context = thecanvas.getContext('2d');



    context.drawImage(video, 0, 2, 350, 150);

    var dataURL = thecanvas.toDataURL();



    //create img

    var img = document.createElement('img');

    img.setAttribute('src', dataURL);

 

    document.getElementById('thumbnailContainer').appendChild(img);

    

    var video_id = $('#video_id').val();



     $.ajax({

        type: "POST",

        url : "<?php echo base_url(); ?>videoedit/uploadvideo_thumbnail",

        dataType: 'JSON',

        data: { imgBase64 : dataURL,video_id : video_id  },

       	success: function(data) { 
           	if (data.success) 
           	{ 
           	 	alert(data.success); 
           	 	location.reload(); 
           	} 
       	} 
    });

})



$(document).on('click','.random_save',function(e){ 

	e.preventDefault();
  
    var video_id = $('#video_id').val();


    if ($('#data_1').is(':checked')) 
    {
    	var dataURL  = $('#data_1').val();
    	$(this).attr('disabled',true)
    } else if ($('#data_2').is(':checked')) 
    {
    	var dataURL  = $('#data_2').val();
    	$(this).attr('disabled',true)
    }else if ($('#data_3').is(':checked')) 
    {
    	var dataURL  = $('#data_3').val();
    	$(this).attr('disabled',true)
    }else if ($('#data_4').is(':checked')) 
    {
    	var dataURL  = $('#data_4').val();
    	$(this).attr('disabled',true)
    }else if ($('#data_5').is(':checked')) 
    {
    	var dataURL  = $('#data_5').val();
    	$(this).attr('disabled',true)
    }else if ($('#data_6').is(':checked')) 
    {
    	var dataURL  = $('#data_6').val();
    	$(this).attr('disabled',true)
    } else{
    	alert('Please select any frame');
    	return false;
    }

    
 
     $.ajax({

        type: "POST",

        url : "<?php echo base_url(); ?>videoedit/uploadvideo_randomthumbnail",

        dataType: 'JSON',

        data: { dataURL : dataURL,video_id : video_id  },

       	success: function(data) { 
           	if (data.success) 
           	{ 
           	 	alert(data.success); 
           	 	location.reload(); 
           	} 
       	} 
    });

})




$(document).on('click','.submit_thumbnail',function(e){ 

	e.preventDefault();
  
    var video_id = $('#video_id').val();

    if ($('.check1').is(':checked')) 
    {
    	var check = $('.check1').val();

    } else if($('.check2').is(':checked')) {

    	var check = $('.check2').val();

    }else if($('.check3').is(':checked')) {

    	var check = $('.check3').val();
    } else {
    	alert('Please select any thumbnail');
    	return false;
    }
     
 
     $.ajax({

        type: "POST",

        url : "<?php echo base_url(); ?>videoedit/update_thumbnail",

        dataType: 'JSON',

        data: { box : check,video_id : video_id  },

       	success: function(data) { 
	   		if (data.success) 
	       	{ 
           	 	alert(data.success); 
           	 	location.reload(); 
	       	} 
       	} 
    });

})


$(document).on('click','#cancel_ajax',function(){ 
	var result = confirm('Are you sure you want to cancel video upload?');
    if(!result) 
    {
    	e.preventDefault();
        return false;
    }
	$('.progress').hide(); 
	$('.cancel_btn').hide(); 
	$('.close_me_off').attr('disabled',false); 
	$('.close_me_off').removeClass('inactiveLink'); 
	location.reload();  
})

$(document).on('click','.check_box',function(){  

	if ($(this).is(':checked')) 
	{
		$('.update_box_on').show(); 
	}else{
		$('.update_box_on').hide(); 
	}
	  
})
 
 

////////////////////////////random

 
   
///////////////////////////////////////////////////////end random

</script>
<script src="<?php echo base_url(); ?>assets/js/form_script.js"></script>
<script>
(function() {
    
var bar = $('.bar');
var percent = $('.percent');
var status = $('#status');
   
$('#replace_video_form').ajaxForm({
    beforeSend: function() {
        status.empty();
        var percentVal = '0%';
        bar.width(percentVal)
        percent.html(percentVal);
    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        bar.width(percentVal)
        percent.html(percentVal);
		//console.log(percentVal, position, total);
    },
    success: function() {
        var percentVal = '100%';
        bar.width(percentVal)
        percent.html(percentVal);
    },
	complete: function(xhr) {
		location.reload(); 
		// status.html(xhr.responseText);
	}
}); 

})();      








</script> 