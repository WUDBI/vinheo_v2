<?php $this->load->view('user/layout/header'); ?> 
 
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
<style type="text/css">
  
    .dropzone {
        background: white;
        border-radius: 5px;
        border: 2px dashed rgb(0, 135, 247);
        border-image: none;
        max-width: 500px; 
    }  

    #upload_page .nice-select{
        display: none !important;
    }

    .show_me{
        display: block !important;
        
    }
    .dropzone .dz-preview .dz-progress {  
        height: 16px;
        left: 31%;
        background-color: aliceblue;
        top: 105%;
        margin-left: -35px;
        width: 122px;
        
    }

    .footer-area{
        display: none;
    }

    .make_copy_row{
        /*display: none;*/
    }
    .dz-progress{
        width: 100%;
        border: 1px solid #ddd;
    }
    .dz-upload{
        background: aquamarine;
        border: 1px solid #ddd;
        padding: 1px;
        border-radius: 3px;
        display: flex; 
        overflow: hidden;
        font-size: .75rem;
        background-color: #e9ecef;
        height: 18px;
    }

    .dz-success-mark ,.dz-error-mark{
        display: none;
    }
    .dz-remove{
        color: #fff  !important;
        background-color: #dc3545  !important;
        border-color: #dc3545  !important;
        margin-top: 14px;
        display: inline-block;
        font-weight: 400;
        color: #212529;
        text-align: center;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-color: transparent;
        border: 1px solid transparent;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: .25rem;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,
        box-shadow .15s ease-in-out;
    }

    .dropzone {
        
        max-width: 94%;
    }

    label{
        display: inline-block;
        margin-bottom: 5px;
        font-size: .875rem;
        font-weight: 700;
        line-height: 1.35;
        color: #1a2e3b;
    }

    .progress 
    {      
        width: 100%;
        bottom: 89px; 
        border: 1px solid #ddd; 
        padding: 1px; 
        border-radius: 3px; 
         
    }
    .bar { 
        background-color: #B4F5B4; 
        width:0%; height:20px; 
        border-radius: 3px; 
    }
    .percent { 
        position:absolute; 
        display:inline-block; 
        top:-2px; left:48%; 
    }

    .row_for_save{
        display: none;
    }

    .dropzone_form2{
    	display: none;
    }

    .show_data_box2,.show_data_box1{
    	display: none;
    }

</style>
 
    <section class="contact-section padding_red" id="upload_page">
        <div class="container">
            <div class="row"> 
                <div class="col-12  ">
                    <div class="container">
                        
                        <section style="padding: 0px 16px;">
                            <div class="row"> 
                                <div class="col-8 dropzone_form1 " style="padding: 0px;">
                                    <div id="dropzonediv1">
                                        <form class="dropzone needsclick" id="form_zone_upload1"  >
                                            <div class="dz-message needsclick">    
                                                <span><?php echo  lang('Project_types8') ?></span> <br>
                                                  
                                            </div>
                                        </form> 
                                    </div> 
                                </div>


                                <div class="col-8 dropzone_form2 " style="    padding: 0px;">
                                    <div id="dropzonediv2">
                                        <form class="dropzone needsclick" id="form_zone_upload2"  >
                                            <div class="dz-message needsclick">    
                                                <span><?php echo  lang('Project_types8') ?></span> <br>
                                                  
                                            </div>
                                        </form> 
                                    </div> 
                                </div>




                                <div class="col-4  ">
                                    <div class="progress">
                                      <div class="progress-bar" style="width:<?php if(isset($percentage)){ echo $percentage; } ?>%"></div>
                                    </div>

                                    <?php if (isset($weekly_limit) and !empty($weekly_limit)): ?>
                                        <h4 style=" margin: 15px 0px;">Weekly <span style="float: right;"><?php echo $weekly_size; ?> of <?php echo $weekly_limit.''.$weekly_limit_name; ?></span></h4>
                                    <?php endif ?>
                                    

                                    <?php if (isset($total_limit) and !empty($total_limit)): ?>
                                        <h4>Total Limit <span style="float: right;"><?php echo $total_size; ?> of <?php echo $total_limit.''.$total_limit_name; ?></span></h4>
                                    <?php endif ?>
                                </div>
                            </div>
                        </section>

                        <br/>
                        
                        
                     

                        <form action="<?php echo base_url(); ?>upload/save_video_data" method="post"> 
                            <div class="row " style="margin-top: 50px"> 
                                <div class="col-6" style="margin-top: 30px">
                                    <div class="dropzone-previews1  youcancheck">
                                         
                                    </div> 
                                </div>
                                <div class="col-6 show_data_box1" >
                                    <div class="col-12 form-group">

                                    	<input type="hidden"   name="video_id[]" id="video_id1" value="">


                                        <label><?php echo  lang('Title') ?></label>
                                        <input type="text" name="title[]" id="name_file1" required="" class="form-control">
                                    </div>

                                    <div class="col-12 form-group">
                                        <label><?php echo  lang('Description') ?></label>
                                        <textarea  name="description[]" required class="form-control" rows="4"></textarea>
                                    </div>

                                    <div class="row">
                                        <div class="col-6 form-group">
                                            <label><?php echo  lang('Privacy') ?></label>
                                            <select required class="form-control show_me" name="privacy[]" >
                                                <option value="1">Anyone can see this video</option>
                                                <option value="2">Only i can see this video</option>
                                                <option value="3">Only people with private link</option>
                                                <option value="4">Only people with password can see see this video</option>
                                            </select>
                                        </div>
                                        <div class="col-6 form-group">
                                            <label>Tags <span style="font-size: 11px;">(Separated by commas, please!)</span></label>
                                            <input required type="text" name="tags[]" class="form-control">
                                        </div>
                                    </div>
                               

                                    <div class="row"> 
                                        <div class="col-6 form-group">
                                            <label><?php echo  lang('Language') ?></label>
                                            <select required class="form-control show_me" name="language[]">
                                                <option value="1">English</option>
                                                 
                                            </select>
                                        </div>
                                        <div class="col-6 form-group">
                                            <label>Download</label>
                                            <select required class="form-control show_me" name="download[]">
                                                <option value="allowed">Allowed</option>
                                                <option value="not">Not Allowed</option> 
                                            </select>
                                        </div>  
                                    </div> 

                                </div>
                            </div>



                            <div class="row add_hr_2"> 
                                <div class="col-6" style="margin-top: 30px">
                                    <div class="dropzone-previews2  youcancheck">
                                         
                                    </div> 
                                </div>
                                <div class="col-6 show_data_box2" >
                                    <div class="col-12 form-group">
                                    	<input type="hidden"   name="video_id[]" id="video_id2" value="">
                                        <label><?php echo  lang('Title') ?></label>
                                        <input type="text" name="title[]" id="name_file2"   class="form-control">
                                    </div>

                                    <div class="col-12 form-group">
                                        <label><?php echo  lang('Description') ?></label>
                                        <textarea  name="description[]"   class="form-control" rows="4"></textarea>
                                    </div>

                                    <div class="row">
                                        <div class="col-6 form-group">
                                            <label><?php echo  lang('Privacy') ?></label>
                                            <select   class="form-control show_me" name="privacy[]" >
                                                <option value="1">Anyone can see this video</option>
                                                <option value="2">Only i can see this video</option>
                                                <option value="3">Only people with private link</option>
                                                <option value="4">Only people with password can see see this video</option>
                                            </select>
                                        </div>
                                        <div class="col-6 form-group">
                                            <label>Tags <span style="font-size: 11px;">(Separated by commas, please!)</span></label>
                                            <input   type="text" name="tags[]" class="form-control">
                                        </div>
                                    </div>
                               

                                    <div class="row"> 
                                        <div class="col-6 form-group">
                                            <label><?php echo  lang('Language') ?></label>
                                            <select   class="form-control show_me" name="language[]">
                                                <option value="1">English</option>
                                                 
                                            </select>
                                        </div>
                                        <div class="col-6 form-group">
                                            <label>Download</label>
                                            <select   class="form-control show_me" name="download[]">
                                                <option value="allowed">Allowed</option>
                                                <option value="not">Not Allowed</option> 
                                            </select>
                                        </div>  
                                    </div> 

                                </div>
                            </div>

                            <div class="row row_for_save">
                                <div class="col-12 form-group" style="text-align: center; margin-top: 4%;">
                                     
                                    <input type="submit"  value="<?php echo  lang('Save') ?>" class="btn btn-primary vinheo_green_button" id='save_video_all' name="valide_login"   >
                                </div>
                            </div>
                             
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
 


<?php $this->load->view('user/layout/footer'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/dropzone.js"></script>

<script type="text/javascript">
    Dropzone.autoDiscover = false;
   

 
    var myDropzone = new Dropzone("#form_zone_upload1", 
    {   
        url: "<?php echo base_url(); ?>upload/save_video",
        parallelUploads: 1, 
        thumbnailHeight: 120,
        thumbnailWidth: 120,
        acceptedFiles: "video/*",
        addRemoveLinks: true,
        createImageThumbnails: true,
        filesizeBase: 1000,
        maxFiles: 1,   
        timeout: 1800000000,
        previewsContainer: ".dropzone-previews1",
        dictRemoveFileConfirmation: "Are you sure you want to remove this video?", 
         
        init: function() {
            this.on("addedfile", function(file) 
            {    
                $('#name_file1').val(file.name);  

                $('.dropzone_form1').hide();
                $('.row_for_save').show();
                $('.show_data_box1').show();
                $('.dropzone_form2').show();
                 
            });

             

        },
         
        success: function(file, response)
        {
            var data = JSON.parse(response);  
             
            $('#video_id1').val(data.video_id);   
            $('.dz-size span').html();
             
        },
        error: function (file, response) {
            if (file.status == 'canceled') 
            {
                $('.dropzone_form1').show();

                $('.show_data_box1').hide();
                $('.dropzone_form2').hide();

                // $('.show_data_box2').hide(); 
            }
             
            alert(response)
        }
    });


    //2nd queue
    var uploaded = 'no';
    var myDropzone = new Dropzone("#form_zone_upload2", 
    {   
        url: "<?php echo base_url(); ?>upload/save_video",
        parallelUploads: 1, 
        thumbnailHeight: 120,
        thumbnailWidth: 120,
        acceptedFiles: "video/*",
        addRemoveLinks: true,
        createImageThumbnails: true,
        filesizeBase: 1000,
        maxFiles: 1,   
        timeout: 1800000000,
        previewsContainer: ".dropzone-previews2",
        dictRemoveFileConfirmation: "Are you sure you want to remove this video?", 
        init: function() {
            var myDropZone = this;
            myDropZone.on('maxfilesexceeded', function(file) 
            {
                myDropZone.removeFile(file);
            });
            this.on("addedfile", function(file) 
            {    

            	uploaded = 'yes';
                $('#name_file2').val(file.name);  
 
                $('.add_hr_2').before('<hr>'); 
 
                $('.show_data_box2').show(); 
                $('.dropzone_form2').show(); 
            });  
        },
         
        success: function(file, response)
        {
            var data = JSON.parse(response);  
             
            $('#video_id2').val(data.video_id);   
            $('.dz-size span').html();
             
        },
        error: function (file, response) {

            if (file.status == 'canceled') 
            {
                $('.show_data_box2').hide(); 
            }
             
            alert(response)
        }
    });




    
    window.setInterval(function()
    {

        if ($('.dz-upload').width() ) 
        {
            if (  $('.dz-upload').width() > 100) 
            {
                $('#progress_bar_sow').text('Upload Complete!');
            } else{
                $('#progress_bar_sow').text($('.dz-upload').width()+'% uploaded');
                // $('.dz-progress').css("width", $('.dz-upload').width()+'%'); 
            }
            
        }
        
    }, 2000);    
    

    $('#save_video_all').on('click',function(e){
        
        if($('#video_id1').val() == '')
        {
            alert('Please make sure video is uploaded.');
            e.preventDefault();
        } 

        
    });

    
    
    

</script>

<script type="text/javascript">
    $('body').on('click', '.dz-remove',function(e){
        var result = confirm("Are you sure you want to delete?");
        if(!result) {
            e.preventDefault();
        }
        total_videos_until--;
    });
</script>

