<?php $this->load->view('user/layout/header'); 

$ci=& get_instance(); 

$ci->load->model('CommonModel'); 
?>
<style type="text/css">
    

.pagination-style-one a { padding: 7px; background: #31ABDE; color: #ffffff; border-radius: 50%; box-shadow: 0px 5px 10px 0px rgba(0,0,0,.1); margin: 0px 2px;}
.pagination-style-one a.selected, .pagination-style-one a:hover, .pagination-style-one a:active, .pagination-style-one a:focus { padding: 12px; box-shadow: 0px 5px 10px 0px rgba(0,0,0,.5);}
.pagination{
    display: block;
}
.overlay_status{
    position: absolute;
    bottom: 0;
}
.overlay_edit_del{
    position: absolute;
    right: -12px; 
    top: 3px;
}
.overlay_status p{
    background: #28a7dc;
    color: #fff;
    font-size: 11px;
    padding: 0 13px;
}
.overlay_edit_del p
{
    color: #2e3c42;
    font-size: 11px;
    padding: 0 13px;  
} 


.price_img_box img{
    width: 100%;
}
 
</style>
<?php
 
function formatSizeUnits($size, $precision = 1, $show = "")
{
    $b = $size;
    $kb = round($size / 1024, $precision);
    $mb = round($kb / 1024, $precision);
    $gb = round($mb / 1024, $precision);

    if($kb == 0 || $show == "B") {
        return $b . " bytes";
    } else if($mb == 0 || $show == "KB") {
        return $kb . "KB";
    } else if($gb == 0 || $show == "MB") {
        return $mb . "MB";
    } else {
        return $gb . "GB";
    }
}

?>
<section class="contact-section  " style="padding: ">
	<div class="manage_video" style="background: #f9fcff url(<?php echo base_url() ?>assets/img/manage_video.jpg) no-repeat;background-size: 100% 100%;">
		<h2><?php echo  lang('Privacy4') ?></h2>
	</div>
	<div class=" ">
		
		<div class="row" id="tabs_layouts">
			<!-- <div class="col-md-3 mb-3 vertical_tab" style="" >
				<ul class="nav nav-pills flex-column" id="myTab" role="tablist">
					
					<li class="nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><?php echo  lang('Dashboard') ?></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><?php echo  lang('Manage_Videos') ?></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><?php echo  lang('Account') ?></a>
					</li>
				</ul>
			</div> -->
			
			
			
			<div class="col-sm-12 sort_box" >
                <form method="get" action="">
    				<div class="row">
                        
    					<div class="col-sm-6 left_box">
    						<label>Sort By</label>
    						<select name="sort_by"> 
    							<option value="" hidden="" disabled="" selected="">Select</option>
                                <option value="popular">Most popular(Coming Soon)</option>
    							<option value="oldest">Date added (oldest)</option>
    							<option value="newest">Date added (newest)</option>
    						</select>
    					</div>
    					<div class="col-sm-6 right_box">
    						<label><input type="submit"  value="Search"></label>
    						<input type="text" name="search">
    					</div>
                        
    				</div>
                </form>
				<div class="row inner_layoutss">
					<div class="col-sm-12 ">
					 	<?php if ($this->session->flashdata('success')): ?> 
						     <div class="alert alert-success alert-dismissible " role="alert">
						            <?php echo $this->session->flashdata('success'); ?>
						            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						                  <span aria-hidden="true">&times;</span>
						            </button>
						     </div> 
						<?php endif ?>

						<?php if ($this->session->flashdata('error')): ?>  
                            <div class="alert alert-warning alert-dismissible " role="alert">
					            <?php echo $this->session->flashdata('error'); ?>
					            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					                  <span aria-hidden="true">&times;</span>
					            </button>
					        </div> 
						<?php endif ?>
					</div>
                    <?php if(!empty($all_videos)): ?>
                     
                        <?php foreach ($all_videos as $key => $value): ?> 
        					<div class="col-sm-4 listing_box">
                				<a href="<?php echo base_url(); ?>upload/detail/<?php echo $value->video_id; ?>">
                					<div class="price_img_box">
                                        <?php  $thumbnails = $ci->CommonModel->getAll('video_thumbnails', ' video_id ="'.$value->video_id.'" order by id desc limit 3 '); ?>

                                        <?php if (isset($thumbnails[0]) and $thumbnails[0]->base_img == 'no'): ?> 
                                            <img src="<?php echo base_url(); ?>assets/video_thumnails/<?php echo $this->session->userdata('user_id') ?>/<?php echo $thumbnails[0]->thumbnail; ?>"  > 
                                        <?php elseif(isset($thumbnails[0]->base_img) and $thumbnails[0]->base_img == 'yes'): 
                                            ?>   
                                            <img src="<?php echo base_url(); ?><?php echo $thumbnails[0]->thumbnail; ?>"  > 

                                        <?php else: ?>
                                            <img src="http://www.marcusirwin.org/userimages/NoImageAvailable.jpg" alt="">
                                        <?php endif; ?> 
 
                                        
        								<div class="overlay_date">
            	      					    <p> <?php echo formatSizeUnits($value->video_size*1024); ?></p>
                      					</div>

                                        <div class="overlay_status">
                                            <p> <?php echo  ucfirst($value->status); ?></p>
                                        </div>

                                        <div class="overlay_edit_del">
                                            <p> <a href="<?php echo base_url(); ?>videoedit/edit_video/<?php echo $value->video_id; ?>"><i class="fa fa-pencil btn btn-info"></i> </a>
                                             <a  onclick="return confirm('Are you sure you want to delete this video?');" href="<?php echo base_url(); ?>videoedit/delete_video/<?php echo $value->video_id; ?>"><i class="fa fa-trash btn btn-danger" ></i></a>
                                            </p>
                                        </div>
                  					</div>
            			        </a>

                                <div class="food_info">
                					
                					<h5 style="    white-space: nowrap;">
                                        <?php if (!empty($value->title)): ?>
                                            <?php echo $value->title; ?>
                                        <?php else: ?>
                                            <?php echo $value->video_name; ?>
                                        <?php endif; ?>               
                                    </h5>
                                    <hr>
                              		<div class="row" >
                                        <a href="#">
                        					<div class="col-sm-2 avtar_img">
                                                <?php  if (isset($value->profile_pic) and !empty($value->profile_pic)): ?> 
                                                    <img src="<?php echo base_url().'assets/img/user_profiles/'.$value->user_id.'/'.$value->profile_pic; ?>" style='width: 100%;    height: 45px;' alt="">
                                                <?php else: ?>
                                                    <img  style='width: 100%;    height: 45px;' src="https://d1.globalitvision.com/TravelFeastWith//assets/images/profiles/avatar2.png" alt="">
                                                <?php endif; ?>
                        						 
                        					</div>
                        					<div class="col-sm-10 rating">
                        						<h4>
                                                    <?php if (!empty($value->first_name)): ?>
                                                        <?php echo ucfirst($value->first_name); ?> 
                                                    <?php endif; ?> 

                                                    <?php if (!empty($value->last_name)): ?>
                                                        <?php echo ucfirst($value->last_name); ?> 
                                                    <?php endif; ?>   
                        						</h4>
                        						<i class="fa fa-star"></i>
                        						<i class="fa fa-star"></i>
                        						<i class="fa fa-star"></i>
                        						<i class="fa fa-star"></i>
                        						<i class="fa fa-star"></i>
                        					</div>
                                        </a>
            		  	            </div>
                                    
                				</div>

                                <div class="row" >
                                    <div class="col-sm-3" style="background-color: #0EF91E; text-align: center;"> 
                                        <a   style="cursor: pointer;" class="copyText" data-selection="linkvalue<?php echo $value->video_id; ?>" >
                                            <div style="display: none;">
                                                <input type="text" value="<?php echo base_url(); ?>upload/detail/<?php echo $value->video_id; ?>"  id='linkvalue<?php echo $value->video_id; ?>'>
                                            </div>
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                    </div> 
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-3"  style="background-color: #0EF91E; text-align: center;"> 
                                        <a   style="cursor: pointer;"  data-toggle="modal" data-target=".exampleModal" > 
                                            <i class="fa fa-share" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                			</div>
                        <?php endforeach; ?>
                            
                    <?php endif; ?>

                    <div class="col-sm-12 "> 
                        <?php if (isset($pagination)): ?>
                            <div class="pagination pagination-style-one  m-t-20" style="text-align: right;">
                                <?php echo $pagination ?>
                            </div>
                        <?php endif ?>
    	            </div>
				</div>  
			</div>
		</div> 
	</div>
</section>

<!-- Modal -->
<div class="modal fade exampleModal" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <img src="https://blog.hubspot.com/hs-fs/hub/53/file-953681786-png/social-icons-02-1.png">
                    </div>

                    <div class="col-sm-12">
                        <img src="https://blog.hubspot.com/hs-fs/hub/53/file-951108259-png/social-icons-01-2.png">
                    </div>


                    <div class="col-sm-12">
                        <img src="https://blog.hubspot.com/hs-fs/hub/53/file-951108319-png/social-icons-05.png">
                    </div>

                    <div class="col-sm-12">
                        <img src="https://blog.hubspot.com/hs-fs/hub/53/file-951108339-png/social-icons-03-1.png">
                    </div>

                    <div class="col-sm-12">
                        <img  src="https://blog.hubspot.com/hs-fs/hub/53/file-951108354-png/social-icons-04.png">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                 
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('user/layout/footer'); ?>

<script type="text/javascript">
    $('.copyText').on('click',function(){ 

        var selection = $(this).attr('data-selection');
        var copyText  = $('#'+selection).val();
        CopyToClipboard(copyText)
         
    });
    

    function CopyToClipboard (text) {
   
        if (window.clipboardData && window.clipboardData.setData) 
        {
            // IE specific code path to prevent textarea being shown while dialog is visible.
            return clipboardData.setData("Text", text); 

        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
            var textarea = document.createElement("textarea");
            textarea.textContent = text;
            textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
            document.body.appendChild(textarea);
            textarea.select();

        try {
            alert('Copied')
            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
        } catch (ex) {
            console.warn("Copy to clipboard failed.", ex);
            return false;
        } finally {
            document.body.removeChild(textarea);
        }
    }
}
</script>