<?php 
include('layout/header.php'); 
$CI = &get_instance();
$CI->load->model('CommonModelAdmin');
$setting = $CI->CommonModelAdmin->getsetting();
?>
<style type="text/css">
    .hide_btns{
      display: none;
    }
    /*.content:hover p {
      transform: scaleY(0.66)
    }

    .content:hover {
      transform: scaleY(1.5);
      transform-origin: bottom;
    }*/

    @media (max-width: 576px) {
          /* line 18*/
        .about_us_text {   
            padding: 50px 0px;
        }
    }
     
</style>


		<!-- banner part start-->
	<section class="breadcrumb_part breadcrumb_bg" style="background-image: url(<?php echo base_url().'assets/template_setting/'.$page_template->page_template_image; ?>) !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item">
                            <h2><?php echo $page_template->page_template_heading; ?></h2>
                            <p><?php echo $page_template->page_template_content; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section>
    	<div class="container">
    		<div class="row">
    			<div class="col-md-12 col-lg-12 text-center">
                    <div class="about_us_text">
                        <?php echo $body; ?> 
                    </div>
                </div>
    		</div>
    	</div>
    </section>


    <section class="pricing_data main_section_all" id="pricing_data_tab">

        <div class="container">

            <div class="row top_sticy ">

                <div class="col-lg-4 col-md-12 col-sm-12 pricing-set">

                    <h1>Choose a plan.</h1>

                    <p>Try any plan risk-free for 30 days. Or, start free with <a href="#">Vinheo Basic.</a></p>

                </div>

                <div class="col-lg-2  pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li><h3>Plus</h3></li>

                        <li>5GB/week</li>

                    </ul>

                </div>

                <div class="col-lg-2  pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li><h3>Pro</h3></li>

                        <li>20GB/week</li>

                    </ul>

                </div>

                <div class="col-lg-2  pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li><h3>Business</h3></li>

                        <li>No weekly limits</li>

                    </ul>

                </div>

                <div class="col-lg-2  pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li><h3>Premium</h3></li>

                        <li>Unlimited live streaming</li>

                    </ul>

                </div>

            </div>

        </div>

    </section>

    <section class="pricing_data_set main_section_all">

        <div class="container">

            <div class="row top_sticy pricing_data_234">

                <div class="col-lg-4  pricing-set">

                    

                </div>

                <div class="col-lg-2  pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li>250GB every year</li>

                        <li>Single user</li>

                        <li><span>$<?php echo $setting->plus; ?></span>/mo</li>

                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown_1"

                            role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                            Enterprise

                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="services.html">$6  per month, billed annually (save 40%)</a>

                            <a class="dropdown-item" href="dep.html">$10 per month, billed monthly</a>

                        </div>

                    </li>

                    <li><a href="#" class="pricing_ddr" style="padding: 6px 15px;">Get Plus</a></li>
                    <li class=" "><a href="#" style="padding: 7px 0px;width: 72%; font-weight: 200; color: blue" class=" ">or Start Free Trial</a></li>

                </ul>

                </div>

                <div class="col-lg-2  pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li>1TB every year</li>

                        <li>3 team members</li>

                        <li><span>$<?php echo $setting->pro; ?></span>/mo</li>

                        <li>billed annually</li>

                        <li><a href="#" class="bbusiness_set" style="padding: 6px 15px;">Get Pro</a></li>
                        <li class=" "><a href="#" style="padding: 7px 0px;width: 72%; font-weight: 200; color: blue" class=" ">or Start Free Trial</a></li>

                    </ul>

                </div>

                <div class="col-lg-2  pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li>5TB total storage</li>

                        <li>10 team members</li>

                        <li><span>$<?php echo $setting->business; ?></span>/mo</li>

                        <li>billed annually</li>

                        <li><a href="#" class="bbusiness" style="padding: 6px 15px;">Get Business</a></li>
                        <li class=" "><a href="#" style="padding: 7px 0px;width: 72%; font-weight: 200; color: blue" class=" ">or Start Free Trial</a></li>

                    </ul>

                </div>

                <div class="col-lg-2  pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li>7TB total storage</li>

                        <li>Unlimited live viewers</li>

                        <li><span>$<?php echo $setting->premium; ?></span>/mo</li>

                        <li>Unlimited live viewers</li>

                        <li><a href="#" class="bbusiness_set_last" style="padding: 6px 15px;">Get Premium</a></li>
                        <li class=" "><a href="#" style="padding: 7px 0px;width: 72%; font-weight: 200; color: blue" class=" ">or Start Free Trial</a></li>

                        

                    </ul>

                </div>

            </div>

        </div>

    </section>


    <section class="pricing_222 main_section_all">

        <div class="container">

            <div class="row top_sticy pricing_22234">

                <div class="col-lg-4  pricing-set">

                    

                </div>

                <div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li>Player customization</li>

                        <li>Privacy controls</li>

                        <li>Social distribution</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li>Review and approval</li>

                        <li>Private team projects</li>

                        <li>Customizable Showcase sites</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li>Player calls-to-action</li>

                        <li>Lead generation</li>

                        <li>Engagement graphs</li>

                        <li>Google Analytics</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li>Unlimited live events</li>

                        <li>Live stream to multiple destinations</li>

                        <li>Live Q&A, graphics and polls</li>

                        <li>Audience chat</li>

                        

                    </ul>

                </div>

            </div>

        </div>

    </section>

    <section class="pricing_222 main_section_all">

        <div class="container">

            <div class="row top_sticy pricing_22234">

                <div class="col-lg-4 pricing-set">

                    <a href="#">Video Player <i class="fa fa-angle-right"></i></a>

                </div>

                <div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                        

                    </ul>

                </div>

            </div>

        </div>

    </section>

    <section class="pricing_222 main_section_all">

        <div class="container">

            <div class="row top_sticy pricing_222343">

                <div class="col-lg-4 pricing-set">

                    

                </div>

                <div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li>Embed anywhere</li>

                        <li>Customize video player <i class="fa fa-angle-right"></i></li>

                        <li>Custom end screens</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li>Embed anywhere</li>

                        <li>Customize colors & components</li>

                        <li>Customize end-screens</li>

                        <li>Add your logo</li>

                        <li>Playback speed control</li>

                        <li>Third-party player support</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li>Embed anywhere</li>

                        <li>Customize colors & components</li>

                        <li>Customize end-screens</li>

                        <li>Add your logo</li>

                        <li>Playback speed control</li>

                        <li>Third-party player support</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li>Embed anywhere</li>

                        <li>Customize colors & components</li>

                        <li>Customize end-screens</li>

                        <li>Add your logo</li>

                        <li>Playback speed control</li>

                        <li>Third-party player support</li>

                        

                    </ul>

                </div>

            </div>

        </div>

    </section>

    <section class="pricing_222 main_section_all">

        <div class="container">

            <div class="row top_sticy pricing_22234">

                <div class="col-lg-4 pricing-set">

                    <a href="#">Privacy <i class="fa fa-angle-right"></i></a>

                </div>

                <div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                        

                    </ul>

                </div>

            </div>

        </div>

    </section>

    <section class="pricing_222 main_section_all">

        <div class="container">

            <div class="row top_sticy pricing_22234">

                <div class="col-lg-4 pricing-set">

                    <a href="#">Collaborations <i class="fa fa-angle-right"></i></a>

                </div>

                <div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                        

                    </ul>

                </div>

            </div>

        </div>

    </section>

    <section class="pricing_222 main_section_all">

        <div class="container">

            <div class="row top_sticy pricing_22234">

                <div class="col-lg-4 pricing-set">

                    <a href="#">Distribution & marketing <i class="fa fa-angle-right"></i></a>

                </div>

                <div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                        

                    </ul>

                </div>

            </div>

        </div>

    </section>

    <section class="pricing_222 main_section_all">

        <div class="container">

            <div class="row top_sticy pricing_22234">

                <div class="col-lg-4 pricing-set">

                    <a href="#">Analytics <i class="fa fa-angle-right"></i></a>

                </div>

                <div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                        

                    </ul>

                </div>

            </div>

        </div>

    </section>

    <section class="pricing_222 main_section_all">

        <div class="container">

            <div class="row top_sticy pricing_22234">

                <div class="col-lg-4 pricing-set">

                    <a href="#">Live streaming <i class="fa fa-angle-right"></i></a>

                </div>

                <div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                        

                    </ul>

                </div>

            </div>

        </div>

    </section>

    <section class="pricing_222 main_section_all">

        <div class="container">

            <div class="row top_sticy pricing_22234">

                <div class="col-lg-4 pricing-set">

                    <a href="#">Priority support <i class="fa fa-angle-right"></i></a>

                </div>

                <div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                        

                    </ul>

                </div>

            </div>

        </div>

    </section>

    <section class="pricing_222 main_section_all">

        <div class="container">

            <div class="row top_sticy pricing_22234 pricing_2223434">

                <div class="col-lg-4 pricing-set">

                    <a href="#">Earn money <i class="fa fa-angle-right"></i></a>

                </div>

                <div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                    </ul>

                </div>

                <div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

                    <ul>

                        <li>Unlimited bandwidth in the Vinheo player</li>

                        <li>4K & HDR support</li>

                        <li>No ads before, after, or on your video</li>

                        

                    </ul>

                </div>

            </div>

        </div>

    </section>

	
<?php include('layout/footer.php'); ?>
	
	 <section style=" position: fixed; left: 0; bottom: 0;width: 100%;    top: unset;    background-color: rgb(142, 142, 142);" class="pricing_data content"  >

        <div class="container">

            <div class="row top_sticy ">

                <div class="col-lg-4 col-md-12 col-sm-12 pricing-set">

                    <h1>Choose a plan.</h1>

                    <p  class="hide_btns" style="    color: white;">Try any plan risk-free for 30 days. Or, start free with <a href="#" style="color: blue">Vinheo Basic.</a></p>

                </div>

                <div class="col-lg-2  pricing-one border_top_1" style="padding-left: 0px !important;" id="border_top_1">

                    <ul>
                    	<li><a href="#" style="padding: 7px 10px;width: 72%; font-weight: 200;background-image: linear-gradient(#3bf948, #22a22b) !important;color: black;" class="pricing_ddr ">Get Plus</a></li>
                        	
                    	<li class="hide_btns"><a href="#" style="padding: 7px 0px;width: 72%; font-weight: 200; color: blue" class=" ">or Start Free Trial</a></li>
                        <li class="hide_btns">5GB/week</li>
                        <li class="hide_btns">250GB every year</li>
                        <li class="hide_btns">$<?php echo $setting->plus; ?>/mon</li> 
                    </ul> 
                </div>

                <div class="col-lg-2  pricing-one border_top_2" style="padding-left: 0px !important;" id="border_top_2">

                    <ul>
                    	<li><a href="#" style="padding: 7px 10px;width: 72%;font-weight: 200;background-image: linear-gradient(#3bf948, #22a22b) !important;color: black;" class="bbusiness_set ">Get Pro</a></li> 
                        
                    	<li class="hide_btns"><a href="#" style="padding: 7px 0px;width: 72%;font-weight: 200; color: blue" class=" ">or Start Free Trial</a></li>
                        <li class="hide_btns">20GB/week</li>
                        <li class="hide_btns">1TB every year</li>
                        <li class="hide_btns">$<?php echo $setting->pro; ?>/mon</li>

                        

                    </ul>
  
                </div>

                <div class="col-lg-2  pricing-one border_top_3" style="padding-left: 0px !important;" id="border_top_3">

                    <ul>
                    	<li><a href="#" style="padding: 7px 10px;width: 72%;    font-weight: 200;background-image: linear-gradient(#3bf948, #22a22b) !important;color: black;" class="bbusiness ">Get Business</a></li>
                       
                    	<li  class="hide_btns"><a href="#" style="padding: 7px 0px;width: 72%; font-weight: 200; color: blue" class=" ">or Start Free Trial</a></li>
                        <li  class="hide_btns">No weekly limits</li>
                        <li  class="hide_btns">5TB total storage </li>
                        <li  class="hide_btns">$<?php echo $setting->business; ?>/mon</li>

                        
                    </ul>
                    
                </div>

                <div class="col-lg-2  pricing-one border_top_4" style="padding-left: 0px !important;" id="border_top_4">

                    <ul>
                    	<li ><a style="padding: 7px 10px;width: 72%;background-image: linear-gradient(#3bf948, #22a22b) !important;    font-weight: 200;color: black;" href="#" class="bbusiness_set_last ">Get Premium</a></li> 
                    	<li class="hide_btns "><a href="#" style="padding: 7px 0px;width: 72%; font-weight: 200;color: blue">or Start Free Trial</a></li> 



                        <li class="hide_btns "> live streaming</li>
                        <li class="hide_btns "> 7TB total storage</li>
                        <li class="hide_btns "> $<?php echo $setting->premium; ?>/mon</li>
                    </ul> 
                     
                </div>

            </div>

        </div>

    </section>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('mouseenter', '.content', function () {
            $(this).find(".hide_btns").show();
        }).on('mouseleave', '.content', function () {
            $(this).find(".hide_btns").hide();
        });

        $(document).on('mouseenter', '.main_section_all', function () {
            $(".content").hide();
        }).on('mouseleave', '.main_section_all', function () {
            $(".content").show();
        });
    }); 

</script>