<?php include('layout/header.php'); 

$CI = &get_instance();

$CI->load->model('CommonModelAdmin');

$about_us = $CI->CommonModelAdmin->getPagesTemplates();

?>







		<!-- banner part start-->

	<section class="breadcrumb_part breadcrumb_bg">

        <div class="container">

            <div class="row">

                <div class="col-lg-12">

                    <div class="breadcrumb_iner">

                        <div class="breadcrumb_iner_item">

                            <h2><?php echo $about_us->banner_heading; ?></h2>

                            <p><?php echo $about_us->banner_detail; ?></p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>





    <section>

    	<div class="container">

    		<div class="row">

    			<div class="col-md-12 col-lg-12 text-center">

                    <div class="about_us_text">

                      <h2><?php echo $about_us->about_us_heading; ?></h2>

                       <?php echo $about_us->about_us_detail; ?>

                    </div>

                </div>

    		</div>

    	</div>

    </section>

	

<?php include('layout/footer.php'); ?>

	

	 