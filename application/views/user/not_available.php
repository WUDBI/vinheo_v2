<?php include('layout/header.php'); ?>



		<!-- banner part start-->
	<section class="breadcrumb_part breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item">
                            <h2><?php echo $this->lang->line('vinheo_info'); ?></h2>
                            <p><?php echo $this->lang->line('vinheo_info2'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section>
    	<div class="container">
    		<div class="row">
    			<div class="col-md-12 col-lg-12 text-center">
                    <div class="about_us_text">
                      <h2>Plan not available.</h2>
                      <p>Sorry, this plan is not yet available, we are working to make it available soon. Please apologize for the inconvenience. Thank you</p>
                        <p><a href="https://vinheo.com/home/pricing"><strong>Go to prices</strong></a></p>
                    </div>
                </div>
    		</div>
    	</div>
    </section>
	
<?php include('layout/footer.php'); ?>
	
	 