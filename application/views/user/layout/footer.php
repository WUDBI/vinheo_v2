<?php 
    $CI = &get_instance();
    $CI->load->model('CommonModelAdmin');
    $setting = $CI->CommonModelAdmin->getsetting();
 ?>


</div>
<footer class="footer-area">

	<div class="footer section_padding"  >

		<div class="container">

			<div class="row justify-content-between">

				<div class="col-xl-2 col-md-4 col-sm-6 single-footer-widget">

					<a href="<?php echo base_url(); ?>home" class="footer_logo"> <img src="<?php echo base_url(); ?>assets/img/logo--setfooter.png" alt="#" style='max-width: 100%; height: auto;'> </a>

				</div>

				<div class="col-xl-2 col-sm-6 col-md-4 single-footer-widget">

					<h4>Vinheo</h4>

					<ul>

						<li><a href="<?php echo base_url(); ?>home/pricing">Pricing</a></li>

						<li><a href="<?php echo base_url(); ?>upload/upload_video">Upload</a></li>

						<li><a href="<?php echo base_url(); ?>home/privacy">Privacy</a></li>

					  	<li><a href="<?php echo base_url(); ?>home/cookies">Cookies</a></li>

						<li><a href="<?php echo base_url(); ?>home/terms">Terms and Conditions</a></li>

						<li><a href="<?php echo base_url(); ?>home/copyright">Copyright Policy</a></li>

					</ul>

				</div>

				<div class="col-xl-2 col-sm-6 col-md-4 single-footer-widget">

					<h4>Features</h4>

					<ul>

						  <li><a href="#">Video Player</a></li> 

						  <li><a href="#">Collaboration</a></li>

						  <li><a href="#">Distribution & Marketing</a></li>

						  <li><a href="#">Monetization</a></li>

						  <li><a href="#">Live Streaming</a></li>

						  <li><a href="#">Analytics</a></li>

						  <li><a href="#">Hosting & Management</a></li>

						  <li><a href="#">Enterprise</a></li>

						</ul>

				</div>

				<div class="col-xl-2 col-sm-6 col-md-6 single-footer-widget">

					<h4>Apps</h4>

					<ul>

						<li><a href="#">macOS</a></li>

						<li><a href="#">IOS</a></li>

						<li><a href="#">Android</a></li>

					</ul>

				</div>

				<div class="col-xl-2 col-sm-6 col-md-6 single-footer-widget">

					<h4>Resources</h4>

					<ul>

					  <li><a href="<?php echo base_url(); ?>home/contact">Help Center</a></li>
 

					  <li><a href="#">OTT Resources</a></li>

					  <li><a href="#">Developers</a></li>

					  <li><a href="#">Students</a></li>

					  <li><a href="#">Guidelines</a></li>

					</ul>

					

				</div>

				<div class="col-xl-2 col-sm-6 col-md-6 single-footer-widget">

					<h4>Companys</h4>

					<ul>

					  	<li><a href="<?php echo base_url(); ?>user/home/about">About</a></li>

					  	<li><a href="#">Jobs</a></li>

					  	<li><a href="#">Partners</a></li> 
					</ul> 

				</div>

			</div>

		</div>

	</div>

	<div class="copyright_part">

		<div class="container">

			<div class="row align-items-center">

				<p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

					Copyright &copy; <?php echo Date('Y'); ?> All rights reserved

					

				</p>

				<div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">

					<a href="<?php echo $setting->social_fb; ?>"><i class="ti-facebook"></i></a>

					<a href="<?php echo $setting->social_twitter; ?>"> <i class="ti-twitter"></i> </a>

					<a href="<?php echo $setting->social_insta; ?>"><i class="ti-instagram"></i></a>

					<a href="<?php echo $setting->social_gplus; ?>"><i class="ti-skype"></i></a>

				</div>

			</div>

		</div>

	</div>

</footer>

<!-- footer part end-->

<!-- jquery plugins here-->

<script src="<?php echo base_url(); ?>assets/js/jquery-1.12.1.min.js"></script>

<!-- popper js -->

<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>

<!-- bootstrap js -->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- owl carousel js -->

<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.nice-select.min.js"></script>

<!-- contact js -->

<script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>

<script src="<?php echo base_url(); ?>assets/js/contact.js"></script>

<!-- custom js -->

<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

</body>

</html>

<script src="https://apis.google.com/js/platform.js" async defer></script>
<script>
  function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      
    });
  }
</script>