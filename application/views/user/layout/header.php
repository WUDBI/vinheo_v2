<!DOCTYPE html>

<html lang="en">

	<head>
		<meta name="google-signin-client_id" content="428453813549-g0j7f8dbd3f0rd3362it947hthijd88i.apps.googleusercontent.com">
		<!-- Required meta tags -->

		<meta charset="utf-8">

		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title id="progress_bar_sow">   Vinheo</title>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png">

		<!-- Bootstrap CSS -->

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">

		<!-- animate CSS -->

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">

		<!-- owl carousel CSS -->

		<link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500i,600,700,900&display=swap" rel="stylesheet">

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">

		<!-- themify CSS -->

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">

		<!-- flaticon CSS -->

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flaticon.css">

		<!-- magnific popup CSS -->

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">

		<!-- nice select CSS -->

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/nice-select.css">

		<!-- swiper CSS -->

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">

		<!-- style CSS -->

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">

	</head>

	<body>



<style type="text/css">

	.select_lang .nice-select {

		padding-top: 0px !important;

		margin-left:  5px !important;

		line-height: 28px !important;

		height: 29px !important;

	}
	.select_lang select{
		display: block !important;
	}

	.banner_part .banner_text h1 
	{
    	font-size: 49px !important; 
    	font-weight: 600 !important;
    
	}
	.row {
	    margin-right: 0 !important; 
	    margin-left: 0 !important; 
	}
	.navbar-expand-lg .navbar-nav .dropdown-menu {
	    position: absolute;
	    left: -78px !important;
	    top: 55px !important;
	}

	.padding_topper{
		padding-top: 67px !important;
	}	

</style>

 

<header class="main_menu home_menu"> 
	<div class="row" style="width: 100%;">

		<div class="col-lg-12 top_set_padd">

			<nav class="navbar navbar-expand-lg navbar-light">

				<a class="navbar-brand" href="<?php echo base_url(); ?>home"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="logo"> </a>

				<button class="navbar-toggler" type="button" data-toggle="collapse"

				data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"

				aria-expanded="false" aria-label="Toggle navigation">

				<span class="navbar-toggler-icon"></span>

				</button>

				<div class="collapse navbar-collapse main-menu-item"

					id="navbarSupportedContent">



					<ul class="navbar-nav mr-auto">



						<?php if($this->session->userdata('user_id')): ?>

							<li class="nav-item dropdown">

					          	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

					            	Manage videos

					          	</a>

					          	<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="    left: 7px !important;">

					            	<a style="padding: 5px 18px !important;" class="dropdown-item" href="<?php echo base_url(); ?>upload/all_videos">Videos</a> 

					          	</div>

					        </li>

						<?php endif ?>



					</ul>







					<ul class="navbar-nav ml-auto">



						<?php if(!$this->session->userdata('user_id')): ?>

							<li class="nav-item">

								<a class="nav-link set_join" href="<?php echo base_url(); ?>home/signup" class="btn btn-primary"><?php echo  lang('join') ?></a>

							</li>

						<?php endif ?>



						<?php if(!$this->session->userdata('user_id')): ?> 

						

							<li class="nav-item active">

								<a class="nav-link" href="<?php echo base_url(); ?>home/login"><?php echo  lang('Log_in') ?></a>

							</li>



						<?php endif ?>



						<li class="nav-item">

							<a class="nav-link" href="<?php echo base_url(); ?>home/about"><?php echo  lang('About') ?></a>

						</li>

						 

						<li class="nav-item">

							<a class="nav-link" href="<?php echo base_url(); ?>home/pricing"><?php echo  lang('Pricing') ?></a>

						</li>

						 



					 

							<li class="nav-item">

								<a class="nav-link set_upload" style="    padding: 0px;" href="<?php echo base_url(); ?>upload/upload_video" class="btn btn-primary"><img src="<?php echo base_url(); ?>assets/img/upload.png"></a>										

							</li>

						 
 



						<li class="nav-item select_lang">



					     	<select class="form-control" style="display: block;" onchange="javascript:window.location.href='<?php echo base_url(); ?>MultiLanguageSwitcher/switch/'+this.value;"> 

						        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?> > English </option>
 

						        <option value="spanish" <?php if($this->session->userdata('site_lang') == 'spanish') echo 'selected="selected"'; ?> > Spanish </option> 
 

					        </select>

						</li>





						<?php if($this->session->userdata('user_id')): ?>
 										
							<?php 
							$user_id = $this->session->userdata('user_id');
							$ci=& get_instance();
							$ci->load->model('CommonModel');
							$userdata = $ci->CommonModel->getActiveUser( $this->session->userdata('user_id') );
							?>
							<li class="nav-item dropdown">

					          	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  style=' padding: 8px 0px;'>


					          		<?php if (!empty($userdata->profile_pic)): ?>
					          			<img src="<?php echo base_url().'assets/img/user_profiles/'.$user_id.'/'.$userdata->profile_pic; ?>" width="22" height="22" class="rounded-circle"  >
					          		<?php else: ?>
					          			<img src="<?php echo base_url().'assets/img/avatar2.png'; ?>" width="22" height="22" class="rounded-circle">


					          		<?php endif; ?>

					            	

					          	</a>

					          	<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

					            	<a class="dropdown-item" href="#">Dashboard</a>

					            	<a class="dropdown-item" href="<?php echo base_url(); ?>/profile/user_profile">Edit Profile</a> 

					            	<a class="dropdown-item" href="<?php echo base_url(); ?>home/logout" onclick="return signOut();"><?php echo  lang('Logout') ?></a>

					            	

					          	</div>

					        </li>

						<?php endif ?>

					</ul>

				</div>

				

			</nav>

		</div> 
	</div>  
</header>

<div class="padding_topper">
	
