<?php include('layout/header.php'); ?>




<style type="text/css">

	.center_me{

		background: #fff;

	    border-radius: 5px;

	    border-top: 5px solid #0EF91E;

	    margin: 0 auto;

	    border-bottom: 1px solid #ccc;

	    border-left: 1px solid #ccc;

	    border-right: 1px solid #ccc;

	    box-shadow: 0 0 7px 0 #ccc;

	    width: 381px;

	    padding: 20px 9px;

	    

	}

	.contact-title{

		text-align: center;

	}
	.abcRioButtonLightBlue{
		width: 100% !important;
	}

	.abcRioButtonContentWrapper{
		color: #fff !important;
    	background-color: #17a2b8 !important;
    	border-color: #17a2b8 !important;
    	border-radius: 3px;
	}
	.abcRioButtonLightBlue svg{
		display: none;
	}
	.abcRioButtonContents span{
		display: inline-block;
	    font-weight: 400;
	    /* color: #212529; */
	    text-align: center;
	    vertical-align: middle;
	    /* -webkit-user-select: none; */
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    background-color: transparent;
	    border: 1px solid transparent;
	    padding: .375rem .75rem;
	    font-size: 1rem;
	    line-height: 1.5;
	    border-radius: .25rem;
	    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}

	.fb_register{
		width: 100%; background-image: linear-gradient(#3c66c4, #1e3569); border: none; color: #fff; font-weight: bold;  padding: 10px 0; border-radius: 35px;font-weight: 400;text-transform: capitalize;
	}

	.login_btn{
		width: 100% !important; 
		background-image: linear-gradient(#3bf948, #22a22b) !important; 
		border: none !important; 
		color: #fff !important; 
		font-weight: 400 !important; 
		padding: 10px 0 !important; 
		border-radius: 35px !important;
		text-transform: capitalize !important;
		font-size: 1rem !important;
	}

	@media (max-width: 576px) {
		  /* line 18*/
	  	.center_me {
		    width: 100%;
	  	}
		  
	}
</style>	









	<section class="contact-section padding_red">

		<div class="container">

			<div class="row">

				

			 	<div class="col-12  ">

			 		<div class="center_me" style="padding-bottom: 0px;">  

			 			<div class="row">

			 				<div class="col-12  ">

								<h2 class="contact-title"><?php echo  lang('Log_in') ?></h2>

							</div>

						</div>

						<div class="login_msg">
							
						</div>

						<?php if ($this->session->flashdata('error')): ?> 

					      	<div class="alert alert-warning alert-dismissible " role="alert">

					            <?php echo $this->session->flashdata('error'); ?>

					            <button type="button" class="close" data-dismiss="alert" aria-label="Close">

					                  <span aria-hidden="true">&times;</span>

					            </button>

					      	</div> 

					    <?php endif ?>





					    <?php if ($this->session->flashdata('success')): ?> 

					      	<div class="alert alert-success alert-dismissible " role="alert">

					            <?php echo $this->session->flashdata('success'); ?>

					            <button type="button" class="close" data-dismiss="alert" aria-label="Close">

					                  <span aria-hidden="true">&times;</span>

					            </button>

					      	</div> 

					    <?php endif ?>



				 		<form  action="<?php echo base_url() ?>home/validate_login" method="post" >

				 			<div class="row">

				 				<div class="col-12  form-group">

				 					<label><?php echo  lang('Email') ?></label>

				 					<input type="email" class="form-control" name="email" placeholder="<?php echo  lang('Email') ?>">

				 				</div>

				 			</div>



				 			<div class="row">

				 				<div class="col-12 form-group">

				 					<label><?php echo  lang('Password') ?></label>

				 					<input type="password"  class="form-control"  name="password" placeholder="<?php echo  lang('Password') ?>">

				 				</div>

				 			</div>





				 			<div class="row">

				 				<div class="col-12 form-group" style="margin-bottom: 9px;text-align: center;    margin-top: 4%;">
 

				 					<input type="submit"  value="Login" class="btn btn-primary login_btn" name="valide_login" >

				 				</div>

				 			</div>


				 			<div class="row"> 
				 				<div class="col-12 form-group" style="margin-bottom: 9px;text-align: center; ">  <button class="btn btn-info fb_register"  ><i class="fa fa-facebook"></i> Facebook</button> 
				 				</div> 
				 			</div>

				 			<div class="row"> 
				 				<div class="col-12 form-group" style="margin-bottom: 9px;text-align: center; ">
 									<div style="display: none;" class="g-signin2" data-onsuccess="onSignIn"></div>

				 				 
				 				</div> 
				 			</div>

				 			<div class="row">

				 				<div class="col-12 form-group" style="text-align: left;    margin-top: 4%;"> 

				 					<a style=" font-weight: 600; text-transform: capitalize;" href="<?php echo base_url(); ?>home/forget_password"><?php echo  lang('Forget_password') ?>?</a>

				 				</div>

				 			</div>

				 		</form>



			 		</div>

			 	</div>



			</div>

		</div>

	</section>

		



<?php include('layout/footer.php'); ?>
<script type="text/javascript">


	var ajaxURLPath = '<?php echo base_url(); ?>';
 
  jQuery.ajaxSetup({ cache: true });
  jQuery.getScript('https://connect.facebook.net/en_US/sdk.js', function()
  {
    FB.init({
      appId: '518927675378904',
      version: 'v2.1' // or v2.1, v2.2, v2.3, ...
    });     
    jQuery('.fb_login').removeAttr('disabled');
    FB.getLoginStatus(function(response) {
     
      if (response.status === 'connected') {
        	getFBProfile(function(profile) {
          
        });
      }
      
    });
    jQuery('body').on('click', '.fb_login', function(e) {
      e.preventDefault();
    
      FB.login(function(response){
         
        getFBProfile(function(profile) {
          
			var profile_pic = profile.picture.data.url;
			var first_name  = profile.first_name;
			var last_name   = profile.last_name;
			var email       = profile.email;
			if(email !='')
			{
	         	$.ajax({
		            url: ajaxURLPath+'home/fb_login',
		            method: 'POST',
		            data: {first_name:first_name,last_name:last_name,email:email,profile_pic:profile_pic},
		            success: function(result) 
	            	{
		               	if(result == 'logged_in'){
		                 	window.location.href=ajaxURLPath+'profile/user_profile';
		              	}else{
		                	$('.login_msg').html(result);
		              	}
                	}
	            });
          	}
          // activeLevel('level8');
        });
      }, {scope: 'email'});
    });

    jQuery('body').on('click', '.fb_register', function(e) {
      e.preventDefault();
    
      FB.login(function(response){
         
        getFBProfile(function(profile) {
          
			var profile_pic = profile.picture.data.url;
			var first_name = profile.first_name;
			var last_name = profile.last_name;
			var email = profile.email;
			if(email !=''){
	          	$.ajax({
		            url: ajaxURLPath+'home/fb_login',
		            method: 'POST',
		            data: {first_name:first_name,last_name:last_name,email:email,profile_pic:profile_pic},
		            success: function(result) 
		            {
	               		if(result == 'logged_in'){
		                 	window.location.href=ajaxURLPath+'profile/user_profile';
		              	}else{
		                	$('.login_msg').html(result);
		              	}
	                }
    			});
          	}
          // activeLevel('level8');
        });
      }, {scope: 'email'});
    });
  });

  function getFBProfile(callback) 
  {
      FB.api('/me?fields=id,first_name,last_name,picture.width(100).height(100),email', function(response) {
       var fname = response.first_name;
       var lname = response.last_name;
       var email = response.email;
       jQuery('#service_modal .fname').val(fname);
       jQuery('#service_modal .lname').val(lname);
       jQuery('#service_modal .user_email').val(email);
       // jQuery('#service_modal .customer-detials-box').show();
        jQuery('.service_modal .dir_btn.next').show();
        callback(response);

      });
  }

  	function onSignIn(googleUser) {
	  	var profile = googleUser.getBasicProfile();
	  	// console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	  
	  	var first_name  = profile.getName();
	  	var profile_pic = profile.getImageUrl();
	  	var email       = profile.getEmail();
  	 	window.onbeforeunload = function(e){
  			gapi.auth2.getAuthInstance().signOut();
		}
	  	$.ajax({
            url: ajaxURLPath+'home/fb_login',
            method: 'POST',
            data: {first_name:first_name ,email:email,profile_pic:profile_pic},
            success: function(result) 
        	{
               	if(result == 'logged_in'){
                 	window.location.href=ajaxURLPath+'profile/user_profile';
              	}else{
                	$('.login_msg').html(result);
              	}
        	}
        });
	}

	// $(document).ready()

	 
		
	  $(window).load(function() {
	  	$('.g-signin2').show();
	     $('.abcRioButtonContents').find('span:first-child').html('<i style="    margin-right: 45px;" class="fa fa-google-plus"> Gmail</i> '); 
	});
	 
 
</script>
 
 