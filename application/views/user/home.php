<?php 
	include('layout/header.php');  
    $CI = &get_instance();
    $CI->load->model('CommonModelAdmin');
    $setting = $CI->CommonModelAdmin->getsetting();
 ?>
 <style type="text/css">
 	#carouselExampleControls{
 		height: 100% !important;
 	}
 	img {
	    max-width: 100% !important;
	    height: auto !important;
	}
	.btn_left{
		float: left;
		background-color: white;
		color: black;
	}

	.btn_right{
		float: right;
		background-color: white;
		color: black;
	}

	.btn_left:hover ,.btn_right:hover
	{
		background-color: #0EF91E;
		color: white !important;
	}
	.padding_top{
		padding: 5px 0 !important;
	}

	
	@media (max-width: 576px) {
		  /* line 18*/
	  	.slider_overlay {   
		    padding: 27px 40px; 
		   
    		width: 85%;
		}
		.slider_overlay h1 {
	    	font-size: 17px;
		}
		.slider_overlay p {
	    	font-size: 11px;
		}
		.slider_overlay .btn  {
	    	float: none;
	    	margin: 9px;
	    	 
		}
		.slider_area img {
		    height: 100% !important;
		}
		.carousel-inner {
    		 
    		height: 100%;
    	}
    	.carousel-item {
    		 
    		height: 100%;
    	}
	}
 </style>
<?php 
	$slider_images = $CI->CommonModelAdmin->get_slider_images();
	$slider_number = $CI->CommonModelAdmin->get_slider_names(); 
?>
		<!-- banner part start-->
		<?php if (isset($slider_images) and !empty($slider_images)): ?>
			
		
		<section class="banner_part slider_area">

			<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
			  	<div class="carousel-inner">
 

			  		<?php if ($slider_number->image1 == 2): ?>
			  			<?php if (isset($slider_images[0]->file_name) and !empty($slider_images[0]->file_name)): ?>
						    <div class="carousel-item active">
						      	<img class="d-block w-100" src="<?php echo base_url(); ?>assets/slider/<?php echo $slider_images[0]->file_name; ?>" alt="Second slide">

						      	
						    </div>
					    	 
						<?php endif ?> 
			  		<?php endif ?>

			  		<?php if ($slider_number->image2 == 2): ?>
			  			<?php if (isset($slider_images[1]->file_name) and !empty($slider_images[1]->file_name)): ?>
						    <div class="carousel-item active">
						      	<img class="d-block w-100" src="<?php echo base_url(); ?>assets/slider/<?php echo $slider_images[1]->file_name; ?>" alt="Second slide">
						    </div>
						     
						<?php endif ?> 
			  		<?php endif ?>

			  		<?php if ($slider_number->image3 == 2): ?>
			  			<?php if (isset($slider_images[2]->file_name) and !empty($slider_images[2]->file_name)): ?>
						    <div class="carousel-item active">
						      	<img class="d-block w-100" src="<?php echo base_url(); ?>assets/slider/<?php echo $slider_images[2]->file_name; ?>" alt="Second slide">
						    </div>
						    <div class="slider_overlay">
						      		<h1>..<?php echo  lang('Vinheo_is_what_you_need') ?>..</h1>
									<p>..<?php echo  lang('Share_your_videos_with_the_whole_world') ?>..</p>
									<a  href="<?php echo base_url(); ?>home/signup"   class="btn btn-primary btn_left">Join for free</a>
									<a href="<?php echo base_url(); ?>home/pricing"   class="btn btn-primary btn_right">Compare plans</a>
						<?php endif ?>   
			  		<?php endif ?>





			  		<?php if ($slider_number->image1 == 3): ?>
			  			<?php if (isset($slider_images[0]->file_name) and !empty($slider_images[0]->file_name)): ?>
						    <div class="carousel-item">
						      	<img class="d-block w-100" src="<?php echo base_url(); ?>assets/slider/<?php echo $slider_images[0]->file_name; ?>" alt="Second slide">
						    </div>
						    <div class="slider_overlay">
						      		<h1>..<?php echo  lang('Vinheo_is_what_you_need') ?>..</h1>
									<p>..<?php echo  lang('Share_your_videos_with_the_whole_world') ?>..</p>

									<a  href="<?php echo base_url(); ?>home/signup"   class="btn btn-primary btn_left">Join for free</a>
									<a  href="<?php echo base_url(); ?>home/pricing"   class="btn btn-primary btn_right">Compare plans</a>
						      	</div>
						<?php endif ?> 
			  		<?php endif ?>

			  		<?php if ($slider_number->image2 == 3): ?>
			  			<?php if (isset($slider_images[1]->file_name) and !empty($slider_images[1]->file_name)): ?>
						    <div class="carousel-item">
						      	<img class="d-block w-100" src="<?php echo base_url(); ?>assets/slider/<?php echo $slider_images[1]->file_name; ?>" alt="Second slide">
						    </div>
						    <div class="slider_overlay">
						      		<h1>..<?php echo  lang('Vinheo_is_what_you_need') ?>..</h1>
									<p>..<?php echo  lang('Share_your_videos_with_the_whole_world') ?>..</p>

									<a  href="<?php echo base_url(); ?>home/signup"  class="btn btn-primary btn_left">Join for free</a>
									<a  href="<?php echo base_url(); ?>home/pricing"   class="btn btn-primary btn_right">Compare plans</a>
						      	</div>
						<?php endif ?> 
			  		<?php endif ?>

			  		<?php if ($slider_number->image3 == 3): ?>
			  			<?php if (isset($slider_images[2]->file_name) and !empty($slider_images[2]->file_name)): ?>
						    <div class="carousel-item">
						      	<img class="d-block w-100" src="<?php echo base_url(); ?>assets/slider/<?php echo $slider_images[2]->file_name; ?>" alt="Second slide">
						    </div>
						    	<div class="slider_overlay">
						      		<h1>..<?php echo  lang('Vinheo_is_what_you_need') ?>..</h1>
									<p>..<?php echo  lang('Share_your_videos_with_the_whole_world') ?>..</p>

									<a  href="<?php echo base_url(); ?>home/signup"  class="btn btn-primary btn_left">Join for free</a>
									<a   href="<?php echo base_url(); ?>home/pricing"  class="btn btn-primary btn_right">Compare plans</a>
						      	</div>
						<?php endif ?>   
			  		<?php endif ?>

				    
			  	</div>
				<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>

			 

		</section>


		<?php endif ?>
	 
		<section class="padding_top top_left_back_1">

			<div class="container">

				<div class="row justify-content-between align-items-center">

					<div class="col-md-6 col-lg-6">

                          
					</div>

					<div class="col-md-6 col-lg-5 top_brand">

						<h1><?php echo  lang('Enhance_your_brand_with_top_quality_videos') ?>.</h1>

						<p><?php echo  lang('msg2') ?>.</p>

						<a href="<?php echo base_url(); ?>home/quality_video" class="learn"><?php echo  lang('Learn_more') ?> <i class="fa fa-angle-right"></i></a>

						<p>Plans from <b>$<?php echo $setting->quality_videos; ?></b>.</p>

						<a href="#" class="btn_sub"><?php echo  lang('Compare_plans') ?></a>

					</div>

				</div>

			</div>

		</section>



		<section class="padding_top top_left_back_2" style="background-position: right bottom;background: #f9fcff url(<?php echo base_url() ?>assets/img/privacy.jpg) no-repeat right top;background-size: 50% 100%;">

			<div class="container">

				<div class="row justify-content-between align-items-center">

					

					<div class="col-md-6 col-lg-5 top_brand">

						<h1><?php echo  lang('msg3') ?> </h1>

						<p><?php echo  lang('msg4') ?>.</p>

						<a href="<?php echo base_url(); ?>home/privacy_control" class="learn"><?php echo  lang('Learn_more') ?> <i class="fa fa-angle-right"></i></a>

						<p><?php echo  lang('msg5') ?><b>$<?php echo $setting->privacy; ?></b>.</p>

						<a href="#" class="btn_sub"><?php echo  lang('Compare_plans') ?> </a>

					</div>


					<div class="col-md-6 col-lg-6">

                         <!-- <div class="about_us_img_2">

							<img src="<?php echo base_url(); ?>assets/img/first-frame.jpg" alt="">

						</div> -->

					</div>

				</div>

			</div>

		</section>



		<section class="padding_top top_left_back_3" style="background: #f9fcff url(<?php echo base_url() ?>assets/img/team.jpg) no-repeat left top;background-size: 50% 100%;">

			<div class="container">

				<div class="row justify-content-between align-items-center">

					<div class="col-md-6 col-lg-6">

                        <!--  <div class="about_us_img_2">

							<img src="<?php echo base_url(); ?>assets/img/first-frame.jpg" alt="">

						</div> -->

					</div>

					<div class="col-md-6 col-lg-5 top_brand">

						<h1><?php echo  lang('msg6') ?> </h1>

						<p><?php echo  lang('msg7') ?></p>

						<a href="<?php echo base_url(); ?>home/team" class="learn"><?php echo  lang('Learn_more') ?> <i class="fa fa-angle-right"></i></a>

						<p><?php echo  lang('msg8') ?> <b>$<?php echo $setting->team; ?></b></b>.</p>

						<a href="#" class="btn_sub"><?php echo  lang('Compare_plans') ?></a>

					</div>

				</div>

			</div>

		</section>



		<section class="padding_top top_left_back_4"  style="background-position: right bottom;background: #f9fcff url(<?php echo base_url() ?>assets/img/public_grow.jpg) no-repeat right top;background-size: 50% 100%;">

			<div class="container">

				<div class="row justify-content-between align-items-center">

					<div class="col-md-6 col-lg-5 top_brand">

						<h1><?php echo  lang('msg9') ?> </h1>

						<p><?php echo  lang('msg10') ?>.</p>

						<a href="<?php echo base_url(); ?>home/public_grow" class="learn"><?php echo  lang('Learn_more') ?><i class="fa fa-angle-right"></i></a>

						<p><?php echo  lang('msg11') ?><b>$<?php echo $setting->public_grow; ?></b>.</p>

						<a href="#" class="btn_sub"><?php echo  lang('Compare_plans') ?></a>

					</div>


					<div class="col-md-6 col-lg-6">

                          <!-- <div class="about_us_img_2">

							<img src="<?php echo base_url(); ?>assets/img/first-frame.jpg" alt="">

						</div> -->

					</div>

					

				</div>

			</div>

		</section>



		<section class="padding_top top_left_back_5" style="background: #f9fcff url(<?php echo base_url() ?>assets/img/streaming.jpg) no-repeat left top;background-size: 50% 100%;">

			<div class="container">

				<div class="row justify-content-between align-items-center">

					<div class="col-md-6 col-lg-6">

                        <!--  <div class="about_us_img_2">

							<img src="<?php echo base_url(); ?>assets/img/first-frame.jpg" alt="">

						</div> -->

					</div>

					<div class="col-md-6 col-lg-5 top_brand">

						<h1><?php echo  lang('msg12') ?> </h1>

						<p><?php echo  lang('msg13') ?></p>

						<a href="<?php echo base_url(); ?>home/streaming" class="learn"><?php echo  lang('Learn_more') ?> <i class="fa fa-angle-right"></i></a>

						<p><?php echo  lang('msg14') ?><b>$<?php echo $setting->streaming; ?></b>.</p>

						<a href="#" class="btn_sub"><?php echo  lang('Compare_plans') ?></a>

					</div>

				</div>

			</div>

		</section>




		<section id="gallery">
		  	<div class="container">
			    <div id="image-gallery">
			      <div class="row">
			        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
			          <div class="img-wrapper">
			            <a href="https://unsplash.it/500"><img src="https://unsplash.it/500" class="img-responsive"></a>
			            <div class="img-overlay">
			              <i class="fa fa-plus-circle" aria-hidden="true"></i>
			            </div>
			          </div>
			        </div>
			        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
			          <div class="img-wrapper">
			            <a href="https://unsplash.it/600"><img src="https://unsplash.it/600" class="img-responsive"></a>
			            <div class="img-overlay">
			              <i class="fa fa-plus-circle" aria-hidden="true"></i>
			            </div>
			          </div>
			        </div>
			        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
			          <div class="img-wrapper">
			            <a href="https://unsplash.it/700"><img src="https://unsplash.it/700" class="img-responsive"></a>
			            <div class="img-overlay">
			              <i class="fa fa-plus-circle" aria-hidden="true"></i>
			            </div>
			          </div>
			        </div>
			        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
			          <div class="img-wrapper">
			            <a href="https://unsplash.it/800"><img src="https://unsplash.it/800" class="img-responsive"></a>
			            <div class="img-overlay">
			              <i class="fa fa-plus-circle" aria-hidden="true"></i>
			            </div>
			          </div>
			        </div>
			        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
			          <div class="img-wrapper">
			            <a href="https://unsplash.it/900"><img src="https://unsplash.it/900" class="img-responsive"></a>
			            <div class="img-overlay">
			              <i class="fa fa-plus-circle" aria-hidden="true"></i>
			            </div>
			          </div>
			        </div>
			        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
			          <div class="img-wrapper">
			            <a href="https://unsplash.it/1000"><img src="https://unsplash.it/1000" class="img-responsive"></a>
			            <div class="img-overlay">
			              <i class="fa fa-plus-circle" aria-hidden="true"></i>
			            </div>
			          </div>
			        </div>
			        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
			          <div class="img-wrapper">
			            <a href="https://unsplash.it/1100"><img src="https://unsplash.it/1100" class="img-responsive"></a>
			            <div class="img-overlay">
			              <i class="fa fa-plus-circle" aria-hidden="true"></i>
			            </div>
			          </div>
			        </div>
			        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
			          <div class="img-wrapper">
			            <a href="https://unsplash.it/1200"><img src="https://unsplash.it/1200" class="img-responsive"></a>
			            <div class="img-overlay">
			              <i class="fa fa-plus-circle" aria-hidden="true"></i>
			            </div>
			          </div>
			        </div>
			      </div><!-- End row -->
			    </div><!-- End image gallery -->
		  	</div><!-- End container --> 
		</section>

	 



		<section class="pricing_data" id="pricing_data_tab">

			<div class="container">

				<div class="row top_sticy ">

					<div class="col-lg-4 col-md-12 col-sm-12 pricing-set">

						<h1>Choose a plan.</h1>

						<p>Try any plan risk-free for 30 days. Or, start free with <a href="#">Vinheo Basic.</a></p>

					</div>

					<div class="col-lg-2  pricing-one border_top_1" id="border_top_1">

						<ul>

							<li><h3>Plus</h3></li>

							<li>5GB/week</li>

						</ul>

					</div>

					<div class="col-lg-2  pricing-one border_top_2" id="border_top_2">

						<ul>

							<li><h3>Pro</h3></li>

							<li>20GB/week</li>

						</ul>

					</div>

					<div class="col-lg-2  pricing-one border_top_3" id="border_top_3">

						<ul>

							<li><h3>Business</h3></li>

							<li>No weekly limits</li>

						</ul>

					</div>

					<div class="col-lg-2  pricing-one border_top_4" id="border_top_4">

						<ul>

							<li><h3>Premium</h3></li>

							<li>Unlimited live streaming</li>

						</ul>

					</div>

				</div>

			</div>

		</section>

		<section class="pricing_data_set">

			<div class="container">

				<div class="row top_sticy pricing_data_234">

					<div class="col-lg-4  pricing-set">

						

					</div>

					<div class="col-lg-2  pricing-one border_top_1" id="border_top_1">

						<ul>

							<li>250GB every year</li>

							<li>Single user</li>

							<li><span>$<?php echo $setting->plus; ?></span>/year</li>

							<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown_1"

								role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

								Enterprise

							</a>

							<div class="dropdown-menu" aria-labelledby="navbarDropdown">

								<a class="dropdown-item" href="services.html">$6  per month, billed annually (save 40%)</a>

								<a class="dropdown-item" href="dep.html">$10 per month, billed monthly</a>

							</div>

						</li>

						<li><a href="#" class="pricing_ddr">Get Plus</a></li>

					</ul>

				</div>

				<div class="col-lg-2  pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>1TB every year</li>

						<li>3 team members</li>

						<li><span>$<?php echo $setting->pro; ?></span>/year</li>

						<li>billed annually</li>

						<li><a href="#" class="bbusiness_set">Get Pro</a></li>

					</ul>

				</div>

				<div class="col-lg-2  pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>5TB total storage</li>

						<li>10 team members</li>

						<li><span>$<?php echo $setting->business; ?></span>/year</li>

						<li>billed annually</li>

						<li><a href="#" class="bbusiness">Get Business</a></li>

					</ul>

				</div>

				<div class="col-lg-2  pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>7TB total storage</li>

						<li>Unlimited live viewers</li>

						<li><span>$<?php echo $setting->premium; ?></span>/year</li>

						<li>Unlimited live viewers</li>

						<li><a href="#" class="bbusiness_set_last">Get Premium</a></li>

						

					</ul>

				</div>

			</div>

			</div>

		</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4  pricing-set">

					

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Player customization</li>

						<li>Privacy controls</li>

						<li>Social distribution</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Review and approval</li>

						<li>Private team projects</li>

						<li>Customizable Showcase sites</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Player calls-to-action</li>

						<li>Lead generation</li>

						<li>Engagement graphs</li>

						<li>Google Analytics</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited live events</li>

						<li>Live stream to multiple destinations</li>

						<li>Live Q&A, graphics and polls</li>

						<li>Audience chat</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Video Player <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_222343">

				<div class="col-lg-4 pricing-set">

					

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Embed anywhere</li>

						<li>Customize video player <i class="fa fa-angle-right"></i></li>

						<li>Custom end screens</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Embed anywhere</li>

						<li>Customize colors & components</li>

						<li>Customize end-screens</li>

						<li>Add your logo</li>

						<li>Playback speed control</li>

						<li>Third-party player support</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Embed anywhere</li>

						<li>Customize colors & components</li>

						<li>Customize end-screens</li>

						<li>Add your logo</li>

						<li>Playback speed control</li>

						<li>Third-party player support</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Embed anywhere</li>

						<li>Customize colors & components</li>

						<li>Customize end-screens</li>

						<li>Add your logo</li>

						<li>Playback speed control</li>

						<li>Third-party player support</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Privacy <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Collaborations <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Distribution & marketing <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Analytics <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Live streaming <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234">

				<div class="col-lg-4 pricing-set">

					<a href="#">Priority support <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="pricing_222">

		<div class="container">

			<div class="row top_sticy pricing_22234 pricing_2223434">

				<div class="col-lg-4 pricing-set">

					<a href="#">Earn money <i class="fa fa-angle-right"></i></a>

				</div>

				<div class="col-lg-2 pricing-one border_top_1" id="border_top_1">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_2" id="border_top_2">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_3" id="border_top_3">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

					</ul>

				</div>

				<div class="col-lg-2 pricing-one border_top_4" id="border_top_4">

					<ul>

						<li>Unlimited bandwidth in the Vinheo player</li>

						<li>4K & HDR support</li>

						<li>No ads before, after, or on your video</li>

						

					</ul>

				</div>

			</div>

		</div>

	</section>







<?php include('layout/footer.php'); ?>

<script type="text/javascript">
	// Gallery image hover
$( ".img-wrapper" ).hover(
  function() {
    $(this).find(".img-overlay").animate({opacity: 1}, 600);
  }, function() {
    $(this).find(".img-overlay").animate({opacity: 0}, 600);
  }
);

// Lightbox
var $overlay = $('<div id="overlay"></div>');
var $image = $("<img>");
var $prevButton = $('<div id="prevButton"><i class="fa fa-chevron-left"></i></div>');
var $nextButton = $('<div id="nextButton"><i class="fa fa-chevron-right"></i></div>');
var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

// Add overlay
$overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
$("#gallery").append($overlay);

// Hide overlay on default
$overlay.hide();

// When an image is clicked
$(".img-overlay").click(function(event) {
  // Prevents default behavior
  event.preventDefault();
  // Adds href attribute to variable
  var imageLocation = $(this).prev().attr("href");
  // Add the image src to $image
  $image.attr("src", imageLocation);
  // Fade in the overlay
  $overlay.fadeIn("slow");
});

// When the overlay is clicked
$overlay.click(function() {
  // Fade out the overlay
  $(this).fadeOut("slow");
});

// When next button is clicked
$nextButton.click(function(event) {
  // Hide the current image
  $("#overlay img").hide();
  // Overlay image location
  var $currentImgSrc = $("#overlay img").attr("src");
  // Image with matching location of the overlay image
  var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
  // Finds the next image
  var $nextImg = $($currentImg.closest(".image").next().find("img"));
  // All of the images in the gallery
  var $images = $("#image-gallery img");
  // If there is a next image
  if ($nextImg.length > 0) { 
    // Fade in the next image
    $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
  } else {
    // Otherwise fade in the first image
    $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
  }
  // Prevents overlay from being hidden
  event.stopPropagation();
});

// When previous button is clicked
$prevButton.click(function(event) {
  // Hide the current image
  $("#overlay img").hide();
  // Overlay image location
  var $currentImgSrc = $("#overlay img").attr("src");
  // Image with matching location of the overlay image
  var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
  // Finds the next image
  var $nextImg = $($currentImg.closest(".image").prev().find("img"));
  // Fade in the next image
  $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
  // Prevents overlay from being hidden
  event.stopPropagation();
});

// When the exit button is clicked
$exitButton.click(function() {
  // Fade out the overlay
  $("#overlay").fadeOut("slow");
});
</script>