<?php include('layout/header.php'); ?>



<style type="text/css">

	.center_me{

		background: #fff;

	    border-radius: 5px;

	    border-top: 5px solid #0EF91E;

	    margin: 0 auto;

	    border-bottom: 1px solid #ccc;

	    border-left: 1px solid #ccc;

	    border-right: 1px solid #ccc;

	    box-shadow: 0 0 7px 0 #ccc;

	    width: 700px;

	    padding: 20px 9px;

	    

	}

	.contact-title{

		text-align: center;

	}

	.btn_reset{
		width: 25%;
		background-color: #333333 !important; 
		background-image: linear-gradient(#3bf948, #22a22b) !important;    
		border-radius: 35px !important;
	}

	@media (max-width: 576px) {
		  /* line 18*/
	  	.center_me {
		    width: 100%;
	  	}

	  	.btn_reset{
	  		width: 81%; 
		}
  	}

</style>	









	<section class="contact-section padding_red">

		<div class="container">

			<div class="row">

				

			 	<div class="col-12  ">

			 		<div class="center_me">



			 			<div class="row">

			 				<div class="col-12  ">

								<h2 class="contact-title"><?php echo  lang('forgot_password') ?>?</h2>
								 
							</div>

						</div>



						<div class="row">

			 				<div class="col-12  ">

								<?php if ($this->session->flashdata('error')): ?> 

							      	<div class="alert alert-warning alert-dismissible " role="alert">

							            <?php echo $this->session->flashdata('error'); ?>

							            <button type="button" class="close" data-dismiss="alert" aria-label="Close">

							                  <span aria-hidden="true">&times;</span>

							            </button>

							      	</div> 

							    <?php endif ?>





							    <?php if ($this->session->flashdata('success')): ?> 

							      	<div class="alert alert-success alert-dismissible " role="alert">

							            <?php echo $this->session->flashdata('success'); ?>

							            <button type="button" class="close" data-dismiss="alert" aria-label="Close">

							                  <span aria-hidden="true">&times;</span>

							            </button>

							      	</div> 

							    <?php endif ?>

							</div>

						</div>



				 		<form class="" action="<?php echo base_url(); ?>home/forget_password" method="post" >

				 			<div class="row"> 
				 				<div class="col-12  form-group">

				 					<label><?php echo  lang('Email') ?></label>

				 					<input type="email" class="form-control" name="email" placeholder="<?php echo  lang('Email') ?>">

				 				</div>

				 			</div>


 


				 			<div class="row">

				 				<div class="col-12 form-group" style="text-align: center;    margin-top: 4%;">

				 					 

				 					<input type="submit"  value="<?php echo  lang('reset_password') ?>" class="btn btn-primary btn_reset" name="save_data"   >

				 				</div>

				 			</div>

				 		</form>



			 		</div>

			 	</div>



			</div>

		</div>

	</section>

		



<?php include('layout/footer.php'); ?>

