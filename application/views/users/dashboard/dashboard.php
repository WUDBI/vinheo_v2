<?php echo $this->load->view('users/layout/header','',true); ?>
         
        
<?php echo $this->load->view('users/layout/sticky_header','',true); ?>


<div id="page-wrapper">
	<h2 class="title1">Charts</h2>
	<div class="charts">
		<div class="col-md-4 charts-grids widget">
			<div class="card-header">
				<h3>Bar chart</h3>
			</div>
			
			<div id="container" style="width: 100%; height:270px;">
				<canvas id="canvas"></canvas>
			</div>
			<button id="randomizeData">Randomize Data</button>
			<button id="addDataset">Add Dataset</button>
			<button id="removeDataset">Remove Dataset</button>
			<button id="addData">Add Data</button>
			<button id="removeData">Remove Data</button>
			
		</div>
		
		<div class="col-md-4 charts-grids widget states-mdl">
			<div class="card-header">
				<h3>Column & Line Graph</h3>
			</div>
			<div id="chartdiv1"></div>
		</div>

		<div class="clearfix"> </div>
	</div>
</div>

<?php  echo $this->load->view('users/layout/footer','',true); ?>