<?php echo $this->load->view('users/layout/header','',true); ?>
         
        
<?php echo $this->load->view('users/layout/sticky_header','',true);  

$ci=& get_instance(); 

$ci->load->model('CommonModel'); 
?>
<style type="text/css">
    

.pagination-style-one a { padding: 7px; background: #31ABDE; color: #ffffff; border-radius: 50%; box-shadow: 0px 5px 10px 0px rgba(0,0,0,.1); margin: 0px 2px;}
.pagination-style-one a.selected, .pagination-style-one a:hover, .pagination-style-one a:active, .pagination-style-one a:focus { padding: 12px; box-shadow: 0px 5px 10px 0px rgba(0,0,0,.5);}
.pagination{
    display: block;
}
.overlay_status{
    position: absolute;
    bottom: 51px;
}
.overlay_edit_del{
    position: absolute;
    right: 2px;
    top: 10px;
}
.padding_5{
    padding: 5px !important;
}
.overlay_status p{
    background: #28a7dc;
    color: #fff;
    font-size: 11px;
    padding: 0 13px;
}
.overlay_edit_del p
{
    color: #2e3c42;
    font-size: 11px;
    padding: 0 13px;  
} 


.overlay_date {
    position: absolute;
    top: 10px;
}

.overlay_date p {
    background: #28a7dc;
    color: #fff;
    font-size: 11px;
    padding: 0 13px;
}
.price_img_box img{
    width: 100%;
    height: 201px;
}
.padding_0{
    padding: 0px !important;
}

.food_info h5{
    background-color: #dedede;
    padding: 13px 7px;
}

.add_padding_avatar_detail{
    padding: 6px 0px;
}
 .add_padding_avatar_detail img{
    width: 100%;
    border-radius: 50%; 
}
.padding_top_bottom_10{
    padding-top: 10px;
    padding-bottom: 10px;
}
</style>
<?php
 
function formatSizeUnits($size, $precision = 1, $show = "")
{
    $b = $size;
    $kb = round($size / 1024, $precision);
    $mb = round($kb / 1024, $precision);
    $gb = round($mb / 1024, $precision);

    if($kb == 0 || $show == "B") {
        return $b . " bytes";
    } else if($mb == 0 || $show == "KB") {
        return $kb . "KB";
    } else if($gb == 0 || $show == "MB") {
        return $mb . "MB";
    } else {
        return $gb . "GB";
    }
}

?>
<div id="page-wrapper">

    <div class="main-page">
        <div class="media">
            <h2 class="title1">Manage Your Videos</h2>
            <div class="bs-example5 " data-example-id="default-media">
                <div class="media">
                    <form method="get" action=""> 
                        <div class="row  "> 

                            <div class="col-sm-12 ">
                                <?php if ($this->session->flashdata('success')): ?> 
                                     <div class="alert alert-success alert-dismissible " role="alert">
                                            <?php echo $this->session->flashdata('success'); ?>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                            </button>
                                     </div> 
                                <?php endif ?>

                                <?php if ($this->session->flashdata('error')): ?>  
                                    <div class="alert alert-warning alert-dismissible " role="alert">
                                        <?php echo $this->session->flashdata('error'); ?>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div> 
                                <?php endif ?>
                            </div>
                            <div class="col-sm-3  form-group">
                                <label>Sort By</label>
                                <select class="form-control" name="sort_by"> 
                                    <option value="" hidden="" disabled="" selected="">Select</option>
                                    <option value="popular">Most popular(Coming Soon)</option>
                                    <option value="oldest">Date added (oldest)</option>
                                    <option value="newest">Date added (newest)</option>
                                </select>
                            </div>

                            <div class="col-sm-3  form-group">
                                <label>Status</label>
                                <select class="form-control" name="sort_by"> 
                                    <option value="" hidden="" disabled="" selected="">Select</option>
                                    <option value="draft">Draft</option>
                                    <option value="pploaded">Uploaded</option> 
                                </select>
                            </div>
                            <div class="col-sm-6  form-group"> 
                                <label>Search by video name</label>
                                <input type="text"  class="form-control"   name="search"> 
                            </div> 

                            <div class="col-sm-12 form-group">
                                <input type="submit"  class="btn btn-primary pull-right" value="Search">
                            </div> 
                        </div>
                    </form>
                </div> 
            </div>  

            <hr>
        </div>
    </div>



    <div class="main-page">
        <div class="media"> 
            <div class="bs-example5 " data-example-id="default-media">
                <div class="media">
                     
                    <div class="row  "> 
                        <?php if(!empty($all_videos)): ?>
                     
                            <?php foreach ($all_videos as $key => $value): ?> 
                                <div class="col-sm-4  padding_top_bottom_10">
                                    <a href="<?php echo base_url(); ?>upload/detail/<?php echo $value->video_id; ?>">
                                        <div class="price_img_box">
                                            <?php  $thumbnails = $ci->CommonModel->getAll('video_thumbnails', ' video_id ="'.$value->video_id.'" order by id desc limit 3 '); ?>

                                            <?php if (isset($thumbnails[0]) and $thumbnails[0]->base_img == 'no'): ?> 
                                                <img src="<?php echo base_url(); ?>assets/video_thumnails/<?php echo $this->session->userdata('user_id') ?>/<?php echo $thumbnails[0]->thumbnail; ?>"  > 
                                            <?php elseif(isset($thumbnails[0]->base_img) and $thumbnails[0]->base_img == 'yes'): 
                                                ?>   
                                                <img src="<?php echo base_url(); ?><?php echo $thumbnails[0]->thumbnail; ?>"  > 

                                            <?php else: ?>
                                                <img src="<?php echo base_url(); ?>assets/video_thumnails/1/load1.png" alt="">
                                            <?php endif; ?> 

                                            
                                            <div class="overlay_date">
                                                <p> <?php echo formatSizeUnits($value->video_size*1024); ?></p>
                                            </div>

                                            <div class="overlay_status">
                                                <p> <?php echo  ucfirst($value->status); ?></p>
                                            </div>

                                            <div class="overlay_edit_del">
                                                <p> <a href="<?php echo base_url(); ?>videoedit/edit_video/<?php echo $value->video_id; ?>"><i class="fa fa-pencil btn btn-info padding_5"></i> </a>
                                                 <a  onclick="return confirm('Are you sure you want to delete this video?');" href="<?php echo base_url(); ?>videoedit/delete_video/<?php echo $value->video_id; ?>"><i class="fa fa-trash btn btn-danger padding_5" ></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </a>

                                    <div class="food_info">
                                        
                                        <h5 style="    white-space: nowrap;">
                                            <?php if (!empty($value->title)): ?>
                                                <?php echo $value->title; ?>
                                            <?php else: ?>
                                                <?php echo $value->video_name; ?>
                                            <?php endif; ?>               
                                        </h5>
                                        
                                       <!--  <a href="#">
                                            <div class="row add_padding_avatar_detail"  > 
                                                <div class="col-sm-2 avtar_img padding_0">
                                                    <?php  if (isset($value->profile_pic) and !empty($value->profile_pic)): ?> 
                                                        <img src="<?php echo base_url().'assets/img/user_profiles/'.$value->user_id.'/'.$value->profile_pic; ?>" style='width: 100%;    height: 45px;' alt="">
                                                    <?php else: ?>
                                                        <img  style='width: 100%;height: 45px;' src="<?php echo base_url(); ?>assets/admin/images/2.jpg" alt="">
                                                    <?php endif; ?> 
                                                </div>
                                                <div class="col-sm-10 rating">
                                                    <h4>
                                                        <?php if (!empty($value->first_name)): ?>
                                                            <?php echo ucfirst($value->first_name); ?> 
                                                        <?php endif; ?> 

                                                        <?php if (!empty($value->last_name)): ?>
                                                            <?php echo ucfirst($value->last_name); ?> 
                                                        <?php endif; ?>   
                                                    </h4>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div> 
                                            </div> -->
                                        </a>    
                                    </div>

                                    <!-- <div class="row" >
                                        <div class="col-sm-3" style="background-color: #0EF91E; text-align: center;"> 
                                            <a   style="cursor: pointer;" class="copyText" data-selection="linkvalue<?php echo $value->video_id; ?>" >
                                                <div style="display: none;">
                                                    <input type="text" value="<?php echo base_url(); ?>upload/detail/<?php echo $value->video_id; ?>"  id='linkvalue<?php echo $value->video_id; ?>'>
                                                </div>
                                                <i class="fa fa-link" aria-hidden="true"></i>
                                            </a>
                                        </div> 
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3"  style="background-color: #0EF91E; text-align: center;"> 
                                            <a   style="cursor: pointer;"  data-toggle="modal" data-target=".exampleModal" > 
                                                <i class="fa fa-share" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div> -->
                                </div>
                            <?php endforeach; ?>
                            
                        <?php endif; ?>

                        <div class="col-sm-12 "> 
                            <?php if (isset($pagination)): ?>
                                <div class="pagination pagination-style-one  m-t-20" style="text-align: right;">
                                    <?php echo $pagination ?>
                                </div>
                            <?php endif ?>
                        </div>
                    </div> 
                </div> 
            </div>  
        </div>
    </div>
  
</div>

<?php  echo $this->load->view('users/layout/footer','',true); ?>